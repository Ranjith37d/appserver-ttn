import React, { useState, useMemo, useEffect } from "react";
import { Section, InputNumber } from "@/components/visualizations/editor";
import Checkbox from "antd/lib/checkbox/Checkbox";
import ColorPicker from "@/components/ColorPicker";
import ColorPalette from "@/visualizations/ColorPalette";
import Table from "antd/lib/table";
import Input from "antd/lib/input";
import Form from "antd/lib/form";
import Button from "antd/lib/button";
import { UpdateOptionsStrategy } from "@/components/visualizations/editor/createTabbedEditor";
import { max } from "lodash";

const RangeStateSettings = ({ form, options, data, visualizationName, onOptionsChange }) => {


  const [states, setStates] = useState(options.states);
  const MIN = options.range[0];
  const MAX = options.range[1];

  const colors = useMemo(
    () => ({
      Automatic: null,
      ...ColorPalette,
    }),
    []
  );

  useEffect(() => {
    onOptionsChange({ states: states }, UpdateOptionsStrategy.shallowMerge);
  }, [states]);

  const addRow = () => {
    var newState = [...states];
    var index = newState.reduce((accumulator, currentValue) => Math.max(accumulator, currentValue.key), newState[0].key)
    //  data.reduce((max, b) => Math.max(max, b.y), data[0].y);

    newState.push({ range: [0, 0], key: index + 1, color: "#FFFFFF" });
    setStates(newState);
  };

  const deleteRow = recordKey => {
    var newState = [...states];
    var index = newState.findIndex(record => record.key == recordKey);

    newState.splice(index, 1);
    setStates(newState);
  };

  const updateStates = (value, index, record) => {
    var newState = [...states];
    const keyIndex = newState.findIndex(data => data.key === record.key)
  
    if ("color" == index) {
      newState[keyIndex].color = value;
      onOptionsChange({ states: newState });
    } else if ("from" == index) {
      newState[keyIndex].from = value;

      onOptionsChange({ states: newState });
    } 
    else if ('label' == index) {
      newState[keyIndex].label = value;
      onOptionsChange({ states: newState });
    }
    
    else {

      newState[keyIndex].to = value;

      onOptionsChange({ states: newState });
    }
  };

  const columns = [
    {
      title: "Label",
      key: "label",
      dataIndex: "label",
      render: (label, record) =>
        <Input value={label} onChange={e => updateStates(e.target.value, 'label', record)} />,
    },
    {
      title: "From",
      key: "from",
      dataIndex: "from",
      render: (from, record) => (

        <InputNumber value={from} key={record.key} onChange={value => updateStates(value, "from", record)} />

      ),
    },
    {
      title: "TO",
      key: "to",
      dataIndex: "to",
      render: (to, record) => (

        <InputNumber value={to} key={record.key} onChange={value => updateStates(value, "to", record)} />


      ),
    },
    {
      title: "Color ",
      key: "color",
      dataIndex: "color",
      render: (color, record) => (
        <ColorPicker
          key={record.key}
          color={color}
          interactive
          presetColors={colors}
          placement="topRight"
          onChange={newColor => updateStates(newColor, "color", record)}
          addonAfter={<ColorPicker.Label color={color} presetColors={colors} />}
        />
      ),
    },

    {
      title: "",
      dataIndex: "operation",
      render: (text, record) => {
        return <>{record.key > 2 && <i className="fa fa-trash-o m-r-5" onClick={() => deleteRow(record.key)} />}</>;
      },
    },
  ];

  return (
    <>
      <div className="add-btn" style={{ display: "flex", justifyContent: "flex-end", marginBottom: "5px" }}>
        <div style={{ paddingRight: "15px", marginRight: "auto" }}>
          <Section>
            <Checkbox checked={options.showLabel} onChange={e => onOptionsChange({ showLabel: e.target.checked })}>
              Show Label
            </Checkbox>

            <Checkbox checked={options.showValue} onChange={e => onOptionsChange({ showValue: e.target.checked })}>
              Show Value
            </Checkbox>
            <Checkbox checked={options.showIcon} onChange={e => onOptionsChange({ showIcon: e.target.checked })}>
              Show Icon
            </Checkbox>
          </Section>
        </div>
        <Button onClick={addRow} type="primary">
          <i className="fa fa-plus m-r-5" />
          Add
        </Button>
      </div>
      <Section>
        <Table dataSource={states} columns={columns} pagination />
      </Section>
    </>
  );
};

export default RangeStateSettings;
