import createTabbedEditor from "@/components/visualizations/editor/createTabbedEditor";
import { GenralSetting } from "./GenralSettings";
import { DownlinkCriteria } from "./DownlinkCriteria";

export default createTabbedEditor([
  { key: "Genral", title: "Genral", component: GenralSetting },
  { key: "Condition", title: "Condition", component: DownlinkCriteria },
]);
