import Renderer from "./Renderer";
import Editor from "./Editor";

const DEFAULT_OPTIONS = {
  value: 0,
  title: { text: "" },
  showLink: false,
  displaylogo: false,
  selectedValue: "",
  gauge: {
    axis: {
      range: [0, 100],
      visible: false,
    },
  },

  type: "indicator",
  mode: "gauge+number",
};

export default {
  type: "GAUGE",
  name: "Gauge",
  getOptions: options => ({ ...DEFAULT_OPTIONS, ...options }),
  Renderer,
  Editor,
  defaultColumns: 3,
  defaultRows: 8,
  minColumns: 1,
  minRows: 5,
};
