import time
import logging

from flask import request
from funcy import project

from datetime import datetime

from redash import models
from redash.serializers import serialize_downlink_command, serialize_downlink_schedule, serialize_alert_downlinks
from redash.handlers.base import BaseResource, get_object_or_404, require_fields
from redash.permissions import (
    require_access,
    require_admin_or_owner,
    require_permission,
    view_only,
)
from redash.utils import json_dumps
from redash.tasks import schedule_cron, send_downlink_command, rq_scheduler

from flask_restful import abort

logger = logging.getLogger(__name__)

class DownlinkCommandResource(BaseResource):
    def get(self, command_id):
        downlink = get_object_or_404(
            #models.Alert.get_by_id_and_org, alert_id, self.current_org
            models.DownlinkCommand.get_by_id_and_user, command_id, self.current_user
        )
        #require_access(downlink, self.current_user, view_only)
        self.record_event(
            {"action": "view", "object_id": downlink.id, "object_type": "downlink_command"}
        )
        return serialize_downlink_command(downlink)

    def post(self, command_id):
        req = request.get_json(True)
        params = project(req, ("name", "device_id", "channel_id", "payload", "mode", "confirm", "fport"))
        command = get_object_or_404(
            models.DownlinkCommand.get_by_id_and_user, command_id, self.current_user
        )
        require_admin_or_owner(command.user_id)

        self.update_model(command, params)
        models.db.session.commit()

        self.record_event(
            {"action": "edit", "object_id": command.id, "object_type": "downlink_command"}
        )

        return serialize_downlink_command(command)

    def delete(self, command_id):
        command = get_object_or_404(
            models.DownlinkCommand.get_by_id_and_user, command_id, self.current_user
        )
        require_admin_or_owner(command.user_id)

        for alertDownlink in models.AlertDownlinks.by_downlink(command_id):
            models.db.session.delete(alertDownlink)

        for schedule in command.schedules:
            rq_scheduler.cancel(schedule.job_id)
            models.db.session.delete(schedule)

        models.db.session.delete(command)
        models.db.session.commit()


class DownlinkCommandListResource(BaseResource):
    def post(self):
        req = request.get_json(True)
        require_fields(req, ("name", "device_id", "channel_id","payload", "mode", "confirm", "fport"))

        command = models.DownlinkCommand(
            name = req["name"],
            device_id = req["device_id"],
            channel_id = req["channel_id"],
            payload = req["payload"],
            mode = req["mode"],
            confirm = req["confirm"],
            fport = req["fport"],
            user_id = self.current_user.id,
            org_id = self.current_org.id,         
        )

        models.db.session.add(command)
        models.db.session.commit()

        self.record_event(
            {"action": "create", "object_id": command.id, "object_type": "downlink_command"}
        )

        return serialize_downlink_command(command)

   # @require_permission("list_downlink_commands")
    def get(self):
        self.record_event({"action": "list", "object_type": "downlink_command"})
        return [
            serialize_downlink_command(command)
            # for alert in models.Alert.all(group_ids=self.current_user.group_ids)
             for command in models.DownlinkCommand.by_user(self.current_user)
        ]

class DownlinkScheduleListResource(BaseResource):
    def get(self, command_id):
        self.record_event({"action": "list", "object_type": "downlink_schedule"})
        return [
            serialize_downlink_schedule(schedule)
            # for alert in models.Alert.all(group_ids=self.current_user.group_ids)
             for schedule in models.DownlinkSchedule.by_downlink(command_id, self.current_user)
        ]
    def post(self, command_id):
        req = request.get_json(True)
        require_fields(req, ("type", "schedule"))

        if req["type"] == "e":
            require_fields(req, ("name","channel_id","device_id","attribute", "operator", "value"))


        command = get_object_or_404(
            models.DownlinkCommand.get_by_id_and_user, command_id, self.current_user
        )
        require_admin_or_owner(command.user_id)

        schedule = models.DownlinkSchedule(
            name = req["name"],
            downlink_id = command_id,
            schedule_type=req["type"],
            schedule = req["schedule"],
            user_id=self.current_user.id
        )

        if req["type"] == "o": # One time execution
            schedule_dt = datetime.strptime(req["schedule"], '%Y-%m-%d %H:%M:%S')
            job = rq_scheduler.enqueue_at(schedule_dt, send_downlink_command, command_id)
            schedule.job_id = job.id

        elif req["type"] == "r": # Recurring via cron schedule
            job = schedule_cron({"func": send_downlink_command, "args": [command_id], "cron_string": req["schedule"] })
            schedule.job_id = job.id
        
        elif req["type"] == "e": 
            schedule.schedule = "v3/{}/devices/{}/uplink".format(req['channel_id'],req['device_id'])
            event_condition = {}
            event_condition["attribute"] = req['attribute']
            event_condition["operator"] = req['operator']
            event_condition["value"] = req['value']
            schedule.event_condition = event_condition
            schedule.job_id = "" 
        
        else:
            abort(400)
        
        
        models.db.session.add(schedule)
        models.db.session.commit()

      #  logger.info("Scheduling %s (%s) with schedule %s.", job.id, job["func_name"], req["schedule"],)

        self.record_event(
            {"action": "create", "object_id": schedule.id, "object_type": "downlink_schedule"}
        )

        return {"id": schedule.id, "name": schedule.name }
    

class DownlinkScheduleResource(BaseResource):
    def delete(self, command_id, schedule_id):
        schedule = get_object_or_404(
            models.DownlinkSchedule.get_by_id_and_user, command_id, schedule_id, self.current_user
        )
        require_admin_or_owner(schedule.user_id)

        rq_scheduler.cancel(schedule.job_id)

        models.db.session.delete(schedule)
        models.db.session.commit()

        return {"id": schedule.id, "name": schedule.name }


class AlertDownlinksListResource(BaseResource):
    def get(self, alert_id):
        self.record_event({"action": "list", "object_type": "alert_downlinks"})
        return [
            serialize_alert_downlinks(alertDownlink)
            # for alert in models.Alert.all(group_ids=self.current_user.group_ids)
             for alertDownlink in models.AlertDownlinks.by_alert(alert_id, self.current_user)
        ]
    def post(self, alert_id):
        req = request.get_json(True)
        require_fields(req, ("alert_id", "command_id"))
        downlink_id = req["command_id"]

        command = get_object_or_404(
            models.DownlinkCommand.get_by_id_and_user, downlink_id, self.current_user
        )
        require_admin_or_owner(command.user_id)

        alert = get_object_or_404(
            models.Alert.get_by_id_and_user, alert_id, self.current_user
        )
        require_admin_or_owner(alert.user_id)

                
        alertDownlink = models.AlertDownlinks(
            
            downlink_id = downlink_id,
            alert_id = alert_id,
            user_id = self.current_user.id,
        )
        models.db.session.add(alertDownlink)
        models.db.session.commit()

        self.record_event(
            {"action": "create", "object_id": alertDownlink.id, "object_type": "alert_downlink"}
        )

        d = alertDownlink.to_dict()
        return d
    

class AlertDownlinkResource(BaseResource):
    def delete(self, alert_id, command_id):
        alertDownlink = get_object_or_404(
            models.AlertDownlinks.get_by_id_and_user, command_id, alert_id, self.current_user
        )
        require_admin_or_owner(alertDownlink.user_id)

        models.db.session.delete(alertDownlink)
        models.db.session.commit()