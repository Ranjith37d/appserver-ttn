import React, { useEffect, useRef, useState } from "react";
import routeWithUserSession from "@/components/ApplicationArea/routeWithUserSession";
import { Form, Input, Button, Select, InputNumber, Radio, Checkbox } from "antd";
import * as Grid from "antd/lib/grid";
import routes from "@/services/routes";
import notification from "@/services/notification";
import { getChannels, getDevices } from "@/services/query-result";
import Command from "@/services/command";
import moment from "moment";
import SchedulerList from "./Scheduler-list";

import DownlinkAlertDialogue from "./SchedulesDialogModal";
import navigateTo from "@/components/ApplicationArea/navigateTo";
import DeleteOutlined from "@ant-design/icons/DeleteOutlined";
import EditInPlace from "@/components/EditInPlace";
import Events from "./Events";

const CommandView = props => {
  const { TextArea } = Input;
  const [CommandDetails, setCommandDetails] = useState({
    name: "",
    command_name: "",
    channe_id: "",
    device_id: "",
    payload: "",
    fport: 12,
    mode: 1,
    confirm: false,
  });
  const formref = useRef();
  const [load, setLoad] = useState(false);
  const [form] = Form.useForm();
  const [schedulesList, setSchedulesList] = useState([]);
  const [commandName, setCommandName] = useState(CommandDetails.name);
  const [channels, setChannels] = useState([]);
  const [selectedChannel, setSelectedChannel] = useState();
  const [selectedDevices, setSelectedDevices] = useState();
  const [devices, setDevices] = useState("");
  const [payload, setPaylod] = useState(CommandDetails.payload);
  const [selecetedInsertedMode, setSelectedInsertedMode] = useState("push");
  const [confirmedDownlink, setConfirmedDownlink] = useState(CommandDetails.confirm);
  const [fPort, setFPort] = useState(CommandDetails.fport);

  useEffect(() => {
    Command.getId(props.id).then(command => {
      setCommandName(command.name);
      setCommandDetails(command);
      setSelectedChannel(command.channel_id);
      setSelectedDevices(command.device_ids);
      setFPort(command.fport);
      setSelectedInsertedMode(command.mode);
      setConfirmedDownlink(command.confirm);
      setPaylod(command.payload);
      form.setFieldsValue({ Device: command.device_ids });
      form.setFieldsValue({ Channel: command.channel_id });
    });
    Command.getScheduleslist(props.id).then(command => setSchedulesList(command));
    if (channels && channels.length === 0) {
      getChannels().then(data => setChannels(data));
    }
  }, [load]);

  useEffect(() => {
    if (selectedChannel != CommandDetails.channel_id) {
      form.setFieldsValue({ Device:[]});

      setSelectedDevices([]);

    }
    if (selectedChannel) {
      getDevices(selectedChannel).then(data => {
        setDevices(data);
      // form.setFieldsValue({ Device: data.device_name});

      
      });
    }
  }, [selectedChannel]);

  const ScheudlerAdd = schedulerDetails => {
    Command.saveScheduler(props.id, schedulerDetails)
      .then(data => {
        notification.success("Schedule created Successfully");
      })
      .catch(data => {
        notification.success("Error while  Creating Schedules");
      });
  };

  const removeSchedules = schedulerId => {
    Command.deleteSchedules(props.id, schedulerId)
      .then(data => notification.success("schedule Delete Successfully"))
      .catch(data => notification.error("Error while deleting Schedules"));
  };

  const saveChange = () => {
    form.submit();
    form.validateFields().then(() => {
      Command.editCommands({
        id: props.id,
        command_name: commandName,
        channel_id: selectedChannel,
        device_ids: selectedDevices,
        payload: payload,
        fport: fPort,
        confirm: confirmedDownlink,
        mode: selecetedInsertedMode,
      })
        .then(data => {
          notification.success("Commands saved Successfully");
        })
        .catch(data => notification.error("Error while Saving Schedules"));
    });
  };

  const EventSchedule = event => {


    ScheudlerAdd({
      name: `${event.channel_id + "/" + event.device_id + ":" + event.attribute + event.operator + event.value}`,
      schedule: "",
      type: "e",
      channel_id: event.channel_id,
      device_id: event.device_id,
      attribute: event.attribute,
      operator: event.operator,
      value: event.value,
    });
    Events.showModal().close();
    setLoad(!load);
  };

  const ScheduleNow = () => {
    var name = moment()
      .utc()
      .format("YYYY-MM-DD HH:mm:ss");
    ScheudlerAdd({
      type: "o",
      name: name,
      timezone: Intl.DateTimeFormat().resolvedOptions().timeZone,
      schedule: name,
    });
    setLoad(!load);
  };

  const deleteCommands = () => {
    Command.deleteCommands(props.id)
      .then(() => {
        notification.success("Commands deleted Successfully");
        navigateTo(`/console/app/commands/`);
      })
      .catch(e => {
        notification.error("Error while delete Commands");
      });
  };

  const command = e => {
    setCommandName(e);
    if (!commandName === CommandDetails.name) {
      saveChange();
    }
  };

  return (
    <>
      <div className="alert-header">
        <div className="alert-title">
          <h3>
            {/* <Input placeholder="Borderless" value={commandName} onChange={e => { command(e.target.value); saveChange() }} bordered={false} /> */}
            <EditInPlace
              isEditable={true}
              onDone={e => {
                command(e);
              }}
              ignoreBlanks
              value={commandName}
            />
          </h3>
        </div>
      </div>
      <div className=" tiled p-20" style={{ background: "#f4f4f4" }}>
        <Grid.Row type="flex" gutter={16}>
          <Grid.Col
            xs={24}
            md={8}
            style={{
              boxShadow: "-3px -3px 5px white, 3px 3px 5px rgba(0, 0, 0, 0.2)",
              background: "#f9f7f7",
              paddingLeft: "15px",
              paddingRight: "15px",
            }}>
            <Form initialValues={{ Device: selectedDevices, Channel: selectedChannel }} layout="vertical" form={form}>
              <Form.Item label="Channel" name="Channel" rules={[{ required: true }]}>
                <Select
                  className="gutter-row"
             
                  style={{ width: "100%", border: "none", borderRadius: "2px" }}
                  placeholder="Select Channel"
                  optionFilterProp="children"
                  onChange={setSelectedChannel}
                  filterOption={(input, option) => option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}>
                  {channels &&
                    channels.map(channel => {
                      return (
                        <Select.Option key={channel.channel_id} value={channel.channel_id}>
                          {channel.channel_name}
                        </Select.Option>
                      );
                    })}
                </Select>
              </Form.Item>
              <Form.Item name="Device" label="Devices" rules={[{ required: true }]}>
                <Select
                  style={{ width: "100%", border: "none", borderRadius: "2px" }}
                  placeholder="Select Device"
                  onChange={setSelectedDevices}
                  mode = "multiple"
                  filterOption={(input, option) => option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}>
                  {devices &&
                    devices.map(device => {
                      return (
                        <Select.Option key={device.device_id} value={device.device_id}>
                          {device.device_name}
                        </Select.Option>
                      );
                    })}
                </Select>
              </Form.Item>
              <Form.Item name="mode" label="Mode">
                <Radio.Group
                  options={["push", "replace"]}
                  onChange={e => {
                    setSelectedInsertedMode(e.target.value);
                  }}
                  defaultValue={selecetedInsertedMode}
                  value={selecetedInsertedMode}>
                  {selecetedInsertedMode && ""}
                </Radio.Group>
              </Form.Item>

              <Form.Item name="Confirmation" label="Confirmation">
                <Checkbox checked={confirmedDownlink} onChange={e => setConfirmedDownlink(e.target.checked)}>
                  Confirmed
                </Checkbox>
              </Form.Item>
              <Form.Item label="Fport">
                <InputNumber value={fPort} defaultValue={CommandDetails.fport} onChange={e => setFPort(e)} />
              </Form.Item>
              <Form.Item label="Payload">
                <TextArea
                  rows={4}
                  type="text"
                  value={payload}
                  onChange={e => {
                    setPaylod(e.target.value);
                  }}
                />
              </Form.Item>
              <Form.Item>
                <Button type="primary" onClick={saveChange}>
                  Save changes{" "}
                </Button>
                <Button style={{ marginLeft: "10px", minWidth: "115px" }} type="ghost" danger onClick={deleteCommands}>
                  <DeleteOutlined
                    style={{
                      color: "#f14646cf",
                    }}
                  />{" "}
                  Delete{" "}
                </Button>
              </Form.Item>
              <Form.Item></Form.Item>
            </Form>
          </Grid.Col>

          <Grid.Col xs={24} md={16}>
            <div style={{ display: "flex", flexDirection: "row", margin: "10px" }}>
              <h4>Schedules</h4>
              <div style={{ display: "flex", marginLeft: "auto" }}>
                {/* <Button
                  style={{ marginRight: "10px" }}
                  type="primary"
                  onClick={() => Events.showModal({ EventSchedule })}>
                  <i className="fa fa-calendar f-12 m-r-5" />
                  Add Event
                </Button> */}

                <Button style={{ marginRight: "10px" }} type="primary" onClick={ScheduleNow}>
                  <i className="fa fa-clock-o f-12 m-r-5" />
                  Schedule Now
                </Button>

                <div>
                  <Button
                    style={{ alignSelf: "end", marginLeft: "auto" }}
                    type="primary"
                    onClick={() => {
                      DownlinkAlertDialogue.showModal({ load, ScheudlerAdd, setLoad });
                    }}>
                    <i className="fa fa-plus f-12 m-r-5" /> Add Schedule
                  </Button>
                </div>
              </div>
            </div>

            <div>
              <SchedulerList
                loadState={load}
                setload={setLoad}
                readonly={true}
                schedules={schedulesList}
                remove={removeSchedules}
              />
            </div>
          </Grid.Col>
        </Grid.Row>
      </div>
    </>
  );
};

routes.register(
  "Commands.View",
  routeWithUserSession({
    path: "/commands/:id",
    title: "Command",
    render: pageProps => <CommandView {...pageProps} currentPage="alerts" />,
  })
);
