import Renderer from "./Renderer";
import Editor from "./Editor";
const DEFAULT_OPTIONS = {
  rowNumber: 1,
  targetRowNumber: 1,
  counterColName: "counter",
  stringDecimal: 0,
  stringDecChar: ".",
  stringThouSep: ",",
  tooltipFormat: "0,0.000",
  temperature: "",
  range: [0, 100],
  ParameterData:'',
  labelName: "",
  showValue: true,
  showLabel: true,
  showIcon:true,
  states: [
    {
      from: 50,
      to: 100,
      key: 0,
      color: "#43B05C",
    },
    {
      from: 20,
      to: 50,
      key: 1,
      color: "#f7bf6a",
    },
    { from: 0, to: 20, key: 2, color: "#ff4245" },
  ],
};

export default {
  type: "TANK",
  name: "Tank",
  parameterData: "",
  labelName: "",

  getOptions: options => ({
    ...DEFAULT_OPTIONS,
    ...options,
  }),
  Renderer,
  Editor,

  defaultColumns: 3,
  defaultRows: 8,
  minColumns: 1,
  minRows: 5,
};
