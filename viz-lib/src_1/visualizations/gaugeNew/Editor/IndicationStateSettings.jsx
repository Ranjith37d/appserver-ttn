import React, { useState, useMemo, useEffect } from "react";
import { Section } from "@/components/visualizations/editor";
import Checkbox from "antd/lib/checkbox/Checkbox";
import ColorPicker from "@/components/ColorPicker";
import ColorPalette from "@/visualizations/ColorPalette";
import Table from "antd/lib/table";
import Input from "antd/lib/input";
import Button from "antd/lib/button"
import { UpdateOptionsStrategy } from "@/components/visualizations/editor/createTabbedEditor";


const IndicationStateSettings = ({ options, data, visualizationName, onOptionsChange }) => {

  const [states, setStates] = useState(options.states);

  const colors = useMemo(
    () => ({
      Automatic: null,
      ...ColorPalette,
    }),
    []
  );

  useEffect(() => {
    onOptionsChange({ states: states }, UpdateOptionsStrategy.shallowMerge)
  }, [states])

  const updateState = (record, newValue, cell) => {

    const newData = [...states];
    const index = newData.findIndex(item => record.key == item.key);

    const item = newData[index];
    item[cell] = newValue;
    newData.splice(index, 1, { ...item });

    setStates(newData);
  }

  const addRow = () => {
    var newState = [...states];
    newState.push({ key: newState.length + 1, upto:'', color: '#FFFFFF' });
    setStates(newState);
  }

  const deleteRow = (key) => {
    var newState = [...states];
    var index = newState.indexOf(key)

    newState.splice(index, 1)
    setStates(newState);

  }



  const columns = [

   
    {
      title: "Upto",
      key: "upto",
      dataIndex: "upto",
      render: (val, record) => <Input value={val} onChange={(e) => updateState(record, e.target.value, 'upto')} />
    },
    {
      title: "Color ",
      key: "color",
      dataIndex: "color",
      render: (color, record) => <ColorPicker
        color={color}
        interactive
        presetColors={colors}
        placement="topRight"
        onChange={(newColor) => updateState(record, newColor, 'color')}
        addonAfter={<ColorPicker.Label color={color} presetColors={colors} />} />,
    },

    {
      title: '',
      dataIndex: 'operation',
      render: (text, record) => {
        return (<>
          {
            
            <i className="fa fa-trash-o m-r-5" onClick={() => deleteRow(record.key)} />
          }
        </>)
      }
    },
  ];

  return (
    <>


      <div className="add-btn" style={{ display: 'flex', justifyContent: "flex-end", marginBottom: '5px' }}>
       <div style={{paddingRight:'15px',marginRight:"auto"}}>
        <Section >
          <Checkbox checked={options.showLabel} onChange={(e) => onOptionsChange({ showLabel: e.target.checked })}   >
            Show Label
         
      </Checkbox>


          <Checkbox checked={options.showValue} onChange={(e) => onOptionsChange({ showValue: e.target.checked })}   >
            Show Value
      </Checkbox>
   


        </Section>
       </div>
        <Button onClick={addRow} type="primary">

          <i className="fa fa-plus m-r-5" />
         Add</Button>
      </div>
      <Section>
        <Table dataSource={states} columns={columns} pagination />
      </Section>
    </>
  );
};

export default IndicationStateSettings;
