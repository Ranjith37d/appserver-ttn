"""empty message

Revision ID: a9a861e80557
Revises: 1702f1b32ab6
Create Date: 2021-01-21 15:30:29.518397

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'a9a861e80557'
down_revision = '1702f1b32ab6'
branch_labels = None
depends_on = None


def upgrade():
    pass


def downgrade():
    pass
