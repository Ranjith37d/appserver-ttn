import React, { Component } from "react";
import moment from "moment";
import { DatePicker } from "antd";
export default class OneTime extends Component {
  constructor(props) {
    super(props);
    this.state = {
      time: moment(),
    };
  }
  componentDidMount() {
    // this.setState({ schedulesType: "o" });
    this.setState({ value: moment() });
    this.props.onChange(moment());
  }
  onChange(e) {
    
    this.setState({ time: e });
    this.props.onChange(e);
  }

  render() {
  
    let value = this.props.value;
    value = moment();
    
    const range = (start, end) => {
      const result = [];
      for (let i = start; i < end; i++) {
        result.push(i);
      }
      return result;
    };

    const disablePastDate = currentDate => {
      return currentDate && currentDate <= moment().startOf("d");
    };

    const disablePastTime = value => {
      let changed = value === undefined ? moment() : value;
      const HOUR = moment().format("D") == changed.format("D") ? moment().hour() : 0;
      const MINUTES = moment().format("hh") == changed.format("hh") ? moment().minute() : 0;
      const SECONDS = moment().second('ss')== changed.format("ss") ? moment().second() : 0;;
      // moment().format("D") == moment().format("D") ? range(0, MINUTES) :
  
      return {
        disabledHours: () => range(0, HOUR),

        disabledMinutes: () => range(0, MINUTES),
        disabledSeconds: () => range(0, SECONDS),
      };
    };

    return (
      <div>
        <DatePicker
          format="YYYY-MM-DD hh:mm:ss"
          disabledTime={disablePastTime}
          disabledDate={disablePastDate}
          onChange={this.onChange.bind(this)}
          style={{ flex: 0.7 }}
          autoFocus
          allowClear={false}
          value={this.state.time}
          onCalendarChange={disablePastTime}
          showTime
        />
      </div>
    );
  }
}
