import { isFinite } from "lodash";
import React, { useState, useEffect } from "react";
import cx from "classnames";
import resizeObserver from "@/services/resizeObserver";
import { RendererPropTypes } from "@/visualizations/prop-types";

import { getCounterData } from "./utils";

import "./render.less";

function getCounterStyles(scale) {


  return {
    msTransform: `scale(${scale})`,
    MozTransform: `scale(${scale})`,
    WebkitTransform: `scale(${scale})`,
    transform: `scale(${scale})`,
  };
}

function getCounterScale(container) {
  const inner = container.firstChild;
  const scale = Math.min(container.offsetWidth / inner.offsetWidth, container.offsetHeight / inner.offsetHeight);
 
  return Number(isFinite(scale) ? scale : 1).toFixed(2); // keep only two decimal places
}

export default function Renderer({setLabelContiner,labelName,currentLabel,type, setscale,scale,setcontainer,container, setlabelTextContainer,settextValueContainer, data, options, visualizationName }) {

  useEffect(() => {
    if (container) {
      const unwatch = resizeObserver(container, () => {
        setscale(getCounterScale(container));
      });
      return unwatch;
    }
  }, [container, options]);

  useEffect(() => {
    if (container) {
      // update scaling when options or data change (new formatting, values, etc.
      // may change inner container dimensions which will not be tracked by `resizeObserver`);
      setscale(getCounterScale(container));
    }
  }, [data, options, container]);

  const {
    showTrend,
    trendPositive,
    counterValue,
    counterValueTooltip,
    targetValue,
    targetValueTooltip,
    counterLabel,
  } = getCounterData(data.rows, options, visualizationName);



  return (
    <div className={cx("counter-visualization-container")}>
      <div className="counter-visualization-content" ref={setcontainer}>
        <div style={getCounterStyles(scale)}>
          <div  ref={settextValueContainer}  className="text-visualization-value">
            {options.showValue && counterValue}
          </div>
          
          {["PARKING"].includes(type) &&
            <div ref={setLabelContiner} className="counter-visualization-target" >
            {options.showLabel &&
              labelName
            }
            </div>
          }
          <div ref={setlabelTextContainer} className="text-visualization-label">
            { options.showLabel && currentLabel}
          </div>
        </div>
      </div>
    </div>
  );
}

Renderer.propTypes = RendererPropTypes;
