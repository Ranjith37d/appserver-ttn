import React, { useState, useEffect, useRef } from "react";
import Plotly from "plotly.js/lib/core";
import { RendererPropTypes } from "@/visualizations/prop-types";

import resizeObserver from "@/services/resizeObserver";
export default function Renderer({ options, data, visualizationName }) {
  const [contextValue, setContextValue] = useState(null);
  const [container, setContainer] = useState(null);

  // filter the last value  from the rows

  const filtervalue = () => {
    if (options.selectedValue !== undefined) {
      if (data.rows.length !== 0) {
        setContextValue(data.rows[0][options.selectedValue]);
        options.value = contextValue;
      } else {
        options.value = 0;
      }
    }
  };

  useEffect(() => {
    if (container) {
      const unwatch = resizeObserver(container, () => {
        // Clear existing data with blank data for succeeding codeCall adds data to existing plot.
   
      });
      return unwatch;
    }
  }, [container]);

  // Cleanup when component destroyed
  useEffect(() => {
    if (container) {
      return () => Plotly.purge(container);
    }
  }, [container]);

  useEffect(() => {
    if (container) {
      if (options.selectedValue.length >= 1) {
        filtervalue();
      }

      Plotly.newPlot(container, [options]);
    }
  }, [options, container, contextValue]);

  return <div className="gauage chart-visualization-container" ref={setContainer} />;
}

Renderer.propsTypes = RendererPropTypes;
