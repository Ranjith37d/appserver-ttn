import time
from flask import request
from flask_restful import abort
from redash import models
from redash.permissions import require_admin, require_permission
from redash.handlers.base import BaseResource, get_object_or_404, require_fields
from sqlalchemy.exc import IntegrityError
import logging

logger = logging.Logger('channels')


class ChannelListResource(BaseResource):
    # @require_permission("list_data_sources")
    def get(self):
        if self.current_user.has_permission("admin"):
            response = models.Channel.all()
        else:
            response = models.Channel.by_user(self.current_user)
        
        return [d.to_dict() for d in response]


    # @require_admin
    def post(self):
        req = request.get_json(True)
        require_fields(req, ("channel_name", "channel_id", ))

        try:
            channel = models.Channel.create(req['channel_id'], req['channel_name'], self.current_user.id)            

        except IntegrityError as e:
            if req["channel_name"] in str(e):
                logger.debug(str(e))
                abort(
                    400,
                    message="Channel with the name {} already exists.".format(
                        req["channel_name"]
                    ),
                )

            abort(400)

        self.record_event(
            {
                "action": "create",
                "object_id": channel.id,
                "object_type": "channel",
            }
        )

        return channel.to_dict()

class ChannelResource(BaseResource):
    def get(self, channel_id):
        response = models.Channel.by_channel_id(channel_id, self.current_user)
        return response.to_dict()
    
    def post(self, channel_id):
        req = request.get_json(True)
        require_fields(req, ("channel_name",))

        try:
           return models.Channel.update(channel_id, req['channel_name'], self.current_user).to_dict()
        except IntegrityError as e:
            if req["channel_name"] in str(e):
                logger.debug(str(e))
                abort(
                    400,
                    message="Channel with the name {} already exists.".format(
                        req["channel_name"]
                    ),
                )
            abort(400)

        self.record_event(
            {
                "action": "create",
                "object_id": channel.id,
                "object_type": "channel",
            }
        )

        return channel.to_dict()

    def delete(self, channel_id,):
        return models.Channel.delete(channel_id,  self.current_user)
