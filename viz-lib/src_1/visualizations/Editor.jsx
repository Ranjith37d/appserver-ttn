import React, { useMemo } from "react";
import PropTypes from "prop-types";
import { EditorPropTypes } from "@/visualizations/prop-types";
import registeredVisualizations from "@/visualizations/registeredVisualizations";

export default function Editor({form,isValid,validation, type, options: optionsProp, data, ...otherProps }) {
  const { Editor, getOptions } = registeredVisualizations[type];
  const options = useMemo(() => getOptions(optionsProp, data), [optionsProp, data]);


  return <Editor options={options} type={type} data={data} {...otherProps} form={form} />;
}

Editor.propTypes = {
  type: PropTypes.string.isRequired,
  ...EditorPropTypes,
};
