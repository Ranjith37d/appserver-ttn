import createTabbedEditor from "@/components/visualizations/editor/createTabbedEditor";
import GeneralSettings from "@/visualizations/components/GeneralSettings";
import RangeStateSettings from "@/visualizations/battery/Editor/RangeIndiactionSettings";
import FormatSettings from "@/visualizations/components/FormatSettings";

export default createTabbedEditor([
  { key: "General", title: "General", component: GeneralSettings },
  { key: "state", title: "State", component: RangeStateSettings },
  { key: "Format", title: "Format", component: FormatSettings },
]);
