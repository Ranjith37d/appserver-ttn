"""empty message

Revision ID: e18481167177
Revises: 37f713b89bf8
Create Date: 2020-10-14 19:27:50.596859

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'e18481167177'
down_revision = '37f713b89bf8'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_index('devices_id', table_name='devices')
    op.create_index('devices_id', 'devices', ['id'], unique=True)
    op.create_unique_constraint(None, 'devices', ['device_id'])
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_constraint(None, 'devices', type_='unique')
    op.drop_index('devices_id', table_name='devices')
    op.create_index('devices_id', 'devices', ['device_id'], unique=True)
    # ### end Alembic commands ###
