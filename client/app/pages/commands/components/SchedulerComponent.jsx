import React, { useState, useEffect } from "react";

import CronTimeFrame from "./CronTimeFrame";

export const SchedulerComponent = props => {
  const [TimeFormat, setTimeFormat] = useState(props.schedulestype);

  const [error, onError] = useState();

  useEffect(() => {
    setTimeFormat(props.schedulestype);
  }, [props.schedulestype]);

  return (
    <>
      <div style={{ paddingTop: "20px", display: "flex" }}>
        <>
          <CronTimeFrame
            onerror={onError}
            time={props.time}
            settime={props.settime}
            setTimezone={props.settimeZone}
            readonly={false}
            value={props.cron}
            onChange={props.setcron}
            schedulesType={props.schedulestype}
          />
        </>
      </div>
    </>
  );
};

export default SchedulerComponent;
