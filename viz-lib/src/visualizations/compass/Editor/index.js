import createTabbedEditor from "@/components/visualizations/editor/createTabbedEditor";
import FormatSettings from "@/visualizations/components/FormatSettings";

import GeneralSettings from "./GenralSettings";

export default createTabbedEditor([
  { key: "General", title: "General", component: GeneralSettings },
  { key: "Format", title: "Format", component: FormatSettings },
]);
