import Renderer  from "./Renderer";
import Editor from "./Editor";
const DEFAULT_OPTIONS = {
 commandName:'',
 commandId:''
};

export default {
  type: "COMMAND",
  name: "Command",
  getOptions: options => ({ ...DEFAULT_OPTIONS, ...options }),
  Renderer,
  Editor,
  defaultColumns: 3,
  defaultRows: 8,
  minColumns: 1,
  minRows: 5,
};
