import React, { useState, useEffect } from "react";
import Drawer from "antd/lib/drawer";
import Form from "antd/lib/form";
import Input from "antd/lib/input";
import Button from "antd/lib/button";
import Select from "antd/lib/select";
import notification from "@/services/notification";
import Visualization from "@/services/visualization";
import recordEvent from "@/services/recordEvent";
import createTabbedEditor from "./CreateTabbedEditor"
export const WidgetDrawerComponent = props => {
  const { visible, onClose, widget, columns } = props;

  const [visualization, setVisualization] = useState();
  useEffect(() => {
    setVisualization(props.widget.visualization);
  }, []);

  function saveVisualization() {

    if (visualization.id) {
      recordEvent("update", "visualization", visualization.id, { type: visualization.type });
    } else {
      recordEvent("create", "visualization", null, { type: visualization.type });
    }

    return Visualization.save(visualization)
      .then(result => {
        notification.success("widget saved");
        return result;
      })
      .catch(error => {
        notification.error("widget could not be saved");
        return Promise.reject(error);
      });
  }

  return (
    <>
      <Drawer
        width={600}
        title={widget.query.name || ""}
        placement="right"
        closable={true}
        onClose={onClose}
        visible={visible}
        footer={
          <div
            style={{
              textAlign: "right",
            }}>
            <Button onClick={onClose} style={{ marginRight: 8 }}>
              Cancel
            </Button>
            <Button onClick={saveVisualization} type="primary">
              Submit
            </Button>
          </div>
        }>
        <Form
          initialValues={{
            labelName: props.widget.visualization.options.labelName,
            parameter: props.widget.visualization.options.ParameterData,
          }}>
          <Form.Item name="labelName" label="Label Name">
            <Input type="text" onChange={e => 
              setVisualization({...visualization,...visualization.options.labelName= e.target.value })} />
          </Form.Item>
          <Form.Item name="parameter" label="Parameter">
            <Select onChange={(value)=> setVisualization({...visualization,...visualization.options.ParameterData= value })} >{columns && columns.map(column => <Select.Option key={column.name}>{column.name}</Select.Option>)}</Select>
          </Form.Item>
        </Form>


      </Drawer>
    </>
  );
};
