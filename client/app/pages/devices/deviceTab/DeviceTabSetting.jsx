import React, { useState, useEffect } from "react";
import { DeviceDataEvents } from "@devices/deviceTab/Data";
import { Tabs } from "antd";
import routeWithUserSession from "@/components/ApplicationArea/routeWithUserSession";
import routes from "@/services/routes";
import { Device } from "@/services/device";
import { MqttDownLink } from "../channeltab/Downlink";
import { DeviceGenralSettings } from "./DeviceGenralSettings";
import { DeviceDetails } from "./DeviceDetails";
import { DashboardGrid } from "./Dashboard";
import io from "socket.io-client";
import { Auth, currentUser } from "@/services/auth";
import { Dashboard as DashboardService } from "@/services/dashboard";
import { SocketProvider } from "react-socket-io-hooks";
const { TabPane } = Tabs;
export function DeviceSettingWrapper(props) {
  const [deviceDetails, setdeviceDetails] = useState({});
  const [dashboard ,setDashboard]=useState()

  useEffect(() => {
    DashboardService.get({ id: 315, slug: "realtime-data-test" }).then(dashboardData => setDashboard(dashboardData));
  }, []);




  useEffect(() => {
    Device.deviceDetails(props.channelId, props.id).then(device => {
      setdeviceDetails(device);
    });
  }, []);


  // const socket = React.useCallback( io("https://beta.thethingsmate.com", { transports: ["websocket"] }),[]);
  const reducer = (state, action) => {
    switch(action.type) {
      case 'UPDATED_COUNT':
        return { count: action.payload };
      default:
        return state;
    }
  }

  //Same API in component

  return (
    <>
       <SocketProvider
      uri="http://localhost:9191/"
      reducer={reducer}
      initialState={{ count: 0 }}
      socketOpts={{ transports: ["websocket"] }}
      
      >
      <DeviceDetails name={deviceDetails.name} />
      <br />
      <div className="settings-screen">
        <div className="container">
          <div className="bg-white tiled">

            <Tabs type="card" style={{ padding: "20px",}}>
              <TabPane tab="Downlink" key="1">
                {/* <MqttDownLink {...props} device={deviceDetails} socketApi={socket} /> */}
              </TabPane>

              <TabPane tab="Data" key="3">
                <></>
                {/* <DeviceDataEvents {...props} deviceDetail={deviceDetails} socketApi={socket} /> */}
              </TabPane>
              <TabPane tab="General settings" key="5">
                <DeviceGenralSettings {...props} deviceDetail={deviceDetails} />
              </TabPane>
              {currentUser.name === "admin" && (
                <TabPane tab="Dashboard" key="6">
                  <DashboardGrid {...props} deviceDetail={deviceDetails} dashboard={dashboard} />
                </TabPane>
              )}
            </Tabs>
          </div>
        </div>
      </div>
      </SocketProvider>
    </>
  );
}

function DeviceOverviewPage(props) {
  return (
    <>
      <DeviceSettingWrapper {...props} />
    </>
  );
}

routes.register(
  "Device.Overview",
  routeWithUserSession({
    path: "/mqtt/:channelId/devices/:id",
    title: "Channels ",
    render: pageProps => (
      <>
        <DeviceOverviewPage {...pageProps} currentPage="alerts" />
      </>
    ),
  })
);
