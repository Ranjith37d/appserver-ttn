import Renderer from "./Renderer";
import Editor from "./Editor";

const DEFAULT_OPTIONS = {
  rowNumber: 1,
  targetRowNumber: 1,
  counterColName: "counter",
  stringDecimal: 0,
  stringDecChar: ".",
  stringThouSep: ",",
  tooltipFormat: "0,0.000",
  range: [0, 100],
  ParameterData: '',
  labelName:'',
  showValue:true,
  showLabel:true,

  states: [],

};

export default {
  type: "New GAUGE",
  name: "Gauge",
  getOptions: options => ({ ...DEFAULT_OPTIONS, ...options }),
  Renderer,
  Editor,
  defaultColumns: 2,
  defaultRows: 8,
  minColumns: 1,
  minRows: 5,
};
