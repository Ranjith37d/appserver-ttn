import React, { useState, useEffect, useRef } from "react";
import Select from "antd/lib/select";
import { Row, Col, Button, Form } from "antd";
import {
  getChannels,
  getDevices,
  getQueryDetails,
  getDeviceAttributes,
  updateQueryDetails,
} from "../../../services/query-result";

import notification from "@/services/notification";

const EditQueryBuilder = (props) => {
  //Hooks
  const [form] = Form.useForm();
  const formRef = useRef();

  const [channels, setChannels] = useState([]);
  const [devices, setDevices] = useState([]);
  const [deviceAttributes, setDeviceAttributes] = useState([]);

  const [selectedChannel, setSelectedChannel] = useState("");
  const [selectedDevices, setSelectedDevices] = useState([]);
  const [selectedDeviceAttributes, setSelectedDeviceAttributes] = useState([]);

  //channel Api calls
  useEffect(() => {
    if (channels && channels.length === 0) {
      getQueryDetails(props.query.id).then(data => {
        setSelectedChannel(data.channel_id);
        setSelectedDevices(data.device_ids);
        setSelectedDeviceAttributes(data.attributes);
        form.setFieldsValue({ name: data.channel_id, device: data.device_ids });
        form.setFieldsValue({ device: data.device_ids });
        form.setFieldsValue({ attributes: data.attributes });
      });
    }
  }, []);
  useEffect(() => {
    if (channels && channels.length === 0) {
      getChannels().then(data => {
        setChannels(data);
      });
    }
  }, []);
  // device api calls
  useEffect(() => {
    if (selectedChannel) {
      getDevices(selectedChannel).then(data => {
        setDevices(data);
      });
    }
    form.setFieldsValue({ device: [] });
  }, [selectedChannel]);
  // attributes api  call
  useEffect(() => {
    if (selectedChannel && selectedDevices.length) {
      getDeviceAttributes(selectedChannel, selectedDevices).then(data => {
        setDeviceAttributes(data);
      });
    
    }
    form.setFieldsValue({ attributes: [] });
  }, [selectedChannel, selectedDevices]);
  // update Query Details
  const updateQuery = () => {
    console.log(selectedDeviceAttributes)
    form.validateFields().then(() => {
     
        updateQueryDetails(props.query.id, selectedChannel, selectedDevices, selectedDeviceAttributes)
          .then(data => {
            props.doExecuteQuery()
            notification.success("updated SuccessFully");
            window.location.reload(window.location.href);
          })
          .catch(e => {
            notification.error(e.response);
            notification.error("Check All the Values");
          });
      
    });
  };

  return (
    <>
      <Row gutter={24} style={{ paddingLeft: "15px", marginRight: "0px" }}>
        <Form style={{flex:1}} layout="inline" form={form} ref={formRef}>
          <Col span={6}>
            <Form.Item
              name="name"
              style={{ minWidth: "150px" }}
              rules={[
                {
                  required: true,
                },
              ]}>
              <Select
                style={{ width: "100%", border: "none", borderRadius: "2px" }}
                placeholder="Select Channel"
                optionFilterProp="children"
                value={selectedChannel}
                onChange={setSelectedChannel}
                filterOption={(input, option) => option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}>
                {channels &&
                  channels.map(channel => {
                    return (
                      <Select.Option key={channel.channel_id} value={channel.channel_id}>
                        {channel.channel_name}
                      </Select.Option>
                    );
                  })}
              </Select>
            </Form.Item>
          </Col>

          <Col span={6}>
            <Form.Item
              name="device"
              style={{ minWidth: "150px" }}
              rules={[
                {
                  required: true,
                },
              ]}>
              <Select
                style={{ width: "100%", border: "none", borderRadius: "2px" }}
                placeholder="Select Device"
                onChange={e=>setSelectedDevices([e])}
                value={selectedDevices}
                filterOption={(input, option) => option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}>
                {devices &&
                  devices.map(device => {
                    return (
                      <Select.Option key={device.device_id} value={device.device_id}>
                        {device.device_name}
                      </Select.Option>
                    );
                  })}
              </Select>
            </Form.Item>
          </Col>
          <Col span={6}>
            <Form.Item
              name="attributes"
              style={{ minWidth: "100px" }}
              rules={[
                {
                  required: true,
                },
              ]}>
              <Select
                mode="multiple"
                placeholder="Select Attributes"
                value={selectedDeviceAttributes}
                onChange={setSelectedDeviceAttributes}>
                {deviceAttributes &&
                  deviceAttributes.map(item => (
                    <Select.Option key={item} value={item}>
                      {item}
                    </Select.Option>
                  ))}
              </Select>
            </Form.Item>
          </Col>
          <Col span={6}>
            <Button type="primary" onClick={updateQuery}>
              Apply Changes
            </Button>
          </Col>
        </Form>
      </Row>
    </>
  );
};

export default EditQueryBuilder;
