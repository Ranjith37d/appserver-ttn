import React, { useEffect, useState } from "react";
import Tree from "antd/lib/tree";
import routeWithUserSession from "@/components/ApplicationArea/routeWithUserSession";
import routes from "@/services/routes";
import { getChannels, getDevices, getDeviceAttributes } from "@/services/query-result";

const QueryTree = () => {
  const [treeData, setTreeData] = useState([]);
  const [rootNodeData, setRootNodeData] = useState([]);

  //key is unique for channel

  useEffect(() => {
    getChannels().then(channels => {
      var channelData = [];
      channels.forEach(channel =>
        channelData.push({
          title: channel.channel_name,
          key: channel.channel_id,
          icon: false,
          type: "channel",
          children: [],
        })
      );
      setTreeData(channelData);
    });
  }, []);

  useEffect(() => {
    setRootNodeData(root => [
      {
        title: "Channel",
        key: "0-0",
        checkable: false,
        children: [...treeData],
        expanded: true,
      },
    ]);

  }, [treeData]);

  //value , event
  const onChecked = (checkedKeys, { node, checked }) => {
  
    //handle checked
    if (checked && node.children.length === 0) {
      const channelIndex = treeData.findIndex(channels => channels.key === node.key);
  
      getDevices(node.key).then(devices => {
        devices.forEach(device => {
          node.children.push({
            title: device.device_name,
            type: "device",
            key: device.device_id,
            checked: false,
            channel_id: node.key,
            device_id: device.device_id,
            children: [],
          });
          node.expanded = node.children.length > 0;
          var updatedTreeData = treeData;
          updatedTreeData.splice(channelIndex, 1, node);
          setTreeData([...updatedTreeData]);
        });
      });

      if (checked && node.type === "device") {
        getDeviceAttributes(node.channel_id, node.device_id).then(attributes => {
          attributes.forEach(attribute =>
            node.children.push({ title: attribute, key: attribute, type: "attribute", children: [] })
          );
          node.expanded = node.children.length > 0;
          var updatedTreeData = treeData;
          const deviceIndex = treeData[17].children.findIndex(device => device.key === node.key);

          updatedTreeData[17].children.splice(deviceIndex, 1, node);

          setTreeData([...updatedTreeData]);
        });
      }
    }
  };

  return (
    <>
      <Tree
        height={500}
        virtual={true}
        // checkStrictly={true}
        onCheck={onChecked}
        checkable
        treeData={rootNodeData}
        autoExpandParent={true}
   
        
      />
    </>
  );
};

export default QueryTree