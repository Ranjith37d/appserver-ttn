import { isFunction, has } from "lodash";
import React from "react";
import PropTypes from "prop-types";
import cx from "classnames";
import { Moment } from "@/components/proptypes";
import TimeAgo from "@/components/TimeAgo";
import SchedulePhrase from "@/components/queries/SchedulePhrase";
import { IMG_ROOT } from "@/services/data-source";

import "./QueryMetadata.less";

export default function QueryMetadata({ query, dataSource, layout, onEditSchedule }) {
  return (
    <div
      className={`query-metadata query-metadata-${layout}`}
      style={{ padding: "none !important", paddingRight: "10px", margin: "none !important" }}>
      <div className="query-metadata-space" />

      <div className="query-metadata-item">
        <div className="query-metadata-property">
          <span className="query-metadata-label">
            <span className="zmdi zmdi-refresh m-r-5" />
            Refresh Schedule
          </span>
          <span className="query-metadata-value">
            <SchedulePhrase
              isLink={isFunction(onEditSchedule)}
              isNew={query.isNew()}
              schedule={query.schedule}
              onClick={onEditSchedule}
            />
          </span>
        </div>
      </div>
    </div>
  );
}

QueryMetadata.propTypes = {
  layout: PropTypes.oneOf(["table", "horizontal"]),
  query: PropTypes.shape({
    created_at: PropTypes.oneOfType([PropTypes.string, Moment]).isRequired,
    updated_at: PropTypes.oneOfType([PropTypes.string, Moment]).isRequired,
    user: PropTypes.shape({
      name: PropTypes.string.isRequired,
      profile_image_url: PropTypes.string.isRequired,
      is_disabled: PropTypes.bool,
    }).isRequired,
    last_modified_by: PropTypes.shape({
      name: PropTypes.string.isRequired,
      profile_image_url: PropTypes.string.isRequired,
      is_disabled: PropTypes.bool,
    }).isRequired,
    schedule: PropTypes.object,
  }).isRequired,
  dataSource: PropTypes.shape({
    type: PropTypes.string,
    name: PropTypes.string,
  }),
  onEditSchedule: PropTypes.func,
};

QueryMetadata.defaultProps = {
  layout: "table",
  dataSource: null,
  onEditSchedule: null,
};
