import React from "react";

import Menu from "antd/lib/menu";
import Link from "@/components/Link";

import { Auth, currentUser } from "@/services/auth";
import { Dropdown } from "antd";
import location from "@/services/location";
import "./DesktopNavbar.less";
import {
  ApiFilled,
  CodeFilled,
  DashboardFilled,
  AlertFilled,
  UserOutlined,
  ApartmentOutlined,
  ThunderboltOutlined,
  MessageFilled 
} from "@ant-design/icons";
import { createBrowserHistory } from "history";
const history = createBrowserHistory();

class DesktopNavbar extends React.Component {
  state = {
    mode: "horizontal",
    theme: "light",
    one: "orgborder a1 ",
    selectedMenu: "dashboards",
    visible: false,
  };

  changeMode = value => {
    this.setState({
      mode: value ? "vertical" : "horizontal",
    });
  };
  handleLogout = () => {
    Auth.logout();
    location.setPath("/logout", false);
  };

  setActiveClass = isActive => {
    return isActive ? "orgborder a1 all active" : "orgborder a1 all";
  };

  setSelectedMenu = menu => {
    this.setState({ seletedMenu: menu });
  };

  changeTheme = value => {
    this.setState({
      theme: value ? "dark" : "light",
    });
  };

  render() {
    // const canCreateQuery = currentUser.hasPermission("create_query");
    // const canCreateDashboard = currentUser.hasPermission("create_dashboard");
    // const canCreateAlert = currentUser.hasPermission("list_alerts");
    const menu = (
      <Menu>
        <Menu.Item onClick={this.handleLogout} key="1" style={{ color: "#404646" }}>
          <Link href="logout">Logout</Link>
        </Menu.Item>
      </Menu>
    );
    return (
      <div className="headerContainer">
        <div className="headerBar">
          <div className="listContainer">
            <div className="header-right">
              <Link href="./" key="logout">
                <h1 style={{ color: "#fff", fontFamily: "Penguin", fontSize: "25px", width: "max-content" }}>
                  The ThingsMate
                </h1>
              </Link>
            </div>
            <ul id="mydiv" className="navListContainer">
              <Link
                id=""
                className={this.setActiveClass(location.path.indexOf("/channels") >= 0)}
                onClick={() => this.setSelectedMenu("channels")}
                href="/console/channels"
                key="channels">
                <ApartmentOutlined className="icons" />
                <span>LoRaWAN</span>
              </Link>
              {currentUser.hasPermission("list_dashboards") && (
                <Link
                  id=""
                  className={this.setActiveClass(location.path.indexOf("/dashboards") >= 0)}
                  onClick={() => this.setSelectedMenu("dashboards")}
                  href="dashboards"
                  key="dashboards">
                  <DashboardFilled className="icons" width="5" />
                  <span>Dashboards</span>
                </Link>
              )}
              {currentUser.hasPermission("view_query") && (
                <Link
                  className={this.setActiveClass(location.path.indexOf("/queries") >= 0)}
                  onClick={() => this.setSelectedMenu("queries")}
                  href="queries/my"
                  key="queries">
                  <CodeFilled width="5em" className="icons" />
                  <span>Queries</span>
                </Link>
              )}

              {currentUser.hasPermission("list_alerts") && (
                <Link
                  className={this.setActiveClass(location.path.indexOf("/alerts") >= 0)}
                  onClick={() => this.setSelectedMenu("alerts")}
                  href="alerts"
                  key="alerts">
                  <AlertFilled className="icons" />
                  <span>Alerts</span>
                </Link>
              )}
              <Link
                className={this.setActiveClass(location.path.indexOf("/mqtt") >= 0)}
                onClick={() => this.setSelectedMenu("queries")}
                href="mqtt"
                key="devices">
                <ApiFilled width="5em" className="icons" />
                <span>MQTT </span>
              </Link>
              <Link
                className={this.setActiveClass(location.path.indexOf("/commands") >= 0)}
                onClick={() => this.setSelectedMenu("commands")}
                href="commands"
                key="commands">
                <ThunderboltOutlined width="5em" className="icons" />
                <span>Commands </span>
              </Link>
              {/* <Link
                className={this.setActiveClass(location.path.indexOf("/destination") >= 0)}
                onClick={() => this.setSelectedMenu("destination")}
                href="destination"
                key="destination">
                <MessageFilled width="5em" className="icons" />
                <span>Destination</span>
              </Link> */}
              {/* {firstSettingsTab && (
              <Link
              className={this.setActiveClass(
                [
                  "/data_sources",
                  "/users",
                  "/groups",
                  "/destinations",
                  "/query_snippets",
                  "/settings/general",
                  "/users/me",
                ].indexOf(location.path) >= 0
                )}
                onClick={() => this.setSelectedMenu("settings")}
                href={firstSettingsTab.path}
                data-test="SettingsLink"
                key="settings">
                <SettingFilled className="icons" />
                <span>Settings</span>
                </Link>
              )} */}
            </ul>
          </div>
          <Dropdown overlay={menu} trigger={["click"]} className="profile">
            <div style={{ padding: "1.2%", color: "#fff" }}>
              <UserOutlined className="icons" />
              <a className="ant-dropdown-link" style={{ color: "#fff" }} onClick={e => e.preventDefault()}>
                {currentUser.name}
              </a>
            </div>
          </Dropdown>
        </div>
      </div>
    );
  }
}
export default DesktopNavbar;
