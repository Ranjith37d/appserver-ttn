import debug from "debug";
import { includes, extend } from "lodash";
import location from "@/services/location";
import { axios } from "@/services/axios";

export const currentUser = {
  canEdit(object) {
    const userId = object.user_id || (object.user && object.user.id);
    return this.hasPermission("admin") || (userId && userId === this.id);
  },

  canCreate() {
    return (
      this.hasPermission("create_query") || this.hasPermission("create_dashboard") || this.hasPermission("list_alerts")
    );
  },

  hasPermission(permission) {
    return includes(this.permissions, permission);
  },

  get isAdmin() {
    return this.hasPermission("admin");
  },
};

export const clientConfig = {};
export const messages = [];

const logger = debug("redash:auth");
const session = { loaded: false };

const AuthUrls = {
  Login: "login",
};

function updateSession(sessionData) {
  logger("Updating session to be:", sessionData);
  extend(session, sessionData, { loaded: true });
  extend(currentUser, session.user);
  extend(clientConfig, session.client_config);
  extend(messages, session.messages);
}

export const Auth = {
  isAuthenticated() {
    return session.loaded && session.user.id;
  },
  setLoginUrl(loginUrl) {
    AuthUrls.Login = loginUrl;
  },
  login() {
    const next = encodeURI(location.url);
    logger("Calling login with next = %s", next);
    window.location.href = `${AuthUrls.Login}?next=${next}`;
  },
  token() {
    return axios.get("/console/api/auth/token");
  },
  logout() {

    var csrf_tokens = "";

    var xhr = new XMLHttpRequest();

    xhr.onreadystatechange = function () {
      if (xhr.readyState === 4) {
        csrf_tokens = xhr.getResponseHeader("x-csrf-token");

        logout_with_csrf(csrf_tokens);

      }
    };
    xhr.open("GET", "/console");
    xhr.send();
    //=======logout
    function logout_with_csrf(csrf_tokens) {
      var xhr2 = new XMLHttpRequest();
      xhr2.onreadystatechange = function () {
        if (xhr2.readyState === 4) {
          if (xhr.status == 403) {
            //retry to get csrf
            var retryXhr = new XMLHttpRequest();
            retryXhr.onreadystatechange = function () {
              if (retryXhr.readyState === 4) {
                csrf_tokens = retryXhr.getResponseHeader("x-csrf-token");

                logout_with_csrf(csrf_tokens);

              }
            };
            retryXhr.open("GET", "/console");
            retryXhr.send();
          }
          var resp = xhr2.responseText;
       

          window.location.href = JSON.parse(resp).op_logout_uri;
       
        }
      };
      xhr2.open("POST", "/console/api/auth/logout");
      xhr2.setRequestHeader("x-csrf-token", csrf_tokens);
      xhr2.send();
    }



  },



  //   logout(){



  //   var csrf_tokens = "";

  //   var xhr = new XMLHttpRequest();

  //   xhr.onreadystatechange = async function() {
  //     if (xhr.readyState === 4) {
  //       delete axios.defaults.headers.common["x-csrf-token"];
  //       csrf_tokens = xhr.getResponseHeader("x-csrf-token");

  //       console.log(csrf_tokens)
  //        axios.defaults.headers.common["x-csrf-token"] = csrf_tokens;

  //       try {
  //         return await  axios.post(`/console/api/auth/logout`, undefined,{
  //         headers: { 'X-CSRF-Token': csrf_tokens }}, )
  //       }catch (error) {
  //         if (
  //           error.response &&
  //           error.response.status === 403 &&
  //           typeof error.response.data === 'string' &&
  //           error.response.data.includes('CSRF')
  //         ) {
  //           // If the CSRF token is invalid, it likely means that the CSRF cookie
  //           // has been deleted or became outdated. Making a new request to the
  //           // current path can then retrieve a fresh CSRF cookie, with which
  //           // the logout can be retried.
  //           const csrfResult =  axios.get("/console")
  //           console.log(csrfRe)
  //           const freshCsrf = csrfResult.headers['x-csrf-token']
  //           if (freshCsrf) {
  //             return await axios.post(`/console/api/auth/logout`, undefined, {
  //               headers: { 'X-CSRF-Token': csrf_tokens }},)
  //           }
  //         }

  //         throw error
  //       }

  //     }
  //   };
  //   xhr.open("GET", "/console");
  //   xhr.send();
  // }  ,
  loadSession() {
    logger("Loading session");
    if (session.loaded && session.user.id) {
      logger("Resolving with local value.");
      return Promise.resolve(session);
    }

    Auth.setApiKey(null);
    return axios.get("api/session").then(data => {
      updateSession(data);
      return session;
    });
  },
  loadConfig() {
    logger("Loading config");
    return axios.get("console/app/api/config").then(data => {
      updateSession({ client_config: data.client_config, user: { permissions: [] }, messages: [] });
      return data;
    });
  },
  setApiKey(apiKey) {
    logger("Set API key to: %s", apiKey);
    Auth.apiKey = apiKey;
  },
  getApiKey() {
    return Auth.apiKey;
  },
  requireSession() {
    logger("Requested authentication");
    if (Auth.isAuthenticated()) {
      return Promise.resolve(session);
    }
    return Auth.loadSession()
      .then(() => {
        if (Auth.isAuthenticated()) {
          logger("Loaded session");
          return session;
        }
        logger("Need to login, redirecting");
        Auth.login();
      })
      .catch(() => {
        logger("Need to login, redirecting");
        Auth.login();
      });
  },
};
