import { axios } from "@/services/axios";

const AlertCommands = {
  query: ({ alertId }) => axios.get(`api/alerts/${alertId}/commands`),
  create: data => axios.post(`api/alerts/${data.alert_id}/commands`, data),
  delete: data => axios.delete(`api/alerts/${data.alert_id}/commands/${data.id}`),
};
export default AlertCommands;
