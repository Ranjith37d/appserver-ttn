import { toUpper } from "lodash";
import React from "react";
import routeWithUserSession from "@/components/ApplicationArea/routeWithUserSession";
import Link from "@/components/Link";
import Button from "antd/lib/button";
import PageHeader from "@/components/PageHeader";
import Paginator from "@/components/Paginator";
import EmptyState from "@/components/empty-state/EmptyState";
import { wrap as itemsList, ControllerType } from "@/components/items-list/ItemsList";
import { ResourceItemsSource } from "@/components/items-list/classes/ItemsSource";
import { StateStorage } from "@/components/items-list/classes/StateStorage";
import AlertService from "@/services/alert";
import ItemsTable, { Columns } from "@/components/items-list/components/ItemsTable";
import { currentUser } from "@/services/auth";
import { ScheduleOutlined } from "@ant-design/icons";
import routes from "@/services/routes";
import Command from "@/services/command";
import DownlinkAlerDialogue from "./DownlinkAlertDialogue";

export const STATE_CLASS = {
  unknown: "label-warning",
  ok: "label-success",
  triggered: "label-danger",
};

class CommandList extends React.Component {
  static propTypes = {
    controller: ControllerType.isRequired,
  };

  listColumns = [
    // Columns.custom.sortable(
    //   (text, scheduler) => <i className={`fa fa-bell-${scheduler.options.muted ? "slash" : "o"} p-r-0`} />,
    //   {
    //     title: <i className="fa fa-bell p-r-0" />,
    //     field: "muted",
    //     width: "1%",
    //   }
    // ),
    Columns.custom.sortable(
      (text, command) => (
        <div>
          <Link className="table-main-title" href={"commands/" + command.id}>
            <ScheduleOutlined /> {command.name}
          </Link>
        </div>
      ),
      {
        title: "Name",
        field: "name",
      }
    ),
    // Columns.custom((text, item) => item.user.name, { title: "Created By", width: "1%" }),

    Columns.timeAgo.sortable({ title: "Last Updated At", field: "updated_at", width: "1%" }),
    Columns.dateTime.sortable({ title: "Created At", field: "created_at", width: "1%" }),
  ];

  render() {
    const { controller } = this.props;

    return (
      <div className="page-alerts-list">
        <div className="container">
          <PageHeader
            title={controller.params.pageTitle}
            actions={
              //   currentUser.hasPermission("list_alerts") ? (
              <Button
                block
                type="primary"
                onClick={() => {
                  DownlinkAlerDialogue.showModal();
                }}>
                <i className="fa fa-plus m-r-5" />
                Add Commands
              </Button>
              //   ) : null
            }
          />
          <div>
            {controller.isLoaded ? (
              <div className="table-responsive bg-white tiled">
                <ItemsTable
                  loading={!controller.isLoaded}
                  items={controller.pageItems}
                  columns={this.listColumns}
                  orderByField={controller.orderByField}
                  orderByReverse={controller.orderByReverse}
                  toggleSorting={controller.toggleSorting}
                />
                <Paginator
                  showPageSizeSelect
                  totalCount={controller.totalItemsCount}
                  pageSize={controller.itemsPerPage}
                  onPageSizeChange={itemsPerPage => controller.updatePagination({ itemsPerPage })}
                  page={controller.page}
                  onChange={page => controller.updatePagination({ page })}
                />
              </div>
            ) : null}
          </div>
        </div>
      </div>
    );
  }
}

const CommandListPage = itemsList(
  CommandList,
  () =>
    new ResourceItemsSource({
      isPlainList: true,
      getRequest() {
        return {};
      },
      getResource() {
        return Command.get.bind(Command);
      },
    }),
  () => new StateStorage({ orderByField: "created_at", orderByReverse: true, itemsPerPage: 20 })
);

routes.register(
  "Commands.List",
  routeWithUserSession({
    path: "/commands",
    title: "Commands",
    render: pageProps => <CommandListPage {...pageProps} currentPage="alerts" />,
  })
);
