import createTabbedEditor from "@/components/visualizations/editor/createTabbedEditor";
import GeneralSettings from "@/visualizations/components/GeneralSettings";
import RangeStateSettings from "./RangeIndicationSettings";
import FormatSettings from "@/visualizations/components/FormatSettings"
export default createTabbedEditor([

  { key: "General", title: "General", component: GeneralSettings },
  {key:"State",title:'State',component:RangeStateSettings},
  {key:'Format',title:'Format' ,component:FormatSettings }

]);
