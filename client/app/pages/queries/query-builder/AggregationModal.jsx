import React,{useState} from "react";
import Modal from "antd/lib/modal";
import { wrap as wrapDialog, DialogPropType } from "@/components/DialogWrapper";




const AggregationModal = props => {
  const [saveInProgress, setSaveInProgress] = useState(false);
  
 

  return (
    <>
        <Modal
        {...props.dialog.props}
        title="Aggregates Methods"
        okText="Save"
        cancelText="Close"
        okButtonProps={{
          // disabled: isValid,
          loading: saveInProgress,
          "data-test": "Aggregates Methods",
        }}
        cancelButtonProps={{
          disabled: saveInProgress,
        }}
        onOk={null}
        closable={!saveInProgress}
        maskClosable={!saveInProgress}
        wrapProps={{
          "data-test": "Aggregates Methods",
        }}>

        </Modal>
    </>
  );
};

AggregationModal.propTypes = {
  dialog: DialogPropType.isRequired,
};

export default wrapDialog(AggregationModal);
