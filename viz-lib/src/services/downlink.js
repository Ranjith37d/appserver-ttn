import notification from "antd/lib/notification";

export const Downlink = options => {
  var commandStatus = "";
  if (options.payloadState) {
    commandStatus = options.onPayload;
  } else {
    commandStatus = options.offPayload;
  }
  fetch(
    `${window.location.origin}/api/v3/as/applications/${options.channelName}/devices/${options.device_id}/down/replace`,
    {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: options.api_key,
      },
      body: JSON.stringify({
        downlinks: [{ f_port: 1, confirmed: false, frm_payload: commandStatus }],
      }),
    }
  )
    .then(response => response.json())
    .then(data =>
      notification["success"]({
        message: "success",
        description: "Downlink was scheduled successfully",
      })
    )
    .catch(err => {
      notification["error"]({
        message: "error",
      });
    });
  // .then(response => setResposeMessage(response));
  // setSwitchColor(!switchColor);
};
