import { axios } from "@/services/axios";

const Command = {
  get: () => axios.get("api/downlink-commands"),
  getScheduleslist: schedulerId => axios.get(`api/downlink-commands/${schedulerId}/schedules`),
  saveScheduler: (schedulerId, schedulerDetails) =>
    axios.post(`api/downlink-commands/${schedulerId}/schedules`, schedulerDetails),
  deleteSchedules: (dowlinkCommandId, schedulerId) =>
    axios.delete(`api/downlink-commands/${dowlinkCommandId}/schedules/${schedulerId}`),
  save: command =>
    axios.post("api/downlink-commands", {
      name: command.command_name,
      channel_id: command.channel_id,
      device_ids: command.device_ids,
      payload: command.payload,
      mode: command.mode,
      fport: command.fport,
      confirm: command.confirm,
    }),
  editCommands: ({ id, command_name, channel_id, device_ids, payload, fport, mode, confirm }) =>
    axios.post(`api/downlink-commands/${id}`, {
      name: command_name,
      channel_id: channel_id,
      device_ids: device_ids,
      payload: payload,
      mode: mode,
      fport: fport,
      confirm: confirm,
    }),
  deleteCommands: commandId => axios.delete(`api/downlink-commands/${commandId}`),
  getId: commandId => axios.get(`api/downlink-commands/${commandId}`),
  scheduler: (commandId, time, timeType) =>
    axios.post(`http://localhost:3000/command/${commandId}`, { type: timeType }, { time: time._d }),
};

export default Command;
