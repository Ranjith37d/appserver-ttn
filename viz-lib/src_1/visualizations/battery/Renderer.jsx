
import React, { useState, useEffect } from "react";
import { RendererPropTypes } from "@/visualizations/prop-types";
import "./battery.css";
import Text from "../text/Renderer";



export default function Renderer({ data, options, visualizationName }) {
  const [scale, setScale] = useState("1.00");
  const [container, setContainer] = useState(null);
  const [currentValue, setCurrentValue] = useState(0);
  const [batteryContainer, setBatteryContainer] = useState();
  const [textValueContainer, setTextvalueContainer] = useState();
  const [labelTextContainer, setLabelTextContainer] = useState();


  // Filter Value
  const filtervalue = () => {
    if (options.ParameterData !== undefined) {
      if (data.rows.length !== 0) {
        setCurrentValue(data.rows[0][options.ParameterData]);
        options.value = currentValue;
      } else {
        options.value = 0;
      }
    }
  };

  useEffect(() => {
    if (container) {
      if (options.ParameterData.length >= 1) {
        filtervalue();
        battteryPercentCalculation();
      }
    }
  }, [options, container, currentValue,batteryContainer]);

  const battteryPercentCalculation = () => {
    const BATTERY_MIN = 1;
    const BATTERY_MAX = 0;
    const RANGE_MIN = options.range[0];
    const RANGE_MAX = options.range[1];
    const BATTERY_FILL_PER_UNIT = (BATTERY_MIN - BATTERY_MAX) / (RANGE_MAX - RANGE_MIN);
    var currentVal = currentValue > RANGE_MAX ? RANGE_MAX : currentValue < RANGE_MIN ? RANGE_MIN : currentValue;

    const batteryFill = BATTERY_FILL_PER_UNIT * currentVal;

    // var state = options.states.find(s => s.value == contextValue)
    var state = options.states.find(value => value.from <= currentValue && value.to >= currentValue);
    if (!(batteryContainer == null || batteryContainer == undefined)) {
      if (options.range[0] < options.range[1]) {
        if (batteryFill <= 1) {
          batteryContainer.style.transform = `scaleY(${batteryFill})`;
        } else {
          batteryContainer.style.transform = `scaleY(1)`;
        }
        if (!(state == null || state == undefined)) {
          batteryContainer.style.fill = state.color;
          textValueContainer.style.color = state.color;
          labelTextContainer.style.color = state.color;
        } else {
          batteryContainer.style.transform = `scaleY(${batteryFill})`;

          batteryContainer.style.fill = "#43B05C";
          textValueContainer.style.color = "#43B05C";
          labelTextContainer.style.color = "#43B05C";
        }
      } else {
        batteryContainer.style.transform = `scaleY(0)`;
      }
    }
  };

  return (
    <>
      <div className="bulb-viz-container">
        <div className="counter-layout">
            { options.showIcon &&
          <div className="column col-main">
            <svg version="1.1" id="Layer_1" x="0px" y="0px" viewBox="0 0 640 480">
              <g>
                <g>
                  <path
                    className="st0"
                    d="M249.8,165c3.4,0,6.2,2.8,6.2,6.2v163.1c0,5.4-4.4,9.7-9.7,9.7h-75.5c-5.4,0-9.7-4.4-9.7-9.7V174.7
    	            	c0-5.4,4.4-9.7,9.7-9.7H249.8 M249.8,164h-79.1c-5.9,0-10.7,4.8-10.7,10.7v159.5c0,5.9,4.8,10.7,10.7,10.7h75.5
    	            	c5.9,0,10.7-4.8,10.7-10.7v-163C257,167.2,253.8,164,249.8,164L249.8,164z"
                  />
                </g>
                <g>
                  <rect x="189.5" y="154.5" className="st1" width="40" height="9" />
                  <path className="st0" d="M229,155v8h-39v-8H229 M230,154h-41v10h41V154L230,154z" />
                </g>
                <g>
                  <path
                    ref={setBatteryContainer}
                    className="st2"
                    d="M243.4,339h-70.7c-3.7,0-6.6-3-6.6-6.6V177.6c0-3.7,3-6.6,6.6-6.6h70.7c3.7,0,6.6,3,6.6,6.6v154.7
    	            	C250,336,247,339,243.4,339z"
                  />
                </g>
              </g>
            </svg>
          </div>
}

          <div className="column col-complementary" role="complementary">
            {options && (
              <Text
                setlabelTextContainer={setLabelTextContainer}
                settextValueContainer={setTextvalueContainer}
                setcontainer={setContainer}
                setscale={setScale}
                options={options}
                visualizationName={visualizationName}
                data={data}
                container={container}
                scale={scale}
                currentLabel={options.labelName}
              />
            )}
          </div>
        </div>
      </div>
    </>
  );
}

Renderer.propTypes = RendererPropTypes;
