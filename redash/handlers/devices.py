import time
from flask import request
from flask_restful import abort
from redash import models
from redash.permissions import require_admin, require_permission
from redash.handlers.base import BaseResource, get_object_or_404, require_fields
from sqlalchemy.exc import IntegrityError



class DeviceListResource(BaseResource):
    # @require_permission("list_data_sources")
    def get(self, channel_id):
        if self.current_user.has_permission("admin"):
            response = models.Device.all()
        else:
            channel = models.Channel.by_channel_id(channel_id, self.current_user)
            if channel is None:
                 abort(
                    400,
                    message="Channel with id {} does not exist.".format(channel_id),
                )

            response = models.Device.by_channel(channel.id, self.current_user)
        
        return [d.to_dict() for d in response]

    # @require_admin
    def post(self, channel_id):
        req = request.get_json(True)
        require_fields(req, ("device_id","device_name"))

        try:
            channel = models.Channel.by_channel_id(channel_id, self.current_user)
            if channel is None:
                abort(
                    400,
                    message="Channel with id {} does not exist.".format(channel_id),
                )
            device = models.Device.create(channel.id, req['device_id'], req['device_name'], self.current_user)           

        except IntegrityError as e:
            if req["device_id"] in str(e):
                abort(
                    400,
                    message="Device with the id {} already exists.".format(
                        req["device_id"]
                    ),
                )

            abort(400)

        self.record_event(
            {
                "action": "create",
                "object_id": device.id,
                "object_type": "device",
            }
        )

        return device.to_dict()

class DeviceResource(BaseResource):
    def get(self, channel_id, device_id):
        channel = models.Channel.by_channel_id(channel_id, self.current_user)
        if channel is None:
            abort(
                400,
                message="Channel with id {} does not exist.".format(channel_id),
            )
        response = models.Device.by_channel_device_id(channel.id, device_id, self.current_user)
        return response.to_dict()

    def post(self, channel_id, device_id):
        req = request.get_json(True)
        require_fields(req, ("device_name",))

        try:
            channel = models.Channel.by_channel_id(channel_id, self.current_user)
            if channel is None:
                abort(
                    400,
                    message="Channel with id {} does not exist.".format(channel_id),
                )

            device = models.Device.update(channel.id, device_id, req['device_name'], self.current_user)
        except IntegrityError as e:
            if req["device_name"] in str(e):
                logger.debug(str(e))
                abort(
                    400,
                    message="Unable to update device information."
                    ),

            abort(400)

        self.record_event(
            {
                "action": "update",
                "object_id": device.id,
                "object_type": "device",
            }
        )

        return device.to_dict()
    
    def delete(self, channel_id, device_id):
        # channel = models.query.filter(cls.channel_id == channel_id, cls.user_id == user.id, cls.is_deleted == False).first()
        # if channel is None:
        #     return
        return models.Device.delete(device_id ,self.current_user)
