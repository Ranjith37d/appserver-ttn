import createTabbedEditor from "@/components/visualizations/editor/createTabbedEditor";
import GenralSettings from "./GenralSettings";
import RangeStateSettings from "./RangeIndiactionSettings";
import FormatSettings from "@/visualizations/components/FormatSettings"
export default createTabbedEditor([
  { key: "General", title: "General", component: GenralSettings },
  { key: "state", title: "State", component: RangeStateSettings },
  {key:"Format" , title:"Format" ,component:FormatSettings},
]);
