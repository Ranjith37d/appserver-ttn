import createTabbedEditor from "@/components/visualizations/editor/createTabbedEditor";
import RangeStateSettings from "@/visualizations/components/RangeIndiactionSettings";
import GenralSettings from "./GeneralSettings";
export default createTabbedEditor([
  { key: "General", title: "General", component: GenralSettings },
  // {key:"State",title:"State" ,component:RangeStateSettings},
]);
