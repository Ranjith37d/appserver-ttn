import React, { useState, useEffect, useRef } from "react";
import Select from "antd/lib/select";
import { Row, Col, Space, Typography, Button, Table } from "antd";
import { getChannels, getDevices, getDeviceAttributes, buildsQuery } from "../../../services/query-result";
import { Form } from "antd";
import { trim } from "lodash";
import Modal from "antd/lib/modal";
import Input from "antd/lib/input";
import DynamicComponent from "@/components/DynamicComponent";
import { wrap as wrapDialog, DialogPropType } from "@/components/DialogWrapper";
import navigateTo from "@/components/ApplicationArea/navigateTo";
import recordEvent from "@/services/recordEvent";
import { policy } from "@/services/policy";
import { QueryService } from "@/services/query";
import notification from "@/services/notification";

const { Text } = Typography;

const QueryBuilder = props => {
  const [name, setName] = useState("");
  const [form] = Form.useForm();
  const formRef = useRef();
  const [isValid, setIsValid] = useState(false);
  const [saveInProgress, setSaveInProgress] = useState(false);
  const [channels, setChannels] = useState([]);
  const [devices, setDevices] = useState([]);
  const [deviceAttributes, setDeviceAttributes] = useState([]);
  const [editdeviceAttributes, editsetDeviceAttributes] = useState([]);

  const [selectedChannel, setSelectedChannel] = useState("");
  const [selectedDevices, setSelectedDevices] = useState([]);
  const [selectedDeviceAttributes, setSelectedDeviceAttributes] = useState([]);
  const [conditions, setConditions] = useState([]);

  const Device = React.useMemo(() => {
    if (selectedChannel) {
      getDevices(selectedChannel).then(data => {
        setDevices(data);
      });
    }
  }, [selectedChannel]);

  useEffect(() => {
    if (channels && channels.length == 0) {
      getChannels().then(data => setChannels(data));
    }
  }, []);

  useEffect(() => {
    if (selectedChannel) {
      getDevices(selectedChannel).then(data => {
        setDevices(data);
      });
      formRef.current.setFieldsValue({ Devices: [] });
      formRef.current.setFieldsValue({ Attributes: [] });

      // .current!.setFieldsValue({ note: 'Hi, man!' });
      setSelectedDevices([]);
    }
  }, [selectedChannel]);

  useEffect(() => {
    if (selectedChannel && selectedDevices.length) {
      getDeviceAttributes(selectedChannel, selectedDevices).then(data => {
        setDeviceAttributes(data);
      });

      setSelectedDeviceAttributes([]);
    }
    formRef.current.setFieldsValue({ Attributes: [] });
  }, [selectedDevices]);

  // useEffect(() => {
  //   selectedChannel && selectedDevices && props.editQuery
  //     ? props.onAdd(buildsQuery(selectedChannel, selectedDevices, editdeviceAttributes, props.QueryParameter))
  //     : props.onAdd(buildsQuery(selectedChannel, selectedDevices, selectedDeviceAttributes, props.QueryParameter));
  // }, [selectedChannel, selectedDevices, selectedDeviceAttributes, editdeviceAttributes]);

  //=====
  function handleNameChange(event) {
    const value = trim(event.target.value);
    setName(value);
  }

  function save() {
    if (
      name.length >= 1 &&
      selectedChannel.length >= 1 &&
      selectedDevices.length >= 1 &&
      selectedDeviceAttributes.length >= 1
    ) {
      QueryService.add({
        name: name,
        channel_id: selectedChannel,
        device_ids: selectedDevices,
        attributes: selectedDeviceAttributes,
      }).then(data => {
        notification.success("Query created Successfully");
        props.dialog.close();
        navigateTo(`/console/app/queries/` + data.id);
      });
    } else {
      notification.error("Fill All the values to continue");
    }

    setIsValid(false);
  }
  const layout = {
    labelCol: {
      span: 6,
    },
    wrapperCol: {
      span: 16,
    },
  };

  return (
    <>
      <Modal
        {...props.dialog.props}
        title="New Query"
        okText="Save"
        cancelText="Close"
        okButtonProps={{
          disabled: isValid,
          loading: saveInProgress,
          "data-test": "QuerySaveButton",
        }}
        cancelButtonProps={{
          disabled: saveInProgress,
        }}
        onOk={save}
        closable={!saveInProgress}
        maskClosable={!saveInProgress}
        wrapProps={{
          "data-test": "CreateQueryDialog",
        }}>
        <Form {...layout} form={form} ref={formRef}>
          <Form.Item
            name="Query name"
            label="Query Name"
            rules={[
              {
                required: true,
              },
            ]}>
            <Input
              defaultValue={name}
              onChange={handleNameChange}
              onPressEnter={save}
              placeholder=" Query Name "
              disabled={saveInProgress}
              autoFocus
            />
          </Form.Item>

          <Form.Item
            name="Channel"
            label="Channel"
            rules={[
              {
                required: true,
              },
            ]}>
            <Select
              className="gutter-row"
              style={{ width: "100%", border: "none", borderRadius: "2px" }}
              placeholder="Select Channel"
              optionFilterProp="children"
              value={selectedChannel}
              onChange={setSelectedChannel}
              filterOption={(input, option) => option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}>
              {channels &&
                channels.map(channel => {
                  return (
                    <Select.Option key={channel.channel_id} value={channel.channel_id}>
                      {channel.channel_name}
                    </Select.Option>
                  );
                })}
            </Select>
          </Form.Item>
          <Form.Item
            name="Devices"
            label="Devices"
            rules={[
              {
                required: true,
              },
            ]}>
            <Select
              style={{ width: "100%", border: "none", borderRadius: "2px" }}
              placeholder="Select Device"
              onChange={(e)=>setSelectedDevices([e])}
              value={selectedDevices}
              filterOption={(input, option) => option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}>
              {devices &&
                devices.map(device => {
                  return (
                    <Select.Option key={device.device_id} value={device.device_id}>
                      {device.device_name}
                    </Select.Option>
                  );
                })}
            </Select>
          </Form.Item>
          <Form.Item
            name="Attributes"
            label="Attributes"
            rules={[
              {
                required: true,
              },
            ]}>
            <Select
              mode="multiple"
              placeholder="Select Attributes"
              style={{ width: "100%" }}
              // onFocus={()=>{props.editQuery=false}}
              onChange={setSelectedDeviceAttributes}
              value={selectedDeviceAttributes}>
              {deviceAttributes &&
                deviceAttributes.map(item => (
                  <Select.Option key={item} value={item}>
                    {item}
                  </Select.Option>
                ))}
            </Select>
          </Form.Item>
        </Form>
      </Modal>
    </>
  );
};

QueryBuilder.propTypes = {
  dialog: DialogPropType.isRequired,
};

export default wrapDialog(QueryBuilder);
