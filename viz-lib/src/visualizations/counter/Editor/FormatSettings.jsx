import React from "react";
import { map } from "lodash";

import { Section, Input, InputNumber, Select } from "@/components/visualizations/editor";
import { EditorPropTypes } from "@/visualizations/prop-types";


export default function FormatSettings({ options, data, onOptionsChange }) {
  const inputsEnabled = true
  return (
    <React.Fragment>
      <Section>
        <Select
          layout="horizontal"
          label="Select Data Type"
          data-test="Counter.General.ValueColumn"
          defaultValue={options.counterDataType}
       
          onChange={counterDataType => onOptionsChange({ counterDataType })}>
          {map(["text","number"], col => (
            <Select.Option key={col} >
              {col}
            </Select.Option>
          ))}
        </Select>
      </Section>
      <Section>
        <InputNumber
          layout="horizontal"
          label="Formatting Decimal Place"
          data-test="Counter.Formatting.DecimalPlace"
          defaultValue={options.stringDecimal}
          disabled={!inputsEnabled}
          onChange={stringDecimal => onOptionsChange({ stringDecimal })}
        />
      </Section>

      <Section>
        <Input
          layout="horizontal"
          label="Formatting Decimal Character"
          data-test="Counter.Formatting.DecimalCharacter"
          defaultValue={options.stringDecChar}
          disabled={!inputsEnabled}
          onChange={e => onOptionsChange({ stringDecChar: e.target.value })}
        />
      </Section>

      <Section>
        <Input
          layout="horizontal"
          label="Formatting Thousands Separator"
          data-test="Counter.Formatting.ThousandsSeparator"
          defaultValue={options.stringThouSep}
          disabled={!inputsEnabled}
          onChange={e => onOptionsChange({ stringThouSep: e.target.value })}
        />
      </Section>

      <Section>
        <Input
          layout="horizontal"
          label="Formatting String Prefix"
          data-test="Counter.Formatting.StringPrefix"
          defaultValue={options.stringPrefix}
          disabled={!inputsEnabled}
          onChange={e => onOptionsChange({ stringPrefix: e.target.value })}
        />
      </Section>

      <Section>
        <Input
          layout="horizontal"
          label="Formatting String Suffix"
          data-test="Counter.Formatting.StringSuffix"
          defaultValue={options.stringSuffix}
          disabled={!inputsEnabled}
          onChange={e => onOptionsChange({ stringSuffix: e.target.value })}
        />
      </Section>

      {/* <Section>
        <Switch
          data-test="Counter.Formatting.FormatTargetValue"
          defaultChecked={options.formatTargetValue}
          onChange={formatTargetValue => onOptionsChange({ formatTargetValue })}>
          Format Target Value
        </Switch>
      </Section> */}
    </React.Fragment>
  );
}

FormatSettings.propTypes = EditorPropTypes;
