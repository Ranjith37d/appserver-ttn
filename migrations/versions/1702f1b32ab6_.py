"""empty message

Revision ID: 1702f1b32ab6
Revises: f49ce35ab50d
Create Date: 2021-01-21 15:28:44.233931

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '1702f1b32ab6'
down_revision = 'f49ce35ab50d'
branch_labels = None
depends_on = None


def upgrade():
    pass


def downgrade():
    pass
