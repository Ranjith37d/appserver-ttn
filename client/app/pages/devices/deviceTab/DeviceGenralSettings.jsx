import React, { useState, useEffect } from "react";
import { Card, Col, Input, Button, Row, Space, Form, Popconfirm } from "antd";
import navigateTo from "@/components/ApplicationArea/navigateTo";
import notification from "@/services/notification";
import { ChannelConsumer } from "../DeviceChannelOverview";
import { Device } from "@/services/device";
import InputWithCopy from "@/components/InputWithCopy";

export const DeviceGenralSettings = props => {
  const [deviceDetails, setdeviceDetails] = useState("");
  const [deviceName, setdeviceName] = useState("");
  const [saveChangesValid, setSaveChangesValid] = useState(true);
  const [publichChannelTopic, setPublishChannelTopic] = useState("");
  const [subscribeChannelTopic, setSubscribeChannelTopic] = useState("");

  useEffect(() => {
    Device.deviceDetails(props.channelId, props.id).then(device => {
      setdeviceDetails(device);
      setdeviceName(device.name);
      setPublishChannelTopic(`channel/${props.channelId}/device/${props.id}/up`);
      setSubscribeChannelTopic(`channel/${props.channelId}/device/${props.id}/down`);
    });
  }, []);

  const layout = {
    labelCol: { span: 4 },
    wrapperCol: { span: 8 },
  };
  const tailLayout = {
    wrapperCol: { offset: 4, span: 16 },
  };

  //HandleNameChanges
  const handleNameChange = e => {
    const newDeviceName = e.target.value;

    if (e.target.value !== deviceName) {
      setSaveChangesValid(false);
      setdeviceDetails(pre => {
        return { ...pre, name: newDeviceName };
      });
    } else {
      setdeviceDetails(pre => {
        return { ...pre, name: deviceName };
      });
      setSaveChangesValid(true);
    }
  };

  return (
    <>
      <Form {...layout} name="basic" initialValues={{ remember: true }}>
        <Form.Item label="device Id" className="m-b-10">
          <InputWithCopy
            id="deviceId"
            className="hide-in-percy"
            value={deviceDetails.device_id}
            data-test="deviceId"
            readOnly
          />
        </Form.Item>
        <Form.Item label="Device Name">
          <Input value={deviceDetails.name} onChange={handleNameChange} placeholder="Channel Name" readOnly />
        </Form.Item>
        <Form.Item label="API Key" className="m-b-10">
          <InputWithCopy
            id="apiKey"
            className="hide-in-percy"
            value={deviceDetails.api_key}
            data-test="ApiKey"
            readOnly
          />
        </Form.Item>
        <Form.Item label="Publish Topic" className="m-b-10">
          <InputWithCopy
            id="apiKey"
            className="hide-in-percy"
            value={publichChannelTopic}
            data-test="ApiKey"
            readOnly
          />
        </Form.Item>
        <Form.Item label="Subscribe Topic" className="m-b-10">
          <InputWithCopy
            id="apiKey"
            className="hide-in-percy"
            value={subscribeChannelTopic}
            data-test="ApiKey"
            readOnly
          />
        </Form.Item>
        <Form.Item label="Device Created At ">
          <Input value={deviceDetails.created_at} readOnly />
        </Form.Item>
        {/* <Form.Item {...tailLayout}>
          <Button type="primary" disabled={saveChangesValid}>
            Save Changes
          </Button>
        </Form.Item> */}
        <Form.Item {...tailLayout}>
          <Popconfirm
            title={`Are you sure you want to delete ${'"' + deviceDetails.name + '"' + "\t" + "?"}`}
            onConfirm={() => {
              Device.deviceDelete(props.channelId, props.id)
                .then(() => {
                  notification.success("Device deleted");
                  navigateTo("mqtt/" + props.channelId);
                })
                .catch(e => notification.error(e.response.data.message));
            }}
            okText="Yes"
            cancelText="No">
            <Button danger>Delete</Button>
          </Popconfirm>
        </Form.Item>
      </Form>
    </>
  );
};
