import { isEqual, map, find, fromPairs } from "lodash";
import React, { useState, useMemo, useEffect, useRef } from "react";
import PropTypes from "prop-types";
import useQueryResultData from "@/lib/useQueryResultData";
import useImmutableCallback from "@/lib/hooks/useImmutableCallback";
import Filters, { FiltersType, filterData } from "@/components/Filters";
import { VisualizationType } from "@redash/viz/lib";
import { Renderer } from "@/components/visualizations/visualizationComponents";
import { getChannels, getDevices } from "../../services/query-result";
import { Button } from "antd";
import { useSocket, useEmitEvent, useSocketSelector } from "react-socket-io-hooks";

function combineFilters(localFilters, globalFilters) {
  // tiny optimization - to avoid unnecessary updates
  if (localFilters.length === 0 || globalFilters.length === 0) {
    return localFilters;
  }

  return map(localFilters, localFilter => {
    const globalFilter = find(globalFilters, f => f.name === localFilter.name);
    if (globalFilter) {
      return {
        ...localFilter,
        current: globalFilter.current,
      };
    }
    return localFilter;
  });
}

function areFiltersEqual(a, b) {
  if (a.length !== b.length) {
    return false;
  }

  a = fromPairs(map(a, item => [item.name, item]));
  b = fromPairs(map(b, item => [item.name, item]));

  return isEqual(a, b);
}

export default function WebsocketVisualizationRenderer(props) {
  const data = useQueryResultData(props.queryResult);
  const [filters, setFilters] = useState(() => combineFilters(data.filters, props.filters)); // lazy initialization
  const filtersRef = useRef();
  const [channels, setChannels] = useState(null);
  const [subscriptionList, setSubscriptionList] = useState()




  filtersRef.current = filters;


  const handleFiltersChange = useImmutableCallback(newFilters => {
    if (!areFiltersEqual(newFilters, filters)) {
      setFilters(newFilters);
      props.onFiltersChange(newFilters);
    }
  });

  // Reset local filters when query results updated
  useEffect(() => {
    handleFiltersChange(combineFilters(data.filters, props.filters));
  }, [data.filters, props.filters, handleFiltersChange]);

  // Update local filters when global filters changed.
  // For correct behavior need to watch only `props.filters` here,
  // therefore using ref to access current local filters
  useEffect(() => {
    handleFiltersChange(combineFilters(filtersRef.current, props.filters));
  }, [props.filters, handleFiltersChange]);



  const { showFilters, visualization } = props;

  let options = { ...visualization.options };

  // define pagination size based on context for Table visualization
  if (visualization.type === "TABLE") {
    options.paginationSize = props.context === "widget" ? "small" : "default";
  }
  //======================== =======Websocket =====================
  const socket = useSocket()

  const CHANNEL_ID = options.channelid
  const DEVICE_ID = options.device_ids[0]
  var topic = `v3/${CHANNEL_ID}/devices/${DEVICE_ID}/uplink`;
  const websocketData = useSocketSelector(state => state[topic])

  const filteredData = useMemo(
    () => ({
      columns: data.columns,
      rows: websocketData ? [websocketData]: [],
      
    }),
    [data,  websocketData]
  );



  useEffect(() => {
    socket.emit("start", CHANNEL_ID, DEVICE_ID)
  }, [])

  //==================================================================


  return (
    <>
   
      <Renderer
        key={`visualization${visualization.id}`}
        type={visualization.type}
        options={options}
        data={filteredData}
        visualizationName={visualization.name}
        addonBefore={showFilters && <Filters filters={filters} onChange={handleFiltersChange} />}
      />
    
    </>
  );
}

WebsocketVisualizationRenderer.propTypes = {
  visualization: VisualizationType.isRequired,
  queryResult: PropTypes.object.isRequired, // eslint-disable-line react/forbid-prop-types
  showFilters: PropTypes.bool,
  filters: FiltersType,
  onFiltersChange: PropTypes.func,
  context: PropTypes.oneOf(["query", "widget"]).isRequired,
};

WebsocketVisualizationRenderer.defaultProps = {
  showFilters: true,
  filters: [],
  onFiltersChange: () => { },
};
