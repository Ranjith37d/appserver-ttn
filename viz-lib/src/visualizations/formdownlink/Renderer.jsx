import { isFinite } from "lodash";
import React, { useState, useEffect } from "react";
import cx from "classnames";
import resizeObserver from "@/services/resizeObserver";
import { RendererPropTypes } from "@/visualizations/prop-types";
import Button from "antd/lib/button";
import { notification } from "antd";
import moment from "moment"

function getCounterStyles(scale) {
  return {
    msTransform: `scale(${scale})`,
    MozTransform: `scale(${scale})`,
    WebkitTransform: `scale(${scale})`,
    transform: `scale(${scale})`,
  };
}

function getCounterScale(container) {
  const inner = container.firstChild;

  const scale = Math.min(container.offsetWidth / inner.offsetWidth, container.offsetHeight / inner.offsetHeight);
  return Number(isFinite(scale) ? scale : 1).toFixed(2); // keep only two decimal places
}

export default function Renderer({ data, options, visualizationName }) {
  const [scale, setScale] = useState("1.00");
  const [container, setContainer] = useState(null);
  const [loading , setLoading] = useState()

  useEffect(() => {
    if (container) {
      const unwatch = resizeObserver(container, () => {
        setScale(getCounterScale(container));
      });
      return unwatch;
    }
  }, [container, options]);

  useEffect(() => {
    if (container) {
      // update scaling when options or data change (new formatting, values, etc.
      // may change inner container dimensions which will not be tracked by `resizeObserver`);
      setScale(getCounterScale(container));
    }
  }, [data, options, container]);

  const sendDownLink = () => {

    if(options.commandId.toString().length>1){
    setLoading(5000)
    var time =()=>{  setTimeout(()=>{
      fetch(`api/downlink-commands/${options.commandId}/schedules`, {
        method: "post",
        body: JSON.stringify({
          name: moment()
          .utc()
          .format("YYYY-MM-DD HH:mm:00"),
          type: "o",
          timezone: "Asia/Kolkata",
          schedule: moment()
            .utc()
            .format("YYYY-MM-DD HH:mm:00"),
        }),
      })
        .then(data =>
          notification["success"]({ message: "Success", description: "Command scheduled", placement: "bottomRight" })
          )
          .catch(error =>
            notification["error"]({
              message: "Error",
              description: "Error  while  scheduling Downlink",
              placement: "bottomRight",
            })
            );
            setLoading(false)


    },5000)
  }
    time()
  clearTimeout(time)
}
  };

  return (
    <div className={cx("counter-visualization-container", {})}>
      <div className="counter-visualization-content" ref={setContainer}>
        <div style={getCounterStyles(scale)}>
          <div className="counter-visualization-value">{options.commandName}</div>

          <div className="counter-visualization-label" style={{paddingTop:'20px'}} >
            <Button type="primary"  loading={loading} onClick={sendDownLink}>send</Button>
          </div>
        </div>
      </div>
    </div>
  );
}

Renderer.propTypes = RendererPropTypes;

