import { Renderer } from "./DownlinkWidget";
import Editor from "./Editor";
import { chain } from "lodash";
const DEFAULT_OPTIONS = {
  channelName: "",
  device_id: "",
  api_key: "",

  ONpayload: "",
  OFFpayload: "",
  switchState: true,

  // confirmed: false,
  // deviceAttribute: "",
  // columnValue: "",
  // condition: "",
  // thershold: "",
};

export default {
  type: "switch",
  name: "switch",
  getOptions: options => ({ ...DEFAULT_OPTIONS, ...options }),
  Renderer,
  Editor,
};
