import { isFinite, transform } from "lodash";
import React, { useState, useEffect } from "react";
import resizeObserver from "@/services/resizeObserver";
import { RendererPropTypes } from "@/visualizations/prop-types";
import Text from "../text/Renderer";
import "./render.less";

function getCounterScale(container) {
  const inner = container.firstChild;
  const scale = Math.min(container.offsetWidth / inner.offsetWidth, container.offsetHeight / inner.offsetHeight);
  return Number(isFinite(scale) ? scale : 1).toFixed(2); // keep only two decimal places
}

export default function Renderer({ data, type, options, visualizationName }) {
  const [scale, setScale] = useState("1.00");
  const [container, setContainer] = useState(null);
  const [currentValue, setCurrentValue] = useState(0);
  const [labelTextContainer, setLabelTextContainer] = useState();
  const [labelContainer,setLabelContainer] = useState()
  const [textValueContainer, setTextvalueContainer] = useState();
  const [currentLabel, setCurentLabel] = useState("");
  const [state, setState] = useState(options.states);

  // Filter Value
  const filtervalue = () => {
    if (options.ParameterData !== undefined) {
      if (data.rows.length !== 0) {
        setCurrentValue(data.rows[0][options.ParameterData]);
        options.value = currentValue;
      } else {
        options.value = 0;
      }
    }
  };

  useEffect(() => {
    if (container) {
      if (options.ParameterData.length >= 1) {
        filtervalue();
        applyConfig();
      }
    }
  }, [options, container, currentValue]);
  useEffect(() => {
    if (container) {
      const unwatch = resizeObserver(container, () => {
        setScale(getCounterScale(container));
      });
      return unwatch;
    }
  }, [container]);

  useEffect(() => {
    if (container) {
      setScale(getCounterScale(container));
    }
  }, [data, options, container]);

  const applyConfig = () => {
    var filterState = options.states.find(s => s.value == currentValue);
    // if (filterState) {
    //   setState(filterState);
    // }

    textValueContainer.style.color = filterState ? filterState.color : "#000";
    labelTextContainer.style.color = filterState ? filterState.color : "#000";
    labelContainer.style.color= filterState ? filterState.color : "#000";
    setCurentLabel(filterState ? filterState.label : "");
  };

  return (
    <>
      <div className="bulb-viz-container">
        <div className="counter-layout">
            {options.showIcon && (
          <div className="column col-main">
              <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" viewBox="0 0 640 480">
                <path className="parking0" d="M176.5,268.6H469.5v76.1H176.5V268.6z" />
                <path className="parking1" d="M176.5,277.2H469.5v58.9H176.5V277.2z" />
                <rect x="215.9" y="303.39" className="parking2" width="22.26" height="2.94" />
                <rect x="248.15" y="303.39" className="parking2" width="24.26" height="3.2" />
                <rect x="295.99" y="303.39" className="parking2" width="24.26" height="3.2" />
                <rect x="342.69" y="303.39" className="parking2" width="24.26" height="3.2" />
                <rect x="381.35" y="303.39" className="parking2" width="24.26" height="3.2" />
                <rect x="423.92" y="303.39" className="parking2" width="24.26" height="3.2" />
                {options.states[1].value == currentValue && (
                  <g>
                    <path
                      className="parking3"
                      d="M326.17,174.64h-18.17c-1.33,0-2.66,0.26-3.85,0.84c-3.1,1.5-4.93,4.58-4.92,7.84v59.34
		c0,3.51,2.84,6.35,6.35,6.35h4.59c3.51,0,6.35-2.84,6.35-6.35v-13.59c3.57-0.02,7.42-0.03,9.65-0.03c15.19,0,27.56-12.2,27.56-27.2
		C353.72,186.84,341.37,174.64,326.17,174.64L326.17,174.64z M326.17,211.73c-2.23,0-6.02,0.02-9.57,0.03
		c-0.02-3.68-0.05-15.98-0.05-19.83h9.63c5.56,0,10.26,4.53,10.26,9.9C336.43,207.2,331.73,211.73,326.17,211.73z"
                    />
                  </g>
                )}
                <path
                  d="M326.17,171.2h-18.17c-1.93,0-3.73,0.4-5.35,1.18c-4.24,2.06-6.88,6.24-6.86,10.93v59.34c0,5.4,4.39,9.79,9.79,9.79h4.59
	c5.4,0,9.79-4.39,9.79-9.79v-10.18c2.42-0.01,4.69-0.02,6.22-0.02c17.09,0,30.99-13.74,30.99-30.63S343.26,171.2,326.17,171.2
	L326.17,171.2z M326.17,225.6c-2.24,0-6.09,0.02-9.67,0.03c-1.89,0.01-3.42,1.54-3.42,3.43v13.59c0,1.61-1.31,2.92-2.92,2.92h-4.59
	c-1.61,0-2.92-1.31-2.92-2.92V183.3c0-2.03,1.14-3.84,2.98-4.73c0.67-0.33,1.49-0.49,2.36-0.49h18.17
	c13.3,0,24.13,10.66,24.13,23.76S339.47,225.6,326.17,225.6L326.17,225.6z"
                />
                <path
                  d="M326.17,188.5h-9.62c-0.91,0-1.79,0.36-2.43,1.01c-0.65,0.65-1,1.52-1,2.44l0.01,4.34c0.01,5.25,0.03,12.76,0.05,15.5
	c0.01,1.89,1.54,3.42,3.43,3.42h0.02c3.54-0.02,7.33-0.03,9.55-0.03c7.43,0,13.7-6.11,13.7-13.33
	C339.87,194.61,333.59,188.5,326.17,188.5z M326.17,208.3c-1.52,0-3.76,0.01-6.15,0.02c-0.01-3.48-0.02-8.34-0.03-12.05v-0.9h6.18
	c3.64,0,6.83,3.02,6.83,6.46C333,205.28,329.81,208.3,326.17,208.3z"
                />
                {options.states[0].value == currentValue && (
                  <g>
                    <path
                      className="parking4"
                      d="M344.88,255.39h-36.43c-5.88,0-10.64-4.77-10.64-10.64v-72.71c0-5.88,4.77-10.64,10.64-10.64h36.43
		c5.88,0,10.64,4.77,10.64,10.64v72.71C355.52,250.62,350.76,255.39,344.88,255.39z"
                    />
                    <path
                      className="parking5"
                      d="M316.84,255.39h-8.39c-5.88,0-10.64-4.76-10.64-10.64v-72.71c0-5.88,4.76-10.64,10.64-10.64h8.39
		c-5.88,0-10.64,4.76-10.64,10.64v72.71C306.19,250.62,310.96,255.39,316.84,255.39z"
                    />
                    <path
                      className="parking3"
                      d="M355.52,187.71l0,52c-11.76,4.34-24.26,5.94-36.56,4.79l0,0c-7.18-0.66-14.29-2.26-21.15-4.79v-52
		c6.97-3.37,14.37-5.46,21.89-6.25c2.32-0.25,4.64-0.37,6.97-0.37C336.53,181.08,346.41,183.29,355.52,187.71L355.52,187.71
		L355.52,187.71z"
                    />
                    <path
                      className="parking6"
                      d="M318.96,244.49c-7.18-0.66-14.29-2.26-21.15-4.79v-52c6.97-3.37,14.37-5.46,21.89-6.25
		C311.67,182.45,308.13,240.42,318.96,244.49z"
                    />
                    <path
                      className="parking4"
                      d="M335.28,230.66h-17.25c-5.12,0-9.28-4.15-9.28-9.28v-17.25c0-5.12,4.15-9.28,9.28-9.28h17.25
		c5.12,0,9.28,4.15,9.28,9.28v17.25C344.56,226.51,340.41,230.66,335.28,230.66z"
                    />
                    <path
                      className="st5"
                      d="M325.13,230.65h-7.09c-5.12,0-9.28-4.15-9.28-9.27v-17.25c0-5.12,4.16-9.28,9.28-9.28h7.09
		c-5.12,0-9.28,4.16-9.28,9.28v17.25C315.86,226.51,320.01,230.65,325.13,230.65z"
                    />
                  </g>
                )}
                {options.states[0].value == currentValue && (
                  <path
                    d="M344.88,158.27h-36.43c-7.59,0-13.78,6.18-13.78,13.78v72.71c0,7.59,6.18,13.78,13.78,13.78h36.43
	c7.59,0,13.78-6.18,13.78-13.78v-72.71C358.65,164.45,352.48,158.27,344.88,158.27L344.88,158.27z M308.45,164.54h36.43
	c4.14,0,7.5,3.36,7.5,7.5v10.87c-8.16-3.25-16.95-4.96-25.72-4.96c-8.77,0-17.57,1.71-25.72,4.96v-10.87
	C300.94,167.9,304.31,164.54,308.45,164.54z M311.9,221.38v-17.25c0-3.38,2.76-6.14,6.14-6.14h17.25c3.38,0,6.14,2.75,6.14,6.14
	v17.25c0,3.38-2.76,6.14-6.14,6.14h-17.25C314.65,227.52,311.9,224.77,311.9,221.38z M342.96,194.4c-2.12-1.67-4.77-2.66-7.67-2.66
	h-17.25c-2.89,0-5.56,1-7.67,2.66l-6.75-5.82c14.87-5.86,31.22-5.86,46.09,0L342.96,194.4z M306.57,199.4
	c-0.6,1.46-0.94,3.06-0.94,4.74v5.49h-4.69v-15.08L306.57,199.4z M300.94,215.9h4.69v5.48c0,2,0.48,3.89,1.32,5.57l-6.02,5.57V215.9
	z M311.1,231.67c1.98,1.34,4.37,2.13,6.94,2.13h17.25c2.57,0,4.96-0.79,6.94-2.13l7.27,6.74c-14.9,4.42-30.78,4.42-45.68,0
	L311.1,231.67z M346.37,226.95c0.85-1.68,1.32-3.57,1.32-5.57v-5.48h4.69v16.63L346.37,226.95z M352.39,209.63h-4.69v-5.49
	c0-1.68-0.34-3.28-0.94-4.74l5.64-4.85V209.63z M344.88,252.25h-36.43c-4.14,0-7.5-3.36-7.5-7.5v-0.67
	c8.37,2.6,17.04,3.91,25.72,3.91c8.68,0,17.35-1.3,25.72-3.91v0.67C352.39,248.89,349.02,252.25,344.88,252.25L344.88,252.25z"
                  />
                )}
                <path
                  className="parking1"
                  d="M444.77,152.77H199.39c-2.28,0-4.12-1.84-4.12-4.12l0,0c0-2.28,1.84-4.12,4.12-4.12h245.38
	c2.28,0,4.12,1.84,4.12,4.12l0,0C448.89,150.93,447.05,152.77,444.77,152.77z"
                />
                <path
                  className="parking7"
                  d="M372.44,151.4h-86.07c-1.52,0-2.75-1.23-2.75-2.75l0,0c0-1.52,1.23-2.75,2.75-2.75h86.07
	c1.52,0,2.75,1.23,2.75,2.75l0,0C375.19,150.17,373.96,151.4,372.44,151.4z"
                />
              </svg>
          </div>
            )}
          <div className="column col-complementary" role="complementary">
            <Text
              setlabelTextContainer={setLabelTextContainer}
              settextValueContainer={setTextvalueContainer}
              setcontainer={setContainer}
              setLabelContiner ={setLabelContainer}
              setscale={setScale}
              options={options}
              visualizationName={visualizationName}
              data={data}
              container={container}
              scale={scale}
              currentLabel={currentLabel}
              type={type}
              labelName={options.labelName}
            />
          </div>
        </div>
      </div>
    </>
  );
}

Renderer.propTypes = RendererPropTypes;
