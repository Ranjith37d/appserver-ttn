
import React from "react";
import routeWithUserSession from "@/components/ApplicationArea/routeWithUserSession";
import Link from "@/components/Link";
import PageHeader from "@/components/PageHeader";
import Paginator from "@/components/Paginator";
import { wrap as itemsList, ControllerType } from "@/components/items-list/ItemsList";
import { ResourceItemsSource } from "@/components/items-list/classes/ItemsSource";
import { StateStorage } from "@/components/items-list/classes/StateStorage";
import Button from "antd/lib/button";
import ItemsTable, { Columns } from "@/components/items-list/components/ItemsTable";
import routes from "@/services/routes";
import CreateSourceDialog from "@/components/CreateSourceDialog";
import { policy } from "@/services/policy";
import navigateTo from "@/components/ApplicationArea/navigateTo";
import helper from "@/components/dynamic-form/dynamicFormHelper";
import Destination, { IMG_ROOT } from "@/services/destination";

import {reject } from "lodash";


export const STATE_CLASS = {
  unknown: "label-warning",
  ok: "label-success",
  triggered: "label-danger",
};

class DestinationLists extends React.Component {
  static propTypes = {
    controller: ControllerType.isRequired,
  };
  constructor(props){
    super(props)
    this.state={
      destinationTypes:[], 
    }
  }

  componentDidMount(){
    Promise.all([Destination.query(), Destination.types()])
    .then(values =>
      this.setState({
        destination:values[0],
        destinationTypes:values[1]
      }),
        () => {
          // all resources are loaded in state
          if (true) {
            if (policy.canCreateDestination()) {
                this.showCreateSourceDialog();
            } else {
              navigateTo("destinations", true);
            }
          }
        }
      )
    .catch(error => this.props.onError(error))
  }

  createDestination = (selectedType, values) => {
    const target = { options: {}, type: selectedType.type };
    helper.updateTargetWithValues(target, values);

    return Destination.create(target).then(destination => {
      this.setState({ loading: true });
      Destination.query().then(destinations => this.setState({ destinations, loading: false }));
      return destination;
    });
  };


  showCreateSourceDialog = () => {
    CreateSourceDialog.showModal({
      types: reject(this.state.destinationTypes, "deprecated"),
      sourceType: "Alert Destination",
      imageFolder: IMG_ROOT,
      onCreate:this.createDestination,
    })
      .onClose((result = {}) => {
        if (result.success) {
          navigateTo(`destinations/${result.data.id}`);
        }
      })
      .onDismiss(() => {
        navigateTo("destinations", true);
      });
  };
  

  listColumns = [
  
    Columns.custom.sortable(
      (text, destination) => (
        <div>
          <Link className="table-main-title" href={"destination/" + destination.id}>
            {destination.name}
          </Link>
        </div>
      ),
      {
        title: "Name",
        field: "name",
      }
    ),
    Columns.custom.sortable(
      (text, destination) => (
        <div>
          <Link className="table-main-title" href={"destination/" + destination.channel_id}>
            {destination.type}
          </Link>
        </div>
      ),
      {
        title: "type",
        field: "type",
      }
    ),

  ];

  render() {
    const { controller } = this.props;

    return (
      <div className="page-alerts-list">
        <div className="container">
          <PageHeader
            title={controller.params.pageTitle}
            actions={
              <Button value="large" type="primary" onClick={()=>this.showCreateSourceDialog()}>
                <i className="fa fa-plus m-r-5" />
                Add Destination
              </Button>
            }
          />
          <div>
            {controller.isLoaded ? (
              <div className="table-responsive bg-white tiled">
                <ItemsTable
                  loading={!controller.isLoaded}
                  items={controller.pageItems}
                  columns={this.listColumns}
                  orderByField={controller.orderByField}
                  orderByReverse={controller.orderByReverse}
                  toggleSorting={controller.toggleSorting}
                />
                <Paginator
                  showPageSizeSelect
                  totalCount={controller.totalItemsCount}
                  pageSize={controller.itemsPerPage}
                  onPageSizeChange={itemsPerPage => controller.updatePagination({ itemsPerPage })}
                  page={controller.page}
                  onChange={page => controller.updatePagination({ page })}
                />
              </div>
            ) : null}
          </div>
        </div>
      </div>
    );
  }
}

const DestinationListPage = itemsList(
  DestinationLists,
  () =>
    new ResourceItemsSource({
      isPlainList: true,
      getRequest() {
        return {};
      },
      getResource() {
        return Destination.query.bind(Destination);
      },
    }),
  () => new StateStorage({ orderByField: "created_at", orderByReverse: true, itemsPerPage: 20 })
);

routes.register(
  "Destination.List",
  routeWithUserSession({
    path: "/destination",
    title: "Detsination ",
    render: pageProps => <DestinationListPage {...pageProps} currentPage="alerts" />,
  })
);
