export default {
  type: "DUSTBIN",
  name: "Dustbin",
  isDefault: true,
  getOptions: options => ({ ...DEFAULT_OPTIONS, ...options }),

  // getOptions,
  Renderer,
  Editor,
  defaultColumns: 3,
  defaultRows: 8,
  minColumns: 1,
  minRows: 5,
};