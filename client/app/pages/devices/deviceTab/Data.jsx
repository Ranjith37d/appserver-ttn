import React, { useState, useEffect, useRef } from "react";
import { VariableSizeGrid as Grid } from "react-window";
import ResizeObserver from "rc-resize-observer";
import classNames from "classnames";
import { Table } from "antd";
import moment from "moment";
import notification from "@/services/notification";
import { Renderer } from "@/components/visualizations/visualizationComponents";

function VirtualTable(props) {
  const { columns, scroll } = props;
  const [tableWidth, setTableWidth] = useState(0);
  const widthColumnCount = columns.filter(({ width }) => !width).length;
  const mergedColumns = columns.map(column => {
    if (column.width) {
      return column;
    }

    return { ...column, width: Math.floor(tableWidth / widthColumnCount) };
  });
  const gridRef = useRef();
  const [connectObject] = useState(() => {
    const obj = {};
    Object.defineProperty(obj, "scrollLeft", {
      get: () => null,
      set: scrollLeft => {
        if (gridRef.current) {
          gridRef.current.scrollTo({
            scrollLeft,
          });
        }
      },
    });
    return obj;
  });

  const resetVirtualGrid = () => {
    gridRef.current.resetAfterIndices({
      columnIndex: 0,
      shouldForceUpdate: false,
    });
  };

  useEffect(() => resetVirtualGrid, [tableWidth]);

  const renderVirtualList = (rawData, { scrollbarSize, ref, onScroll }) => {
    ref.current = connectObject;
    const totalHeight = rawData.length * 54;
    return (
      <Grid
        ref={gridRef}
        className="virtual-grid"
        columnCount={mergedColumns.length}
        columnWidth={index => {
          const { width } = mergedColumns[index];
          return totalHeight > scroll.y && index === mergedColumns.length - 1 ? width - scrollbarSize - 1 : width;
        }}
        height={scroll.y}
        rowCount={rawData.length}
        rowHeight={() => 54}
        width={tableWidth}
        onScroll={({ scrollLeft }) => {
          onScroll({
            scrollLeft,
          });
        }}>
        {({ columnIndex, rowIndex, style }) => (
          <div
            className={classNames("virtual-table-cell", {
              "virtual-table-cell-last": columnIndex === mergedColumns.length - 1,
            })}
            style={style}>
            {rawData[rowIndex][mergedColumns[columnIndex].dataIndex]}
          </div>
        )}
      </Grid>
    );
  };

  return (
    <>
      <ResizeObserver
        onResize={({ width }) => {
          setTableWidth(width);
        }}>
        <Table
          {...props}
          className="virtual-table"
          columns={mergedColumns}
          pagination={false}
          scroll={{ y: 240 }}
          components={{
            body: renderVirtualList,
          }}
        />
      </ResizeObserver>
    </>
  );
} // Usage

export const DeviceDataEvents = props => {
  const [data, setData] = useState([]);
  const [deviceDetails, setDeviceDetails] = useState({});
  const socket = props.socketApi;
  const columns = [
    {
      title: "Time",
      dataIndex: "time",
    },
    {
      title: "Data",
      dataIndex: "data",
    },
  ];
  const webSockekEvents = [
    "connect",
    "disconnect",
    "connect_error",
    "connect_timeout",
    "reconnect_attempt",
    "reconnect_error",
    "reconnect_failed",
    "reconnecting",
    "reconnect",
  ];

  function deviceWebsocket() {
    webSockekEvents.map(events => {
      socket.on(events, function() {
        if (events === "connect") {
          notification.success("connection established");

          setData(preValue => [
            ...preValue,
            { key: data.length, time: moment().format("LTS"), data: "Connected SuccesFully to the server " },
          ]);
          socket.emit("my event", { data: "I'm connected!" });
        }
        if (events === "reconnecting") {
          setData(preValue => [
            ...preValue,
            { key: data.length, time: moment().format("LTS"), data: " Reconnecting  .....  " },
          ]);
          notification.info("Reconnecting");
        }

        socket.emit("my event", { data: "I'm connected!" });

        if (events === "connect_error") {
          notification.error("Something went wrong");
        }
        setData(preValue => [...preValue, { key: data.length, time: moment().format("LTS"), data: arguments[0] }]);
      });
    });


    socket.emit("start", props.deviceDetail.channel_id, props.deviceDetail.device_id);
    socket.on("uplink", function() {
      setData(preValue => [...preValue, { key: data.length, time: moment().format("LTS"), data: arguments[0] }]);
    });
  }

  useEffect(() => {
    deviceWebsocket();

    setDeviceDetails(props.deviceDetail);
  }, []);

  return (
    <>
      <VirtualTable
        columns={columns}
        dataSource={data}
        deviceDetails={deviceDetails}
        scroll={{
          y: 500,
          x: true,
        }}
      />
     
    </>
  );
};


