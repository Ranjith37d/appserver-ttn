import React, { useState, useEffect, useRef } from "react";
import Select from "antd/lib/select";
import { Typography, Divider, Form, Input } from "antd";
import HorizontalFormItem from "../../alert/components/HorizontalFormItem";

import { getChannels, getDevices, getDeviceAttributes } from "../../../services/query-result";
import { trim } from "lodash";
import Modal from "antd/lib/modal";
import { wrap as wrapDialog, DialogPropType } from "@/components/DialogWrapper";
import navigateTo from "@/components/ApplicationArea/navigateTo";
import { QueryService } from "@/services/query";
import notification from "@/services/notification";
import "./Events.less";

const { Text } = Typography;

const Events = props => {
  const [name, setName] = useState("");
  const [form] = Form.useForm();
  const formRef = useRef();
  const [isValid, setIsValid] = useState(false);
  const [channels, setChannels] = useState([]);
  const [devices, setDevices] = useState([]);
  const [deviceAttributes, setDeviceAttributes] = useState([]);
  const [selectedChannel, setSelectedChannel] = useState("");
  const [selectedDevices, setSelectedDevices] = useState([]);
  const [selectedDeviceAttributes, setSelectedDeviceAttributes] = useState([]);
  const [AttributeCondition, setAttributeCondition] = useState("");
  const [threshold, setThershold] = useState("");
  const CONDITIONS = {
    ">": "\u003e",
    ">=": "\u2265",
    "<": "\u003c",
    "<=": "\u2264",
    "==": "\u003d",
    "!=": "\u2260",
  };
  const VALID_STRING_CONDITIONS = ["==", "!="];

  useEffect(() => {
    if (channels && channels.length == 0) {
      getChannels().then(data => setChannels(data));
    }
  }, []);

  useEffect(() => {
    if (selectedChannel) {
      getDevices(selectedChannel).then(data => {
        setDevices(data);
      });
      formRef.current.setFieldsValue({ Devices: [] });
      setAttributeCondition("");
      setSelectedDeviceAttributes("");

      formRef.current.setFieldsValue({ Attributes: [] });
      formRef.current.setFieldsValue({ condition: [] });
      formRef.current.setFieldsValue({ threshold: [] });

      setSelectedDevices([]);
    }
  }, [selectedChannel]);

  useEffect(() => {
    if (selectedChannel && selectedDevices.length) {
      getDeviceAttributes(selectedChannel, selectedDevices).then(data => {
        setDeviceAttributes(data);
      });

      setSelectedDeviceAttributes([]);
      setThershold("");
    }
    // formRef.current.setFieldsValue({ Attributes: [] });
  }, [selectedDevices]);

  function save() {
    var eventDetails = {
      channel_id: selectedChannel,
      device_id: selectedDevices,
      operator: AttributeCondition,
      value: threshold,
      attribute: selectedDeviceAttributes,
    };
    props.EventSchedule(eventDetails);
    props.dialog.close();
  }
  const layout = {
    labelCol: {
      span: 6,
    },
    wrapperCol: {
      span: 16,
    },
  };

  return (
    <>
      <Modal
        {...props.dialog.props}
        title="New Event"
        okText="Save"
        width={700}
        cancelText="Close"
        okButtonProps={{
          disabled: isValid,

          "data-test": "QuerySaveButton",
        }}
        cancelButtonProps={{}}
        onOk={save}
        wrapProps={{
          "data-test": "CreateQueryDialog",
        }}>
        <Form {...layout} form={form} ref={formRef}>
          <Form.Item
            name="Channel"
            label="Channel"
            rules={[
              {
                required: true,
              },
            ]}>
            <Select
              className="gutter-row"
              style={{ width: "100%", border: "none", borderRadius: "2px" }}
              placeholder="Select Channel"
              optionFilterProp="children"
              value={selectedChannel}
              onChange={setSelectedChannel}
              filterOption={(input, option) => option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}>
              {channels &&
                channels.map(channel => {
                  return (
                    <Select.Option key={channel.channel_id} value={channel.channel_id}>
                      {channel.channel_name}
                    </Select.Option>
                  );
                })}
            </Select>
          </Form.Item>
          <Form.Item
            name="Devices"
            label="Devices"
            rules={[
              {
                required: true,
              },
            ]}>
            <Select
              style={{ width: "100%", border: "none", borderRadius: "2px" }}
              placeholder="Select Device"
              onChange={setSelectedDevices}
              value={selectedDevices}
              filterOption={(input, option) => option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}>
              {devices &&
                devices.map(device => {
                  return (
                    <Select.Option key={device.device_id} value={device.device_id}>
                      {device.device_name}
                    </Select.Option>
                  );
                })}
            </Select>
          </Form.Item>

          <div style={{ paddingLeft: "65px" }}>
            <HorizontalFormItem label="Trigger when">
              <div className="input-title">
                <Select
                  name="Attributes"
                  placeholder="Select Attributes"
                  style={{ width: "100%", border: "none", borderRadius: "2px" }}
                  onChange={setSelectedDeviceAttributes}
                  value={selectedDeviceAttributes}>
                  {deviceAttributes &&
                    deviceAttributes.map(item => (
                      <Select.Option key={item} value={item}>
                        {item}
                      </Select.Option>
                    ))}
                </Select>
              </div>
              <div className="input-title">
                <span>Condition</span>
                <Select
                  name="condition"
                  optionLabelProp="label"
                  onChange={e => setAttributeCondition(e)}
                  dropdownMatchSelectWidth={false}
                  style={{ width: 55 }}>
                  <Select.Option value=">" label={CONDITIONS[">"]}>
                    {CONDITIONS[">"]} greater than
                  </Select.Option>
                  <Select.Option value=">=" label={CONDITIONS[">="]}>
                    {CONDITIONS[">="]} greater than or equals
                  </Select.Option>
                  <Select.Option disabled key="dv1">
                    <Divider className="select-option-divider m-t-10 m-b-5" />
                  </Select.Option>
                  <Select.Option value="<" label={CONDITIONS["<"]}>
                    {CONDITIONS["<"]} less than
                  </Select.Option>
                  <Select.Option value="<=" label={CONDITIONS["<="]}>
                    {CONDITIONS["<="]} less than or equals
                  </Select.Option>
                  <Select.Option disabled key="dv2">
                    <Divider className="select-option-divider m-t-10 m-b-5" />
                  </Select.Option>
                  <Select.Option value="==" label={CONDITIONS["=="]}>
                    {CONDITIONS["=="]} equals
                  </Select.Option>
                  <Select.Option value="!=" label={CONDITIONS["!="]}>
                    {CONDITIONS["!="]} not equal to
                  </Select.Option>
                </Select>
              </div>
              <div className="input-title">
                <Input
                  name="threshold"
                  value={threshold}
                  onChange={e => setThershold(e.target.value)}
                  placeholder="Threshold"
                  style={{ width: 90 }}
                />
              </div>
            </HorizontalFormItem>
          </div>
        </Form>
      </Modal>
    </>
  );
};

Events.propTypes = {
  dialog: DialogPropType.isRequired,
};

export default wrapDialog(Events);
