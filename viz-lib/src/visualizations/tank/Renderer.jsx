import "./tankwidget.css";
import React, { useEffect, useState } from "react";
import resizeObserver from "@/services/resizeObserver";
import { isFinite } from "lodash";
import { getCounterData } from "@/visualizations/components/utils";
import Text from "../text/Renderer";

const Renderer = ({ data, options, visualizationName }) => {
  const [scale, setScale] = useState("1.00");
  const [container, setContainer] = useState(null);
  const [tankElement, setTankElement] = useState();
  const [tankTopCap,setTankTopCap] = useState()
  const [currentValue, setCurrentValue] = useState(0);
  const [textValueContainer, setTextvalueContainer] = useState();
  const [labelTextContainer, setLabelTextContainer] = useState();

  useEffect(() => {
    if (container) {
      if (options.ParameterData.length >= 1) {
        filtervalue();
        Watermeter();
      }
    }
  }, [options, container, currentValue, tankElement,labelTextContainer,textValueContainer]);

  // Filter Value
  const filtervalue = () => {
    if (options.ParameterData !== undefined) {
      if (data.rows.length !== 0) {
        setCurrentValue(data.rows[0][options.ParameterData]);
        options.value = currentValue;
      } else {
        options.value = 0;
      }
    }
  };

  const Watermeter = () => {
    const BATTERY_MIN = 1;
    const BATTERY_MAX = 0;
    const RANGE_MIN = options.range[0];
    const RANGE_MAX = options.range[1];
    const BATTERY_FILL_PER_UNIT = (BATTERY_MIN - BATTERY_MAX) / (RANGE_MAX - RANGE_MIN);
    var currentVal = currentValue > RANGE_MAX ? RANGE_MAX : currentValue < RANGE_MIN ? RANGE_MIN : currentValue-RANGE_MIN;

    const batteryFill = BATTERY_FILL_PER_UNIT * currentVal;
    var state = options.states.find(value => value.from <= currentValue && value.to >= currentValue);



    if (!(tankElement == null || tankElement == undefined)) {
      if (options.range[0] < options.range[1]&& options.range[0] <=currentValue) {
        if (batteryFill <= 1) {
      
          tankElement.style.transform = `scaleY(${batteryFill})`;
        } else {
          tankElement.style.transform =`scaleY(1)`;
        }
        if (!(state == null || state == undefined)) {
          tankElement.style.fill = state.color;
          textValueContainer.style.color = state.color;
          labelTextContainer.style.color = state.color;
          tankTopCap.style.fill=state.color
        } else {
          tankTopCap.style.fill='#0095c4'
          tankElement.style.fill = "#0095c4";
          textValueContainer.style.color = "#0095c4";
          labelTextContainer.style.color = "#0095c4";
        }
      } else {
        tankElement.style.transform = `scaleY(0)`;
      }
    }
    if(state){

      labelTextContainer.style.color = state.color;
      textValueContainer.style.color = state.color;
    }
  }


  //   if (options.range[0] < options.range[1]&& options.range[0] <=currentValue) {
      
  //     if (!(state == null || state == undefined)) {
  //       if (batteryFill <= 1 && tankElement) {
  //         tankElement.style.transform = `scaleY(${batteryFill})`;
  //         tankElement.style.fill = state.color;
  //         tankTopCap.style.fill=state.color

  //         labelTextContainer.style.color = state.color;
  //         textValueContainer.style.color = state.color;
  //       } else {
  //         if (!(tankElement == null || tankElement == undefined)) {
  //           tankElement.style.transform = `scaleY(${batteryFill})`;
  //           tankElement.style.fill = state.color;
  //           tankTopCap.style.fill=state.color

  //         }
  //         labelTextContainer.style.color = state.color;
  //         textValueContainer.style.color = state.color;
  //       }
  //     } else {
  //       //handle, when min value is greater than mx value
  //       if (!tankElement == null) {
  //         tankElement.style.transform = `scaleY(0)`;
  //       }
  //     }
  //   }
  // };

  return (
    <>
      <div className="bulb-viz-container">
        <div className="counter-layout">
          {options.showIcon && (
            <div className="column col-main">
              <svg
                id="prefix__Layer_1"
                xmlns="http://www.w3.org/2000/svg"
                x={0}
                y={0}
                viewBox="0 0 640 480"
                xmlSpace="preserve">
                <style>{".prefix__st1{fill:#515153} .prefix__st0{fill:#FFFFFF;stroke:#515153;stroke-width:15;stroke-miterlimit:10;}.prefix__st2{fill:#0095c4}"}</style>
            
                <g>
                  <path
                    class="prefix__st0"
                    d="M398.28,340.5H251.72c-12.27,0-22.22-9.95-22.22-22.22V185.72c0-12.27,9.95-22.22,22.22-22.22h146.55
		c12.27,0,22.22,9.95,22.22,22.22v132.55C420.5,330.55,410.55,340.5,398.28,340.5z"
                  />
                  <path
                    class="prefix__st1"
                    d="M415,162H237c-5.22,0-9.5-4.27-9.5-9.5v0c0-5.22,4.27-9.5,9.5-9.5h178c5.23,0,9.5,4.27,9.5,9.5v0
		C424.5,157.73,420.23,162,415,162z"
                  />
                  <rect x="255.12" y="125" class="st1" width="16.76" height="11" />
                  <path
                    ref={setTankTopCap}
                    class="top-cap"
                    d="M274.45,125h-21.89c-2.5,0-4.55-2.05-4.55-4.55v-1.89c0-2.5,2.05-4.55,4.55-4.55h21.89
		c2.5,0,4.55,2.05,4.55,4.55v1.89C279,122.95,276.95,125,274.45,125z"
                  />
                  <path
                    ref={setTankElement}
                    class="prefix__st2 tank"
                    d="M398.86,335h-148c-7.65,0-13.86-6.2-13.86-13.86V185h176v135.86C413,328.67,406.67,335,398.86,335z"
                  />
                </g>
              </svg>
            </div>
          )}
          <div className="column col-complementary" role="complementary">
            {options && (
              <Text
                setlabelTextContainer={setLabelTextContainer}
                settextValueContainer={setTextvalueContainer}
                setcontainer={setContainer}
                setscale={setScale}
                options={options}
                visualizationName={visualizationName}
                data={data}
                container={container}
                scale={scale}
                currentLabel={options.labelName}
              />
            )}
          </div>
        </div>
      </div>
    </>
  );
};
export default Renderer;
