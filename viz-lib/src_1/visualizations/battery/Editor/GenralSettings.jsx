import React, { useState } from "react";
import { map, transform } from "lodash";
import * as Grid from "antd/lib/grid";
import Form from "antd/lib/form";
import { Section, Select, Input, InputNumber } from "@/components/visualizations/editor";


const GenralSettings = ({ form, setIsValid, options, data, visualizationName, onOptionsChange }) => {
  //form props comes from the EditVisualtionDialog in editor Component

  // range[MIN , MAX ] value as MIN, MAX


  return (
    <>
      <Form
        form={form[0]}
        
        initialValues={{
          Parameter: options.ParameterData,
          Minvalue: options.range[0],
          Maxvalue: options.range[1],
          labelName: options.labelName,
        }}>
        <Section>
          <Form.Item
            name="Parameter"
            rules={[
              {
                required: true,
              },
            ]}>
            <Select
              layout="horizontal"
              label="Select Parameter"
              data-test="Counter.General.ValueColumn"
              disabled={options.countRow}
              onChange={selectedValue => onOptionsChange({ ParameterData: selectedValue })}>
              {map(data.columns, col => (
                <Select.Option key={col.name} data-test={"Counter.General.ValueColumn." + col.name}>
                  {col.name}
                </Select.Option>
              ))}
            </Select>
          </Form.Item>
          <Section>
            <Grid.Row gutter={15} type="flex" align="middle">
              <Grid.Col span={12}>
                <Form.Item
                  name="Minvalue"
               
                  rules={
                    [{
                      required:true,
                    
                    },
                    ({ getFieldValue }) => ({
                      validator(_, value) {
                        if (!value || getFieldValue('Maxvalue') > value) {
                          return Promise.resolve();
                        }
                        return Promise.reject(new Error('maxvalue is not greater Than min value'));
                      },
                    }),
                  ]
                  }
                  
                  >
                  <InputNumber
                    min={0}
                    placeholder="Min"
                    label="Min Value"
                    onChange={value => onOptionsChange({ range: [value, options.range[1]] })}
                  />
                </Form.Item>
              </Grid.Col>
              <Grid.Col span={12}>
                <Form.Item
                  name="Maxvalue"
                  rules={
                    [{
                      required:true,
                    
                    },
                    ({ getFieldValue }) => ({
                      validator(_, value) {
                        if (!value || getFieldValue('Minvalue') <value) {
                          return Promise.resolve();
                        }
                        return Promise.reject(new Error('maxvalue is not greater Than min value'));
                      },
                    }),
                  ]
                  }
                
                  >
                  <InputNumber
                   
                    label="Max Value"
                    placeholder="Max"
                    onChange={value => onOptionsChange({ range: [options.range[0],value] })}

                  />
                </Form.Item>
              </Grid.Col>
            </Grid.Row>
          </Section>
          <Section>
            <Form.Item
              name="labelName"
              rules={[
                {
                  required: false,
                },
              ]}>
              <Input
                name="labelName"
                label="Label Name"
                placeholder="Label Name"
                type="text"
                onChange={e => onOptionsChange({ labelName: e.target.value })}
              />
            </Form.Item>
          </Section>
        </Section>
      </Form>
    </>
  );
};

export default GenralSettings;
