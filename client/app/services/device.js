import { axios } from "@/services/axios";
import { create } from "lodash";

export const Device = {
  //Channels Actions
  channel: () => axios.get("api/channels"),
  createChannel: channel =>
    axios.post("api/channels", { channel_name: channel.channel_name, channel_id: channel.channel_id }),
  channelDetails: channel_id => axios.get(`api/channels/${channel_id}`),
  updateChannel: (channel_id, channel_name) => axios.post(`api/channels/${channel_id}`, { channel_name: channel_name }),
  channelDelete: channel_id => axios.delete(`api/channels/${channel_id}`),
  //Device Actions
  deviceDetails: (channel_id, device_id) => axios.get(`api/channels/${channel_id}/devices/${device_id}`),
  updateDetails: path => axios.post(`api/channels/${path}`),
  device: channel_id => axios.get(`api/channels/${channel_id}/devices`),
  createDevice: (device_name, channel_id, device_id) =>
    axios.post(`api/channels/${channel_id}/devices`, { device_id: device_id, device_name: device_name }),
  deviceDelete: (channel_id, device_id) => axios.delete(`api/channels/${channel_id}/devices/${device_id}`),
};
