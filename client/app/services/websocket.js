import io from "@/pages/devices/channeltab/node_modules/socket.io-client";

export default function deviceWebsocket() {
  const socket = io("http://localhost:9191/");
  socket.on("connect", function() {
    socket.emit("my event", { data: "I'm connected!" });
  });
  socket.emit("start", 1, 1);
  socket.on("uplink", function() {
    // var dataElem = document.getElementById('dataElement');
    // dataElem.innerText += arguments[0];
  });
}
