import { trim } from "lodash";
import React, { useState } from "react";
import Modal from "antd/lib/modal";
import Input from "antd/lib/input";
import DynamicComponent from "@/components/DynamicComponent";
import { wrap as wrapDialog, DialogPropType } from "@/components/DialogWrapper";
import navigateTo from "@/components/ApplicationArea/navigateTo";
import recordEvent from "@/services/recordEvent";
import { policy } from "@/services/policy";


function QueryDialog (){
    return(

    <Modal
    {...dialog.props}
  
    title="New Query"
    okText="Save"
    cancelText="Close"
    okButtonProps={{
      disabled: !isValid || saveInProgress,
      loading: saveInProgress,
      "data-test": QuerySaveButton",
    }}
    cancelButtonProps={{
      disabled: saveInProgress,
    }}
    onOk={save}
    closable={!saveInProgress}
    maskClosable={!saveInProgress}
    wrapProps={{
      "data-test": "CreateQueryDialog",
    }}>
    <DynamicComponent name="CreateQueryDialogExtra">
      <Input
        defaultValue={name}
        onChange={handleNameChange}
        onPressEnter={save}
        placeholder="Q Name"
        disabled={saveInProgress}
        autoFocus
      />
    </DynamicComponent>
  </Modal>
    )
}