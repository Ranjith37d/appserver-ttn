import React, { useEffect, useState } from "react";
import { chain, cloneDeep, find } from "lodash";
import GridLayout, { Responsive, WidthProvider } from "react-grid-layout";
import "./dashboard.css";
import cx from "classnames";
import useDashboard from "@/pages/dashboards/hooks/useDashboard";
import AutoHeightController from "@/components/dashboards/AutoHeightController";
import cfg from "@/config/dashboard-grid-options";
import {DashboardWidget} from "@/components/dashboards/DashboardGrid"
const ResponsiveGridLayout = WidthProvider(Responsive);

export const DashboardGrid = props => {
  ///First Get DashBoard data
  ///intial value has been set For (layouts,disableAnimations:true) and also initiates auto heightController
  ///when Compoennt mount First it willcheck the mbile breakpoint
  const SINGLE = "single-column";
  const MULTI = "multi-column";
  let mode = null;

  const [dashlayout, setDashLayout] = useState();
  const dashboardOptions = useDashboard(props.dashboard);

  const onBreakpointChange = mode => {
    mode = mode;
  };

  useEffect(() => {}, [autoHeightCtl]);

  const onWidgetHeightUpdated = (widgetId, newHeight) => {
    setDashLayout(dashlayout => {
      const layout = cloneDeep(dashlayout[MULTI]); // must clone to allow react-grid-layout to compare prev/next state
      const item = find(layout, { i: widgetId.toSting() });

      if (item) {
        // update widget height
        item.h = Math.ceil((newHeight + cfg.margins) / cfg.rowHeight);
      }

      return { layouts: { [MULTI]: layout } };
    });
  };
  const autoHeightCtl = new AutoHeightController(onWidgetHeightUpdated);

  // height updated by auto-height

  const normalizeTo = layout => ({
    col: layout.x,
    row: layout.y,
    sizeX: layout.w,
    sizeY: layout.h,
    autoHeight: autoHeightCtl.exists(layout.i),
  });

  //height updated by manaual resize

  const onWidgetResize = (layout, oldItem, newItem) => {
    if (oldItem.h !== newItem.h) {
      autoHeightCtl.remove(Number(newItem.i));
    }
    autoHeightCtl.remove();
  };
  const {
    dashboard,
    filters,
    setFilters,
    loadDashboard,
    loadWidget,
    removeWidget,
    saveDashboardLayout,
    globalParameters,
    refreshDashboard,
    refreshWidget,
    editingLayout,
    setGridDisabled,
  } = dashboardOptions;

  //mobile View Check

  //get dashboard Data

  const normalizeFrom = widget => {
    const {
      id,
      options: { position: pos },
    } = widget;

    return {
      i: id.toString(),
      x: pos.col,
      y: pos.row,
      w: pos.sizeX,
      h: pos.sizeY,
      minW: pos.minSizeX,
      maxW: pos.maxSizeX,
      minH: pos.minSizeY,
      maxH: pos.maxSizeY,
    };
  };

  const onLayoutChange = (_, layouts) => {
    if (layouts[MULTI]) {

      setDashLayout(layouts);
    }
    mode = document.body.offsetWidth <= cfg.mobileBreakPoint ? SINGLE : MULTI;
    if (mode == SINGLE) {
      return;
    }

    const normalized = chain(layouts[MULTI])
      .keyBy("i")
      .mapValues(normalizeTo)
      .value();
  };

  return (
    <ResponsiveGridLayout
      className={cx("layout", { "disable-animations": "" })}
      cols={{ [MULTI]: cfg.columns, [SINGLE]: 1 }}
      rowHeight={cfg.rowHeight - cfg.margins}
      margin={[cfg.margins, cfg.margins]}
      isDraggable={true}
      isResizable={true}
      layouts={dashlayout}
      onLayoutChange={onLayoutChange}
      onBreakpointChange={onBreakpointChange}
      breakpoints={{ [MULTI]: cfg.mobileBreakPoint, [SINGLE]: 0 }}>
      {dashboard.widgets.map(widget => (
        <div
          style={{ background: "#cfcfcf" }}
          key={widget.id}
          data-grid={normalizeFrom(widget)}
          data-widgetid={widget.id}
          data-test={`WidgetId${widget.id}`}
          className={cx("dashboard-widget-wrapper", {
            "widget-auto-height-enabled": autoHeightCtl.exists(widget.id),
          })}>
          <DashboardWidget
            websocket={false}
            dashboard={dashboard}
            widget={widget}
            filters={filters}
            isPublic={true}
            isLoading={widget.loading}
            canEdit={dashboard.canEdit()}
            onLoadWidget={loadWidget}
            onRefreshWidget={refreshWidget}
            onRemoveWidget={removeWidget}
            onParameterMappingsChange={loadDashboard}
          />
        </div>
      ))}
    </ResponsiveGridLayout>
  );
};
