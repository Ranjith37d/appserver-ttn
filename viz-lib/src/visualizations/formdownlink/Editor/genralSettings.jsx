import React, { useState, useEffect } from "react";
import { map } from "lodash";
import { Spin, Button, notification } from "antd";

import Form from "antd/lib/form";
import { Section, Select, Input } from "@/components/visualizations/editor";

const GenralSettings = ({ form, type, options, data, visualizationName, onOptionsChange }) => {
  const [fetching, setFetching] = useState();
  const [commandList, setCommandList] = useState([]);
  useEffect(() => {
    setFetching(true);
    fetch(`api/downlink-commands`)
      .then(response => response.json())
      .then(command => {
        setCommandList(command);
        setFetching(false);
      })
      .catch(e => setFetching(false));
  }, []);

  useEffect(() => {
    if (options.commandId) {
      var filtercommand = commandList.filter(commands => commands.id == options.commandId);

      if (filtercommand[0] != undefined) {
        onOptionsChange({ commandName: filtercommand[0].name });
      }
    }
  }, [options.commandId]);
  return (
    <>
      <Section>
        <Form
          form={form[0]}
          initialValues={{
            Parameter: type,
          }}>
            <Form.Item      name={type} rules={[
              {
                required: true,
              },
            ]}>

          <Select
       
            layout="horizontal"
            label="Select Command "
            notFoundContent={fetching ? <Spin size="small" /> : null}
            data-test="Counter.General.ValueColumn"
            defaultValue={options.commandName}
            onChange={command => onOptionsChange({ commandId: command })}>
            {map(commandList, command => (
              <Select.Option
              key={command.id}
              value={command.id}
              data-test={"Counter.General.ValueColumn." + command.name}>
                {command.name}
              </Select.Option>
            ))}
          </Select>
            </Form.Item>
        </Form>
      </Section>
    </>
  );
};

export default GenralSettings;
