import React, { useState, useMemo, useEffect } from "react";
import { Section } from "@/components/visualizations/editor";
import Checkbox from "antd/lib/checkbox/Checkbox";
import ColorPicker from "@/components/ColorPicker";
import ColorPalette from "@/visualizations/ColorPalette";
import Table from "antd/lib/table";
import Input from "antd/lib/input";
import Button from "antd/lib/button";
import { UpdateOptionsStrategy } from "@/components/visualizations/editor/createTabbedEditor";

const StateIndicationSettings  = ({ options, data, visualizationName, onOptionsChange }) => {
  const [states, setStates] = useState(options.states);

  const colors = useMemo(
    () => ({
      Automatic: null,
      ...ColorPalette,
    }),
    []
  );

  useEffect(() => {
    onOptionsChange({ states: states }, UpdateOptionsStrategy.shallowMerge);
  }, [states]);

  const updateState = (record, newValue, cell) => {
    const newData = [...states];
    const index = newData.findIndex(item => record.key == item.key);

    const item = newData[index];
    item[cell] = newValue;
    newData.splice(index, 1, { ...item });

    setStates(newData);
  };

  const addRow = () => {
    var newState = [...states];
    newState.push({ key: newState.length + 1, label: "New State", value: "", color: "#FFFFFF" });
    setStates(newState);
  };

  const deleteRow = recordKey => {
    var newState = [...states];
    var index = newState.findIndex(record=>record.key == recordKey);

    newState.splice(index, 1);
    setStates(newState);
  };

  const columns = [
    {
      title: "Label",
      key: "label",
      dataIndex: "label",
      render: (label, record) => <Input value={label} onChange={e => updateState(record, e.target.value, "label")} />,
    },
    {
      title: "Value",
      key: "value",
      dataIndex: "value",
      render: (val, record) => <Input value={val} onChange={e => updateState(record, e.target.value, "value")} />,
    },
    {
      title: "Color ",
      key: "color",
      dataIndex: "color",
      render: (color, record) => (
        <ColorPicker
          color={color}
          interactive
          presetColors={colors}
          placement="topRight"
          onChange={newColor => updateState(record, newColor, "color")}
          addonAfter={<ColorPicker.Label color={color} presetColors={colors} />}
        />
      ),
    },

    {
      title: "",
      dataIndex: "operations",
      render: (text, record) => {
        return <>{record.key > 2 && <i className="fa fa-trash-o m-r-5" onClick={() => deleteRow(record.key)} />}</>;
      },
    },
  ];

  return (
    <>
      <div className="add-btn" style={{ display: "flex", justifyContent: "flex-end", marginBottom: "5px" }}>
        <div style={{ paddingRight: "15px", marginRight: "auto" }}>
          <Section>
            <Checkbox checked={options.showLabel} onChange={e => onOptionsChange({ showLabel: e.target.checked })}>
              Show Label
            </Checkbox>

            <Checkbox checked={options.showValue} onChange={e => onOptionsChange({ showValue: e.target.checked })}>
              Show Value
            </Checkbox>
            <Checkbox checked={options.showIcon} onChange={e => onOptionsChange({ showIcon: e.target.checked })}>
              Show Icon
            </Checkbox>
          </Section>
        </div>
     
      </div>
      <Section>
        <Table dataSource={states} columns={columns} pagination />
      </Section>
    </>
  );
};

export default StateIndicationSettings;
