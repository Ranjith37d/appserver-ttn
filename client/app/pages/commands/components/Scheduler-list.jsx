import React, { useState } from "react";
import { List, Tooltip, Popconfirm } from "antd";
import { DeleteOutlined } from "@ant-design/icons";

import moment from "moment";
import cronstrue from "cronstrue/i18n";
// import cronpretty from "cronpretty"

const SchedulerList = props => {
  const [scheduleTextString, setScheduleTextString] = useState("");

  const getVal = key => {
    let val = cronstrue.toString(key.toString(), { locale: "en" });
    if (val.search("undefined") === -1) {
      setScheduleTextString(val);
      return val;
    }
    return "-";
  };

  return (
    <List
      bordered={false}
      loadmore={true}
      style={{
        boxShadow: "-3px -3px 5px white, 3px 3px 5px rgba(0, 0, 0, 0.2)",
        background: "#f9f7f7",
        padding: "20px",
        margin: "10px",
      }}
      dataSource={props.schedules}
      renderItem={schedules => (
        <List.Item
          disabled
          actions={[
            <>
              <div style={{ display: "flex", justifyContent: "space-between" }}>
                <Popconfirm
                  title="Are you sure to delete this Schedules?"
                  onConfirm={() => {
                    props.remove(schedules.id);
                    props.setload(!props.loadState);
                  }}
                  okText="Yes"
                  cancelText="No">
                  <Tooltip placement="topLeft" title={"Delete"}>
                    <DeleteOutlined
                      style={{
                        color: "#f14646cf",
                        fontSize: "1.3em",
                      }}
                    />
                  </Tooltip>
                </Popconfirm>
              </div>
            </>,
          ]}>
          {schedules.type === "o" && (
            <label>
              {moment
                .utc(schedules.name)
                .tz("Asia/Kolkata")
                .format("YYYY-MM-DD HH:mm:ss")}
                 <span style={{opacity:0.4, paddingLeft: '5px'}}> ({`${schedules.timezone }`})</span> 
            </label>
          )}
          {schedules.type === "r" && (
            <>
              <label>{schedules.name} <span style={{opacity:0.4, paddingLeft: '5px'}}>  ({`${schedules.timezone }`})</span> </label>
            </>
          )}
          {schedules.type === "e" && <label>{schedules.name} </label>}
        </List.Item>
      )}
    />
  );
};

export default SchedulerList;
