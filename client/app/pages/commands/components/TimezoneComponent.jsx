import React, { Component } from "react";
import moment from "moment";
import momentTimezone from "moment-timezone";
import ReactDOM from "react-dom";
import "antd/dist/antd.css";
import { Row, Col, Select, Divider } from "antd";

const { Option, OptGroup } = Select;

class TimeZoneComponent extends Component {
  constructor(props) {
    super(props);

    this.state = {
      value: null,
    };
  }

  renderOptions() {
    const timezones = moment.tz.names();
    let mappedValues = {};
    let regions = [];

    timezones.map(timezone => {
      const splitTimezone = timezone.split("/");
      const region = splitTimezone[0];
      if (!mappedValues[region]) {
        mappedValues[region] = [];
        regions.push(region);
      }
      mappedValues[region].push(timezone);
    });
    return regions.map(region => {
      const options = mappedValues[region].map(timezone => {
        return <Option key={timezone}>{timezone}</Option>;
      });
      return (
        <OptGroup key={region} title={<div style={{ fontSize: 30 }}>{region}</div>}>
          {options}
        </OptGroup>
      );
    });
  }

  render() {
    return (
      <>
        {/* <h2>{`Your Timezone: ${momentTimezone.tz.guess()}`}</h2> */}
        <div style={{ marginBottom: "20px" }}>
          <label style={{ marginRight: '10px' }}>Timezone</label>
          <Select style={{ flex: 0.4 }} defaultValue={momentTimezone.tz.guess()} onChange={this.props.setTimezone}>
            {this.renderOptions()}
          </Select>
        </div>
      </>
    );
  }
}

export default TimeZoneComponent;
