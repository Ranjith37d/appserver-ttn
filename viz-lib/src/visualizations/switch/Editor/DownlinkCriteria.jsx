import React from "react";
import { useEffect } from "react";
import { useState } from "react";

import Divider from "antd/lib/divider";
import { Section, Select, Input } from "@/components/visualizations/editor";

export const DownlinkCriteria = ({ options, onOptionsChange }) => {
  const [deviceAttributes, setDeviceAttributes] = useState([]);
  useEffect(() => {
    fetch(
      `${window.location.origin}/console/app/api/queries/applications/${options.channelName}/devices/${options.device_id}/attributes`
    )
      .then(response => response.json())
      .then(response => setDeviceAttributes(response));
  }, []);

  const CONDITIONS = {
    ">": "\u003e",
    ">=": "\u2265",
    "<": "\u003c",
    "<=": "\u2264",
    "==": "\u003d",
    "!=": "\u2260",
  };

  return (
    <div data-test="Criteria">
      <div className="input-title">
        <span>Value column</span>
        {/* {editMode ? ( */}
        <Select
          value={options.columnValue}
          onChange={columnValue => onOptionsChange({ columnValue })}
          dropdownMatchSelectWidth={false}
          style={{ minWidth: 100 }}>
          {deviceAttributes.map(device => (
            <Select.Option key={device.device_id}>{device.device_name}</Select.Option>
          ))}
        </Select>
        {/* ) : ( */}
        {/* <DisabledInput minWidth={70}>{alertOptions.column}</DisabledInput> */}
        {/* )} */}
      </div>
      <div className="input-title">
        <span>Condition</span>
        {/* {editMode ? ( */}
        <Select
          value={options.condition}
          onChange={condition => onOptionsChange({ condition })}
          optionLabelProp="label"
          dropdownMatchSelectWidth={false}
          style={{ width: 55 }}>
          <Select.Option value=">" label={CONDITIONS[">"]}>
            {CONDITIONS[">"]} greater than
          </Select.Option>
          <Select.Option value=">=" label={CONDITIONS[">="]}>
            {CONDITIONS[">="]} greater than or equals
          </Select.Option>
          <Select.Option disabled key="dv1">
            <Divider className="select-option-divider m-t-10 m-b-5" />
          </Select.Option>
          <Select.Option value="<" label={CONDITIONS["<"]}>
            {CONDITIONS["<"]} less than
          </Select.Option>
          <Select.Option value="<=" label={CONDITIONS["<="]}>
            {CONDITIONS["<="]} less than or equals
          </Select.Option>
          <Select.Option disabled key="dv2">
            <Divider className="select-option-divider m-t-10 m-b-5" />
          </Select.Option>
          <Select.Option value="==" label={CONDITIONS["=="]}>
            {CONDITIONS["=="]} equals
          </Select.Option>
          <Select.Option value="!=" label={CONDITIONS["!="]}>
            {CONDITIONS["!="]} not equal to
          </Select.Option>
        </Select>
        {/* ) : ( */}
        {/* <DisabledInput minWidth={50}>{CONDITIONS[alertOptions.op]}</DisabledInput> */}
        {/* )} */}
      </div>
      <div className="input-title">
        <span>Threshold</span>
        {/* {editMode ? ( */}
        <Input
          style={{ width: 90 }}
          value={options.thershold}
          onChange={e => onOptionsChange({ thershold: e.target.value })}
        />
        {/* ) : ( */}
        {/* <DisabledInput minWidth={50}>{alertOptions.value}</DisabledInput> */}
        {/* )} */}
      </div>
    </div>
  );
};
