import React, { useState, useEffect, useRef } from "react";
import Select from "antd/lib/select";
import { getChannels, getDevices } from "@/services/query-result";
import { Form, Radio, Typography, Button, Checkbox, InputNumber } from "antd";
import { trim } from "lodash";
import Modal from "antd/lib/modal";
import Input from "antd/lib/input";

import { wrap as wrapDialog, DialogPropType } from "@/components/DialogWrapper";
import navigateTo from "@/components/ApplicationArea/navigateTo";

import notification from "@/services/notification";
import Command from "@/services/command";

const { Text } = Typography;

const DownlinkAlertDialogue = props => {
  const [name, setName] = useState("");
  const [form] = Form.useForm();
  const [isValid, setIsValid] = useState(false);
  const [channels, setChannels] = useState([]);
  const [devices, setDevices] = useState([]);
  const [payload, setpayload] = useState("");
  const [selectedChannel, setSelectedChannel] = useState("");
  const [selectedDevices, setSelectedDevices] = useState("");
  const [selecetedInsertedMode, setSelectedInsertedMode] = useState("push");
  const [confirmedDownlink, setConfirmedDownlink] = useState(false)
  const [fPort, setFPort] = useState(1)
  const formRef = useRef()


  useEffect(() => {
    if (channels && channels.length == 0) {
      getChannels().then(data => setChannels(data));
    }
  }, []);

  useEffect(() => {
   
    if (selectedChannel) {
    
      setSelectedDevices([])
      getDevices(selectedChannel).then(data => {
        setDevices(data);

        formRef.current.setFieldsValue({ Devices: [] })
      });
      setSelectedDevices([]);
    }
  }, [selectedChannel]);

  function handleNameChange(event) {
    const value = trim(event.target.value);
    setName(value);
  }

  function save() {
    form.submit()

    if (name.length >= 1 && selectedChannel.length >= 1
      && selectedDevices.length >= 1 && payload.length >= 1 && selecetedInsertedMode.length >= 1
      && payload.toString().length >= 1
    ) {
      Command.save({
        command_name: name,
        channel_id: selectedChannel,
        device_ids: selectedDevices,
        fport: fPort,
        mode: selecetedInsertedMode,
        confirm: confirmedDownlink,
        payload: payload,
      }).then(data => {
        notification.success("Commands created Successfully");
        props.dialog.close();
        navigateTo(`/console/app/commands/` + data.id);
        // navigateTo(`/console/app/queries/` + data.id);
      });
    } else {
      // notification.error("Fill All the values to continue");
    }

    setIsValid(false);
  }
  const layout = {
    labelCol: {
      span: 6,
    },
    wrapperCol: {
      span: 16,
    },
  };
 
  return (
    <>
      <Modal
        {...props.dialog.props}
        title="Downlink"
        okText="Save"
        cancelText="Close"
        okButtonProps={{
          disabled: isValid,

          "data-test": "QuerySaveButton",
        }}
        cancelButtonProps={{}}
        onOk={save}
        wrapProps={{
          "data-test": "CreateQueryDialog",
        }}>
        <Form {...layout} form={form} ref={formRef} >
          <Form.Item
            name="Commandname"
            label="Command Name"

            rules={[
              {
                required: true, message: 'CommandName is required'
              },
            ]}>
            <Input
              defaultValue={name}
              onChange={handleNameChange}
              onPressEnter={save}
              placeholder="Command Name "
              autoFocus
            />
          </Form.Item>

          <Form.Item
            name="Channel"
            label="Channel"
            rules={[
              {
                required: true,
              },
            ]}>
            <Select
              className="gutter-row"
              style={{ width: "100%", border: "none", borderRadius: "2px" }}
              placeholder="Select Channel"
              optionFilterProp="children"
              value={selectedChannel}
              onChange={setSelectedChannel}
              filterOption={(input, option) => option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}>
              {channels &&
                channels.map(channel => {
                  return (
                    <Select.Option key={channel.channel_id} value={channel.channel_id}>
                      {channel.channel_name}
                    </Select.Option>
                  );
                })}
            </Select>
          </Form.Item>
          <Form.Item
            name="Devices"
            label="Devices"
            rules={[
              {
                required: true,
              },
            ]}>
            <Select
              mode = "multiple"
              style={{ width: "100%", border: "none", borderRadius: "2px" }}
              placeholder="Select Device"
              onChange={setSelectedDevices}
              value={selectedDevices}
              filterOption={(input, option) => option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}>
              {devices &&
                devices.map(device => {
                  return (
                    <Select.Option key={device.device_id} value={device.device_id}>
                      {device.device_name}
                    </Select.Option>
                  );
                })}
            </Select>
          </Form.Item>
          <Form.Item

            label="Insert Mode"
          >
            <Radio.Group value={selecetedInsertedMode} onChange={e => { setSelectedInsertedMode(e.target.value) }}  >

              <Radio value={"push"}>Push</Radio>
              <Radio value={"replace"}>Replace</Radio>

            </Radio.Group>
          </Form.Item>
          <Form.Item
            name="Confirmation"
            label="Confirmation"

          >
            <Checkbox
              value="confirmation" onChange={e => setConfirmedDownlink(e.target.checked)} >
              Confirmed</Checkbox>
          </Form.Item>
          <Form.Item
            name="fport"
            label="Fport"
            rules={[
              {
                required: true,
              },
            ]}
          >
            <InputNumber value={fPort} min={0} onChange={e => setFPort(e)} />
          </Form.Item>

          <Form.Item
            name="payload"
            label="Payload"
            rules={[
              {
                required: true,
              },
            ]}>
            <Input
              type="text"
              onChange={e => {
                setpayload(e.target.value);
              }}>
            </Input>
          </Form.Item>

        </Form>
      </Modal>
    </>
  );
};

DownlinkAlertDialogue.propTypes = {
  dialog: DialogPropType.isRequired,
};

export default wrapDialog(DownlinkAlertDialogue);
