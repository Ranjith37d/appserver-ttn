import logging
import requests

from flask import current_app
import datetime
from redash.worker import job, get_job_logger
from redash import models, utils, settings

logger = get_job_logger(__name__)


@job("default")
def send_downlink_command(command_id):
    logger.info("Sending downlink for command id %s", command_id)

    try:
        command = models.DownlinkCommand.query.get(command_id)
        host = "beta.thethingsmate.com"
        token = settings.DOWNLINK_API_KEY
        url = "https://{}/api/v3/as/applications/{}/devices/{}/down/{}".format(host, command.channel_id, command.device_id, command.mode)
        headers = {
            "Content-Type": "application/json",
            "Authorization": 'Bearer ' + token
            }

        resp = requests.post(
            url,
            json={"downlinks":[{"f_port": command.fport, "confirmed": command.confirm, "frm_payload": command.payload }]},
            headers=headers,
            timeout=5.0,
        )
        if resp.status_code != 200:
            logging.error(
                "downlink send ERROR. status_code => {status}".format(
                    status=resp.status_code
                )
            )
    except Exception:
        logging.exception("downlink send ERROR.")

