import createTabbedEditor from "@/components/visualizations/editor/createTabbedEditor";
import GenralSettings from "./GenralSettings";
import IndicationStateSettings from "./IndicationStateSettings";
import FormatSettings from "@/visualizations/components/FormatSettings" 
export default createTabbedEditor([

  { key: "General", title: "General", component: GenralSettings },
  { key: "State", title: "State", component: IndicationStateSettings },
  {key:"Format" , title: "Format" ,component : FormatSettings}
]);
