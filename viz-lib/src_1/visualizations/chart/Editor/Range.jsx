import { includes } from "lodash";
import React from "react";
import { useDebouncedCallback } from "use-debounce";
import { Section, Input, Checkbox, ContextHelp } from "@/components/visualizations/editor";
import { EditorPropTypes } from "@/visualizations/prop-types";

const Range = ({ options, onOptionsChange }) => {
  const isShowDataLabelsAvailable = includes(
    ["indicator", "area", "column", "scatter", "pie", "heatmap"],
    options.globalSeriesType
  );
  const [debouncedOnOptionsChange] = useDebouncedCallback(onOptionsChange, 200);
  return (
    <>
      <Section>
        <Input
          label={<React.Fragment>Min</React.Fragment>}
          data-test="Chart.DataLabels.NumberFormat"
          defaultValue={options.numberFormat}
          onChange={e => debouncedOnOptionsChange({ numberFormat: e.target.value })}
        />
        <Input
          label={<React.Fragment>Max</React.Fragment>}
          data-test="Chart.DataLabels.NumberFormat"
          defaultValue={options.numberFormat}
          onChange={e => debouncedOnOptionsChange({ numberFormat: e.target.value })}
        />
      </Section>
    </>
  );
};

Range.propTypes = EditorPropTypes;

export default Range;
