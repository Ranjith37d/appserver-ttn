import React, { useState, useEffect } from "react";
import Modal from "antd/lib/modal";
import { wrap as wrapDialog } from "@/components/DialogWrapper";
import cronstrue from "cronstrue/i18n";
import { SchedulerComponent } from "./SchedulerComponent";
import moment from "moment"

const getVal = key => {
    let val = cronstrue.toString(key.toString(), { locale: "en" });
    if (val.search("undefined") === -1) {
      return val;
    }
    return "-";
  
  };
const SchedulesDialogModalComponent = (props) => {

    // o and r means oneTime  and Recurring  .
    // sever side Identify  SchedulesType with this o and r .



    const [SchedulesType, SetSchedulesType] = useState('r');
    const [TimeZone, setTimeZone] = useState('Asia/Kolkata')
    const [Time, setTime] = useState(moment());
    const [cron, setCron] = useState("0 0 00 1/1 * ?*");


    useEffect(() => {

        SetSchedulesType(SchedulesType)

    }, [SchedulesType])




    const AddSchedules = () => {

        if (cron.scheduler === "o") {

            props.ScheudlerAdd(
                {
                    type: 'o',
                    timezone: TimeZone,
                    schedule: cron.val.utc().format("YYYY-MM-DD hh:mm:ss"),
                    name:cron.val.utc().format("YYYY-MM-DD hh:mm:ss")
                }
                    );


                    props.dialog.close()
                    props.setLoad(!props.loadState);
        } else {

            // Cron Post Request 
     
            props.ScheudlerAdd(
                {
                    type: "r",
                    timezone: TimeZone,
                    schedule: cron,
                    name:getVal(cron)
                });


                props.dialog.close()
                props.setLoad(!props.loadState);
        }

    
    }

    return (
        <>
            <Modal

                {...props.dialog.props}
                title="Schedules"
                okText="Save"
                cancelText="Close"
                okButtonProps={{
                    "data-test": "QuerySaveButton",
                }}
                cancelButtonProps={{}}
                onOk={AddSchedules}
                wrapProps={{
                    "data-test": "CreateQueryDialog",
                }}

            >




                <SchedulerComponent
                    schedulerAdd={props.ScheudlerAdd}
                    loadState={props.load}
                    load={props.setLoad}
                    schedulestype={SetSchedulesType}
                    settimeZone={setTimeZone}
                    settime={setTime}
                    timezone={TimeZone}
                    time={Time}
                    cron={cron}
                    setcron={setCron}

                />
            </Modal>





        </>
    )


}



export default wrapDialog(SchedulesDialogModalComponent);

