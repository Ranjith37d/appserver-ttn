import React, { useState } from "react";

import "./downlink.css";
import { options } from "numeral";
export const Renderer = ({ options }) => {
  const [responseMessage, setResposeMessage] = useState({});
  const [switchColor, setSwitchColor] = useState(options.switchState);
  const [payloadState, setPayloadState] = useState({ payload: { on: options.ONpayload, off: options.OFFpayload } });

  const Downlink = () => {
    fetch(
      `https://beta.thethingsmate.com/console/app/api/v3/as/applications/${options.channelName}/devices/${options.device_id}/down/replace`,
      {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          Authorization: options.api_key,
          body: JSON.stringify({
            downlinks: [
              {
                f_port: 1,
                confirmed: options.confirmed,
                frm_payload: options.switchState ? options.ONpayload : options.OFFpayload,
              },
            ],
          }),
        },
      }
    )
      .then(response => response.json())
      .then(response => setResposeMessage(response));
    setSwitchColor(!switchColor);
  };
  return (
    <div className="chart-visualization-container">
      <svg
        xmlns="http://www.w3.org/2000/svg"
        onClick={Downlink}
        version="1.1"
        id="Layer"
        x="0px"
        y="0px"
        viewBox="-40 0 150 150">
        <circle
          style={{ fill: switchColor ? "#C75C5C" : "green" }}
          className="switch"
          cx="32"
          cy="32"
          r="32"
          onclick="alert(hello)"
        />
        <g className="switch1">
          <path
            className="switch2"
            d="M32,52c-9.9,0-18-8.1-18-18c0-6.4,3.4-12.3,8.9-15.5c1-0.6,2.2-0.2,2.7,0.7c0.6,1,0.2,2.2-0.7,2.7   C20.7,24.4,18,29.1,18,34c0,7.7,6.3,14,14,14c7.7,0,14-6.3,14-14c0-5.1-2.7-9.7-7.2-12.2c-1-0.5-1.3-1.8-0.8-2.7   c0.5-1,1.8-1.3,2.7-0.8C46.5,21.5,50,27.5,50,34C50,43.9,41.9,52,32,52z"
          />
        </g>
        <g className="switch1">
          <path
            className="switch2"
            d="M32,36c-1.1,0-2-0.9-2-2V14c0-1.1,0.9-2,2-2c1.1,0,2,0.9,2,2v20C34,35.1,33.1,36,32,36z"
          />
        </g>
        <path
          className="switch3"
          d="M39.8,18c4.9,2.7,8.2,8,8.2,14c0,8.8-7.2,16-16,16c-8.8,0-16-7.2-16-16c0-5.9,3.2-11,7.9-13.8"
        />
        <line className="switch3" x1="32" y1="32" x2="32" y2="12" />
      </svg>
      {/* {details.length > 0 ? <p>{responseMessage.details[0].message_format}</p> : null} */}
    </div>
  );
};
