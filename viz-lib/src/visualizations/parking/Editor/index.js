import createTabbedEditor from "@/components/visualizations/editor/createTabbedEditor";
import GenralSettings from "./GenralSettings";
import StateIndicationSettings from "./StateSettings";
export default createTabbedEditor([
  { key: "General", title: "General", component: GenralSettings },
  { key: "State", title: "State", component: StateIndicationSettings },
]);
