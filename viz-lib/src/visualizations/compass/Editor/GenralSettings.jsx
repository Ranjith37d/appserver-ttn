import React from "react";
import { map } from "lodash";
import Form from "antd/lib/form";
import { Section, Select, Input ,Checkbox} from "@/components/visualizations/editor";

const GenralSettings = ({ form, options, data, visualizationName, onOptionsChange }) => {
  
  return (
    <>
      <Section>
        <Form form={form[0]} initialValues={{ compass_parameter: options.ParameterData, labelName: options.labelName }}>
          <Form.Item name="compass_parameter" rules={[{ required: true }]}>
            <Select
              layout="horizontal"
              label="Select Parameter"
              data-test="Counter.General.ValueColumn"
              defaultValue={options.ParameterData}
              disabled={options.countRow}
              onChange={selectedValue => onOptionsChange({ ParameterData: selectedValue })}>
              {map(data.columns, col => (
                <Select.Option key={col.name} data-test={"Counter.General.ValueColumn." + col.name}>
                  {col.name}
                </Select.Option>
              ))}
            </Select>
          </Form.Item>
          <Section>
            <Form.Item name="label Name" >
              <Input
                label="Label Name"
                placeholder="Label Name"
                type="text"
                onChange={e => onOptionsChange({ labelName: e.target.value })}
                defaultValue={options.labelName}
              />
            </Form.Item>
          </Section>
          <Section>
          <div style={{ paddingRight: "15px", marginRight: "auto" }}>
            <Checkbox style={{padding:'10px'}} checked={options.showLabel} onChange={e => onOptionsChange({ showLabel: e.target.checked })}>
              Show Label
            </Checkbox>

            <Checkbox checked={options.showValue}  style={{padding:'10px'}} onChange={e => onOptionsChange({ showValue: e.target.checked })}>
              Show Value
            </Checkbox>
            <Checkbox checked={options.showIcon}  onChange={e => onOptionsChange({ showIcon: e.target.checked })}>
              Show Icon
            </Checkbox>
        </div>
          </Section>
        </Form>
      </Section>
    </>
  );
};

export default GenralSettings;
