"""empty message

Revision ID: 3a9ffeb17a91
Revises: a9a861e80557
Create Date: 2021-01-21 16:11:54.849671

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '3a9ffeb17a91'
down_revision = 'a9a861e80557'
branch_labels = None
depends_on = None


def upgrade():
    pass


def downgrade():
    pass
