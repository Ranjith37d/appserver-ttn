import logging
from sqlalchemy import create_engine

logger = logging.Logger('app-fields')

db_string = "postgres://postgres:root@localhost:5432/network_server"

db = create_engine(db_string)

def get_user_channels(username):
    channels = []
    logger.debug('getting channels for user', username)
    query = "SELECT channel_id, channel_name FROM app.applications_view WHERE username = '{}';".format(username)
    result_set = db.execute(query)
    for c in result_set:
        channels.append({"channel_id": c["channel_id"], "channel_name": c["channel_name"]})
    
    return channels

def get_user_channel_devices(channel_id):
    devices= []

    if channel_id is None:
        return devices
    
    query = "select device_id, name from app.devices_view where application_id = '{}';".format(channel_id)
    result_set = db.execute(query)
    for c in result_set:
        devices.append({"device_id": c["device_id"], "device_name": c["name"]})

    return devices

def get_application_fields(app_id):
    fields=[]

    logger.debug('getting fields for app', app_id)

    if app_id is None:
        return fields

    query = "SELECT distinct jsonb_object_keys(payload) as field from app.metric where app_id='{}'".format(app_id)
    
    result_set = db.execute(query)

    for r in result_set:
        fields.append(r['field'])

    logger.debug('fields available for app_id', app_id, 'are ', fields)

    return fields

def get_app_device_fields(app_id, device_id):
    fields=[]

    logger.debug('getting fields for app id: ', app_id, 'and device id: ', device_id)

    if app_id is None or device_id is None:
        return fields
    
    query = "SELECT distinct jsonb_object_keys(payload) as field from app.metric where app_id='{}' and dev_id= '{}'".format(app_id, device_id)
    result_set = db.execute(query)

    for r in result_set:
        fields.append(r['field'])

    logger.debug('fields available for app_id', app_id, ' and device id', device_id,'are ', fields)

    return fields

def get_device_fields(app_id, device_ids = []):
    fields=[]

    logger.debug('getting fields for app id: ', app_id, 'and device ids: ', device_ids)

    if app_id is None or device_ids is None or device_ids.count == 0:
        return fields

    devices = []
    for id in device_ids:
        devices.append("'{}'".format(id))

    query = "SELECT distinct jsonb_object_keys(payload) as field from app.metric where app_id='{}' and dev_id IN ({})".format(app_id, ",".join(devices))
    result_set = db.execute(query)

    for r in result_set:
        fields.append(r['field'])

    logger.debug('fields available for app_id', app_id, ' and device id', device_ids,'are ', fields)

    return fields

def parse_query(query):

    query = query.replace("SELECT time::timestamp,","").replace(" ORDER BY \"time\" desc;","")
    querySegments = query.partition(' FROM ') 
    measurementSeg = querySegments[0]
    whereSeg = querySegments[2].partition(' WHERE ')[2]
    whereSegs = whereSeg.partition(' and ')
    deviceSegs  = whereSegs[2].replace("device_id in (","").replace(")","").split(",")
    
    app_id = whereSegs[0].partition('=')[2].strip('\'')

    device_ids = []
    for device_id in deviceSegs:
        device_ids.append(device_id.strip().strip('\''))

    measurements = []
    for measurement in measurementSeg.split(","):
        measurements.append(measurement.strip().partition('\' as ')[2])

    return {'channel_id': app_id, 'device_ids': device_ids, 'attributes': measurements}