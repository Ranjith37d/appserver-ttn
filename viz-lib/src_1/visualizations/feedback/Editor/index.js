import createTabbedEditor from "@/components/visualizations/editor/createTabbedEditor";
import GenralSettings from "./GenralSettings";
export default createTabbedEditor([{ key: "General", title: "General", component: GenralSettings }]);
