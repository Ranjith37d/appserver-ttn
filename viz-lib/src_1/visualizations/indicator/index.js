import Renderer from "./Renderer";
import Editor from "./Editor";
const DEFAULT_OPTIONS = {
  range: [0, 100],
  ParameterData: "",
  rowNumber: 1,
  targetRowNumber: 1,
  stringDecimal: 0,
  stringDecChar: ".",
  stringThouSep: ",",
  tooltipFormat: "0,0.000",
  showLabel: true,
  showValue: true,
  showIcon:true,
  states: [
    {
      key: 1,
      label: "ON",
      value: 1,
      color: "#43B05C",
    },
    {
      key: 2,
      label: "OFF",
      value: 0,
      color: "#ff0000",
    },
  ],
};
export default {
  type: "INDICATOR",
  name: "Indicator",
  isDefault: true,
  getOptions: options => ({ ...DEFAULT_OPTIONS, ...options }),
  Renderer,
  Editor,
  defaultColumns: 3,
  defaultRows: 8,
  minColumns: 1,
  minRows: 5,
};
