import createTabbedEditor from "@/components/visualizations/editor/createTabbedEditor";
import IndicationStateSettings from "./IndicationStateSettings";
import FormatSettings from "@/visualizations/components/FormatSettings" ;
import GeneralSettings from "@/visualizations/components/GeneralSettings";

export default createTabbedEditor([
  { key: "General", title: "General", component: GeneralSettings  },
  {key:"State",title:"State" ,component:IndicationStateSettings},
  {key:"Format",title:"Format" ,component:FormatSettings},
]);
