import React, { useEffect } from "react";
import routeWithUserSession from "@/components/ApplicationArea/routeWithUserSession";
import routes from "@/services/routes";
import { Auth } from "@/services/auth";

export const LogoutComponent = () => {
  useEffect(() => {
    Auth.logout();
  }, []);
  return (
    <>
      <div class="loading-indicator">
        <div id="css-logo">
          <div id="circle">
            <div></div>
          </div>
          <div id="point">
            <div></div>
          </div>
          <div id="bars">
            <div class="bar"></div>
            <div class="bar"></div>
            <div class="bar"></div>
            <div class="bar"></div>
          </div>
        </div>
        <div id="shadow"></div>
      </div>
    </>
  );
};
routes.register(
  "Logout",
  routeWithUserSession({
    path: "/logout",
    title: "Logout",
    render: pageProps => <LogoutComponent {...pageProps} currentPage="logout" />,
  })
);
