import React,{useState} from 'react' 
import Cron from 'react-js-cron';



function CronTab(props) {
  const defaultValue = '0 10 * * 1,3,5'


  return (
    <div>

      <Cron value={props.value} setValue={props.onChange} />
    </div>
  )
}

export default CronTab