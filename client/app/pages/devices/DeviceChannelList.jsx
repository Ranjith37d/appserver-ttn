import { toUpper } from "lodash";
import React from "react";
import routeWithUserSession from "@/components/ApplicationArea/routeWithUserSession";
import Link from "@/components/Link";
import PageHeader from "@/components/PageHeader";
import Paginator from "@/components/Paginator";
import EmptyState from "@/components/empty-state/EmptyState";
import { wrap as itemsList, ControllerType } from "@/components/items-list/ItemsList";
import { ResourceItemsSource } from "@/components/items-list/classes/ItemsSource";
import { StateStorage } from "@/components/items-list/classes/StateStorage";
import Button from "antd/lib/button";
import CreateDeviceDialog from "./CreateDeviceShowDialog";
import ItemsTable, { Columns } from "@/components/items-list/components/ItemsTable";
import { Device } from "@/services/device";
import { currentUser } from "@/services/auth";
import routes from "@/services/routes";
import CreateChannelModal from "@devices/CreateChannelModal";
export const STATE_CLASS = {
  unknown: "label-warning",
  ok: "label-success",
  triggered: "label-danger",
};

class ChannelLists extends React.Component {
  static propTypes = {
    controller: ControllerType.isRequired,
  };

  listColumns = [
    Columns.custom.sortable(
      (text, channel) => (
        <div>
          <Link className="table-main-title" href={"mqtt/" + channel.channel_id}>
            {channel.channel_id}
          </Link>
        </div>
      ),
      {
        title: "ID",
        field: "id",
      }
    ),
    Columns.custom.sortable(
      (text, channel) => (
        <div>
          <Link className="table-main-title" href={"mqtt/" + channel.channel_id}>
            {channel.channel_name}
          </Link>
        </div>
      ),
      {
        title: "Name",
        field: "name",
      }
    ),

    Columns.dateTime.sortable({ title: "Created At", field: "created_at", width: "1%" }),
  ];

  render() {
    const { controller } = this.props;

    return (
      <div className="page-alerts-list">
        <div className="container">
          <PageHeader
            title={controller.params.pageTitle}
            actions={
              <Button value="large" type="primary" onClick={() => CreateChannelModal.showModal()}>
                <i className="fa fa-plus m-r-5" />
                Add Channel
              </Button>
            }
          />
          <div>
            {controller.isLoaded ? (
              <div className="table-responsive bg-white tiled">
                <ItemsTable
                  loading={!controller.isLoaded}
                  items={controller.pageItems}
                  columns={this.listColumns}
                  orderByField={controller.orderByField}
                  orderByReverse={controller.orderByReverse}
                  toggleSorting={controller.toggleSorting}
                />
                <Paginator
                  showPageSizeSelect
                  totalCount={controller.totalItemsCount}
                  pageSize={controller.itemsPerPage}
                  onPageSizeChange={itemsPerPage => controller.updatePagination({ itemsPerPage })}
                  page={controller.page}
                  onChange={page => controller.updatePagination({ page })}
                />
              </div>
            ) : null}
          </div>
        </div>
      </div>
    );
  }
}

const AlertsListPage = itemsList(
  ChannelLists,
  () =>
    new ResourceItemsSource({
      isPlainList: true,
      getRequest() {
        return {};
      },
      getResource() {
        return Device.channel.bind(Device);
      },
    }),
  () => new StateStorage({ orderByField: "created_at", orderByReverse: true, itemsPerPage: 20 })
);

routes.register(
  "Channels.List",
  routeWithUserSession({
    path: "/mqtt",
    title: "Mqtt Channels ",
    render: pageProps => <AlertsListPage {...pageProps} currentPage="alerts" />,
  })
);
