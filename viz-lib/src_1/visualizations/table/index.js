import getOptions from "./getOptions";
import Renderer from "./Renderer";
import Editor from "./Editor";
// all confi done in this file (Renderer , Editor)
export default {
  type: "TABLE",
  name: "Table",
  getOptions, /// data  format  done
  Renderer,
  Editor,

  autoHeight: true,
  defaultRows: 14,
  defaultColumns: 3,
  minColumns: 2,
};
