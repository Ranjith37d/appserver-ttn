import React, { useState, useEffect } from "react";

import { Tabs } from "antd";
import routeWithUserSession from "@/components/ApplicationArea/routeWithUserSession";
import routes from "@/services/routes";
import { ChannelGenralSettings } from "./generalSettings/GeneralSettings";
import { DeviceDetails } from "./deviceTab/DeviceDetails";
import { Device } from "@/services/device";
import { DeviceListPage } from "@/pages/devices/channeltab/DeviceList";
const { TabPane } = Tabs;

function ChannelSetting(props) {
  const [channelDetails, setChannelDetails] = useState({ channel_name: "" });
  useEffect(() => {
    Device.channelDetails(props.id).then(data => {
      setChannelDetails(data);
    });
  }, []);

  return (
    <>
      <DeviceDetails name={channelDetails.channel_name || ""} />
      <br />
      <div className="settings-screen">
        <div className="container">
          <div className="bg-white tiled">
            <Tabs type="card" style={{ padding: "20px" }}>
              <TabPane tab="Devices" key="2">
                <DeviceListPage channel_id={props.id} />
              </TabPane>

              <TabPane tab="General settings" key="5">
                <ChannelGenralSettings channel_id={props.id} />
              </TabPane>
            </Tabs>
            <div className="p-15">
              <div></div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

function ChannelOverviewPage(props) {
  return (
    <>
      <ChannelSetting {...props} />
    </>
  );
}

routes.register(
  "Device.Data",
  routeWithUserSession({
    path: "/mqtt/:id",
    title: "Channels ",
    render: pageProps => (
      <>
        <ChannelOverviewPage {...pageProps} currentPage="alerts" />
      </>
    ),
  })
);
