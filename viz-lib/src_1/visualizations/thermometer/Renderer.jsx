import React, { useEffect, useState } from "react";
import "./thermometer.css";
import cx from "classnames";
import resizeObserver from "@/services/resizeObserver";
import { isFinite } from "lodash";
import { getCounterData } from "@/visualizations/components/utils";
import Text from "../text/Renderer";


const Renderer = ({ data, options, visualizationName }) => {
  // perform Format option

  const {
    showTrend,
    trendPositive,
    counterValue,
    counterValueTooltip,
    targetValue,
    targetValueTooltip,
    counterLabel,
  } = getCounterData(data.rows, options, visualizationName);

  const [scale,setScale]= useState('1.00')
  const [container, setContainer] = useState(null);
  const [indicatorElement, setIndicatorElement] = useState();
  const [circleIndicatorElement, setCircleIndicatorElement] = useState();
  const [contextValue, setContextValue] = useState(0);
  const [textValueContainer, setTextvalueContainer] = useState();
  const [labelTextContainer, setLabelTextContainer] = useState();
  useEffect(() => {
    if (container) {
    if (options.ParameterData.length >= 1) {
      filtervalue();
      thermometerCalculation();
    }
  }
}, [options, container, contextValue,options.showIcon,circleIndicatorElement,indicatorElement]);
  // Filter Value
  const filtervalue = () => {
    if (options.ParameterData !== undefined) {
      if (data.rows.length !== 0) {
        setContextValue(data.rows[0][options.ParameterData]);
        options.value = contextValue;
      } else {
        options.value = 0;
      }
    }
  };

  const thermometerCalculation = () => {
    const BATTERY_MIN = 1;
    const BATTERY_MAX = 0;
    const RANGE_MIN = options.range[0];
    const RANGE_MAX = options.range[1];
    const BATTERY_FILL_PER_UNIT = RANGE_MAX - RANGE_MIN > 0 ? (BATTERY_MIN - BATTERY_MAX) / (RANGE_MAX - RANGE_MIN) : 0;
    var currentVal = contextValue > RANGE_MAX ? RANGE_MAX : contextValue < RANGE_MIN ? RANGE_MIN : contextValue;

    const batteryFill = BATTERY_FILL_PER_UNIT * currentVal;
    if(!indicatorElement == null ){
    
    indicatorElement.style.transform = `scaleY(${batteryFill})`;
    }
    var state = options.states.find(value => value.from <= contextValue && value.to >= contextValue);
      
      if (!(circleIndicatorElement == null || circleIndicatorElement == undefined)) {
        
        if (options.range[0] < options.range[1]) {
          
          if (batteryFill <= 1) {
            indicatorElement.style.transform = `scaleY(${batteryFill})`;
          } else {
            indicatorElement.style.transform = `scaleY(1)`;
          }
          
          if (!(state == null || state == undefined)) {
            indicatorElement.style.fill = state.color;
            circleIndicatorElement.style.fill = state.color;
            textValueContainer.style.color = state.color;
            labelTextContainer.style.color = state.color;
          } else {
            circleIndicatorElement.style.fill = "#43B05C";
            indicatorElement.style.fill = "#43B05C";
            textValueContainer.style.color = "#43B05C";
            labelTextContainer.style.color = "#43B05C";
          }
        }
      } else {
        if(!indicatorElement==null && !circleIndicatorElement==null){

          indicatorElement.style.transform = `scaleY(0)`;
          circleIndicatorElement.style.transform = "scaleY(0)";
        }
      
      }
    };

  
  return (
    <>
      <div className="bulb-viz-container">
        <div className="counter-layout">
            {options.showIcon &&
          <div className="column col-main">
            <svg version="1.1" id="Layer_1" x="0px" y="0px" viewBox="0 0 640 480">
              <path
                className="st0"
                d="M257,293.2c0,18.5-15,33.5-33.5,33.5c-18.5,0-33.5-15-33.5-33.5c0-11.9,6.2-22.3,15.5-28.3
c-0.1-0.8-0.2-1.6-0.2-2.5V146.7c0-10,8.1-18.1,18.1-18.1c5,0,9.5,2,12.8,5.3c3.3,3.3,5.3,7.8,5.3,12.8v115.8c0,0.8-0.1,1.7-0.2,2.5
C250.9,270.9,257,281.3,257,293.2z"
              />
              <path
                className="st1"
                ref={setIndicatorElement}
                d="M235.6,156.7v147.7c0,8.7-5.4,15.7-12,15.7s-12-7-12-15.7V156.7c0-8.7,5.4-15.7,12-15.7c3.3,0,6.3,1.8,8.5,4.6
C234.3,148.4,235.6,152.4,235.6,156.7z"
              />
              <circle ref={setCircleIndicatorElement} className="st1" cx="223.6" cy="293.9" r="26.2" />
              <line className="st2" x1="241.7" y1="182.3" x2="263" y2="182.3" />
              <line className="st2" x1="241.7" y1="160" x2="263" y2="160" />
              <line className="st2" x1="241.7" y1="204.5" x2="263" y2="204.5" />
              <line className="st2" x1="241.7" y1="224.7" x2="263" y2="224.7" />
              <line className="st2" x1="241.7" y1="244.9" x2="263" y2="244.9" />
            </svg>
          </div>
}
          <div className="column col-complementary" role="complementary">
          {options && (
              <Text
                setlabelTextContainer={setLabelTextContainer}
                settextValueContainer={setTextvalueContainer}
                setcontainer={setContainer}
                setscale={setScale}
                options={options}
                visualizationName={visualizationName}
                data={data}
                container={container}
                scale={scale}
                currentLabel={options.labelName}
              />
            )}
          </div>
        </div>
      </div>
    </>
  );
};

export default Renderer;
