import React, { useState, useEffect } from "react";
import { Input, Button, Space, Form, Popconfirm } from "antd";
import { Device } from "@/services/device";
import navigateTo from "@/components/ApplicationArea/navigateTo";
import notification from "@/services/notification";

export const ChannelGenralSettings = props => {
  const [channelDetails, setChannelDetails] = useState("");
  const [channelName, setChannelName] = useState("");
  const [saveChangesValid, setSaveChangesValid] = useState(true);
  //HandleNameChanges
  const handleNameChange = e => {
    const newchannelname = e.target.value;

    if (e.target.value !== channelName) {
      setSaveChangesValid(false);
      setChannelDetails(pre => {
        return { ...pre, channel_name: newchannelname };
      });
    } else {
      setChannelDetails(pre => {
        return { ...pre, channel_name: channelName };
      });
      setSaveChangesValid(true);
    }
  };
  useEffect(() => {
    Device.channelDetails(props.channel_id).then(channel => {
      setChannelDetails(channel);
      setChannelName(channel.channel_name);
    });
  }, []);
  const layout = {
    labelCol: { span: 8 },
    wrapperCol: { span: 8 },
  };
  const tailLayout = {
    wrapperCol: { offset: 8, span: 16 },
  };

  return (
    <>
      <Form
        {...layout}
        name="basic"
        initialValues={{ remember: true }}
        //   onFinish={onFinish}
        //   onFinishFailed={onFinishFailed}
      >
        <Form.Item label="Channel Id">
          <Input value={channelDetails.channel_id} readOnly />
        </Form.Item>

        <Form.Item label="Channel Name">
          <Input value={channelDetails.channel_name} onChange={handleNameChange} placeholder="Channel Name" />
        </Form.Item>

        <Form.Item label="Channel Created At ">
          <Input value={channelDetails.created_at} readOnly />
        </Form.Item>
        <Form.Item {...tailLayout}>
          <Button
            type="primary"
            disabled={saveChangesValid}
            onClick={() => {
              Device.updateChannel(props.channel_id, channelDetails.channel_name)
                .then(() => notification.success("Channel updated Successfully"))
                .catch(() => notification.error("Error Occured while  update the  Channel"));
            }}>
            Save Changes
          </Button>
        </Form.Item>
        <Form.Item {...tailLayout}>
          <Popconfirm
            title={`Are you sure you want to delete ${'"' + channelDetails.channel_name + '"' + "\t" + "?"}`}
            onConfirm={() => {
              Device.channelDelete(props.channel_id)
                .then(() => {
                  notification.success("Channel deleted");
                  navigateTo("mqtt/");
                })
                .catch(() => {
                  notification.success("Error Occured while Delete Channel");
                });
            }}
            okText="Yes"
            cancelText="No">
            <Button danger>Delete</Button>
          </Popconfirm>
        </Form.Item>
      </Form>
    </>
  );
};
