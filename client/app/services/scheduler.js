import { axios } from "@/services/axios";

const SchedulerApi = {
  get: () => axios.get("http://localhost:3000/scheduler"),
  post: data =>
    axios.post("http://localhost:3000/scheduler", { id: Math.floor(Math.random() * Math.floor(10)), ...data }),
  //   get: ({ id }) => axios.get(`api/alerts/${id}`).then(transformResponse),
  //   save: data => axios.post(saveOrCreateUrl(data), transformRequest(data)),
  delete: data => axios.delete(`api/alerts/${data.id}`),
  mute: data => axios.post(`api/alerts/${data.id}/mute`),
  unmute: data => axios.delete(`api/alerts/${data.id}/mute`),
};

export default SchedulerApi;
