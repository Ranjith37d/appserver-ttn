import Renderer from "./Renderer";
import Editor from "./Editor";
const DEFAULT_OPTIONS = {
  range: [0, 100],
  ParameterData: "",
  rowNumber: 1,
  targetRowNumber: 1,
  stringDecimal: 0,
  stringDecChar: ".",
  stringThouSep: ",",
  tooltipFormat: "0,0.000",
  showLabel: true,
  showValue: true,
  showIcon:true,
  states: [
    {
      label:'ON',
      from: 50,
      to: 100,
      key: 0, 
      color: "#fbb540",
    },
    {
      label:'OFF',
      from: 20,
      to: 50,
      key: 1,
      color: "#cbcbcb",
    },
    
  ],

};
export default {
  type: "BULB",
  name: "Bulb",
  isDefault: true,
  getOptions: options => ({ ...DEFAULT_OPTIONS, ...options }),
  Renderer,
  Editor,
  defaultColumns: 3,
  defaultRows: 8,
  minColumns: 1,
  minRows: 5,
};
