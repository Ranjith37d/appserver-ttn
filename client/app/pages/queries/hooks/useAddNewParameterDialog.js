import { map } from "lodash";
import { useCallback } from "react";
import EditParameterSettingsDialog from "@/components/EditParameterSettingsDialog";
import useImmutableCallback from "@/lib/hooks/useImmutableCallback";
import { isDynamicDateRange, getDynamicDateRangeFromString } from "@/services/parameters/DateRangeParameter";

export default function useAddNewParameterDialog(query, onParameterAdded) {
  const handleParameterAdded = useImmutableCallback(onParameterAdded);
  // return useCallback(() => {
  //   EditParameterSettingsDialog.showModal({
  //     parameter: {
  //       title: null,
  //       name: "Timerange",
  //       type: "datetime-range",
  //       value: null,
  //     },
  //     existingParams: map(query.getParameters().get(), p => p.name),
  //   }).onClose(param => {
  //     const newQuery = query.clone();
  //     param = newQuery.getParameters().add(param);
  //     handleParameterAdded(newQuery, param);
  //   });
  // }, [query, handleParameterAdded]);

  return useCallback(() => {
    const newQuery = query.clone();
    const params = newQuery.getParameters().add({
      title: "TimeRange",
      name: "TimeRange",
      type: "datetime-range",
      value: { placeholder: ["Today"] },
      value: {
        name: "This week",
        value: getDynamicDateRangeFromString("d_this_week"),
        label: () =>
          getDynamicDateRangeFromString("d_this_week")
            .value()[0]
            .format("MMM D") +
          " - " +
          getDynamicDateRangeFromString("d_this_week")
            .value()[1]
            .format("MMM D"),
      },
    });
    handleParameterAdded(newQuery, params);
  });
}
