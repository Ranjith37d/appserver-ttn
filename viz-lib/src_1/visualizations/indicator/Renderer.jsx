
import React, { useState, useEffect } from "react";
import { RendererPropTypes } from "@/visualizations/prop-types";
import Text from "../text/Renderer";



export default function Renderer({ data, options, visualizationName }) {


  const [scale, setScale] = useState("1.00");
  const [container, setContainer] = useState(null);
  const [currentValue, setCurrentValue] = useState(0);
  const [circleContainer, setCircleContaniner] = useState();
  const [labelTextContainer, setLabelTextContainer] = useState();
  const [textValueContainer, setTextvalueContainer] = useState();
  const [currentLabel, setCurentLabel] = useState("");

  useEffect(() => {
    if (container) {
      if (options.ParameterData.length >= 1) {
        filtervalue();
        applyConfig();
      }
    }
  }, [options, container, currentValue , circleContainer]);

  // Filter Value
  const filtervalue = () => {
    if (options.ParameterData !== undefined) {
      if (data.rows.length !== 0) {
        setCurrentValue(data.rows[0][options.ParameterData]);
        options.value = currentValue;
      } else {
        options.value = 0;
      }
    }
  };




  const applyConfig = () => {
    var state = options.states.find(s => s.value == currentValue);

    if(!(circleContainer ==null)){
     
      circleContainer.style.fill = state ? state.color : "#000";
    }
    

    textValueContainer.style.color = state ? state.color : "#000";
    labelTextContainer.style.color = state ? state.color : "#000";
    setCurentLabel(state ? state.label : "");
  };

  return (
    <>
      <div className="bulb-viz-container">
        <div className="counter-layout">
            {options.showIcon &&
          <div className="column col-main">
            <svg version="1.1" id="Layer_1" x="0px" y="0px" viewBox="0 0 640 480">
              <g>
                <g>
                  <circle ref={setCircleContaniner} className="indicator0" cx="127" cy="240" r="60.5" />
                </g>
              </g>
            </svg>
          </div>
            }
            
          <div className="column col-complementary" role="complementary">
            {options && (
              <Text
                setlabelTextContainer={setLabelTextContainer}
                settextValueContainer={setTextvalueContainer}
                setcontainer={setContainer}
                setscale={setScale}
                options={options}
                visualizationName={visualizationName}
                data={data}
                container={container}
                scale={scale}
                currentLabel={currentLabel}
              />
            )}
          </div>
        </div>
      </div>
    </>
  );
}

Renderer.propTypes = RendererPropTypes;
