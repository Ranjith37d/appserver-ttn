import Editor from "./Editor"
import Renderer from "./Renderer"
const DEFAULT_OPTIONS = {
    rowNumber: 1,
    targetRowNumber: 1,
    showValue:true,
    showLabel:true,
    showIcon:true,
    counterColName: "counter",
    stringDecimal: 0,
    stringDecChar: ".",
    stringThouSep: ",",
    tooltipFormat: "0,0.000",
    ParameterData: '',
    labelName: '',



}
export default {
    type: 'COMPASS',
    name: 'Compass',
    Editor,
    Renderer,
    getOptions: (options) => ({
        ...DEFAULT_OPTIONS,
        ...options
    }),
    defaultColumns: 3,
    defaultRows: 8,
    minColumns: 1,
    minRows: 5,
}