import React, { useState, useEffect } from "react";
import { map, merge, isNumber } from "lodash";
import { useDebouncedCallback } from "use-debounce";

import { Section, Select, Input, InputNumber, Switch } from "@/components/visualizations/editor";
import * as Grid from "antd/lib/grid";
function toNumber(value) {
  value = isNumber(value) ? value : parseFloat(value);
  return isFinite(value) ? value : null;
}
const GenralSettings = ({ options, data, visualizationName, onOptionsChange }) => {
  // function optionsChanged(newOptions) {
  //   onOptionsChange(merge({}, options, { gauge: { steps: [newOptions] } }));
  // }
  // min max value

  const [minValue, setMinValue] = useState(options.gauge.axis.range[0]);
  const [maxvalue, setMaxvalue] = useState(options.gauge.axis.range[1]);
  const [labelName, setLableName] = useState(options.title.text);
  useEffect(() => {
    onOptionsChange(
      merge({}, options, { title: { text: labelName } }, { gauge: { axis: { range: [minValue, maxvalue] } } })
    );
  }, [minValue, maxvalue, labelName]);

  return (
    <>
      <Section>
      
        <Section>
          <Select
            layout="horizontal"
            label="Select Parameter"
            data-test="Counter.General.ValueColumn"
            defaultValue={options.selectedValue}
            disabled={options.countRow}
            onChange={selectedValue => onOptionsChange({ selectedValue })}>
            {map(data.columns, col => (
              <Select.Option key={col.name} data-test={"Counter.General.ValueColumn." + col.name}>
                {col.name}
              </Select.Option>
            ))}
          </Select>
        </Section>
        <Section>
          <Input
            layout="horizontal"
            label="Label Name"
            value={options.title.text}
            data-test="Counter.Formatting.DecimalPlace"
            onChange={name => setLableName(name.target.value)}
          />
        </Section>

        <Section>
          <Grid.Row gutter={15} type="flex" align="middle">
            <Grid.Col span={12}>
              <InputNumber
                label="Min Value"
                placeholder="Auto"
                // data-test={`Chart.${id}.RangeMin`}
                defaultValue={options.gauge.axis.range[0]}
                onChange={value => setMinValue(toNumber(value))}
              />
            </Grid.Col>
            <Grid.Col span={12}>
              <InputNumber
                label="Max Value"
                placeholder="Auto"
                // data-test={`Chart.${id}.RangeMax`}
                defaultValue={options.gauge.axis.range[1]}
                onChange={value => setMaxvalue(toNumber(value))}
              />
            </Grid.Col>
          </Grid.Row>
        </Section>
        {/* )} */}
      </Section>
    </>
  );
};

export default GenralSettings;
