import React, { useState } from "react";
import { map } from "lodash";
import Form from "antd/lib/form";
// import Select from "antd/lib/select";
import { Section, Input, InputNumber, Select } from "@/components/visualizations/editor";

const GenralSettings = ({ form, options, data, visualizationName, onOptionsChange }) => {
  return (
    <>
      <Form
        form={form[0]}
        initialValues={{
          ParameterData: options.ParameterData,
          labelName: options.labelName,

        }}>
        <Section>
          <Form.Item
            name="ParameterData"
            rules={[
              {
                required: true,
              },
            ]}>
            <Select
              layout="horizontal"
              label="Select Parameter"
              disabled={options.countRow}
              onChange={selectedValue => onOptionsChange({ ParameterData: selectedValue })}>
              {map(data.columns, col => (
                <Select.Option key={col.name} data-test={"Counter.General.ValueColumn." + col.name}>
                  {col.name}
                </Select.Option>
              ))}
            </Select>
          </Form.Item>
          <Section>
            <Form.Item name="labelName">
              <Input
                label="Label Name"
                placeholder="Label Name"
                type="text"
                onChange={e => onOptionsChange({ labelName: e.target.value })}
                defaultValue={options.labelName}
              />
            </Form.Item>
          </Section>

          {/* <Section>
            <Form.Item
              name="occupied"
              rules={[
                {
                  required: true,
                },
              ]}>
              <InputNumber
                label="Occupided Value"
                placeholder=""
                type="text"
                onChange={value => onOptionsChange({ occupied: Number(value) })}
              />
            </Form.Item>
          </Section>
          <Section>
            <Form.Item name="available">
              <InputNumber
                label="Available Value"
                placeholder=""
                type="text"
                onChange={value => onOptionsChange({ available: Number(value) })}
              />
            </Form.Item>
          </Section> */}
        </Section>
      </Form>
    </>
  );
};

export default GenralSettings;
