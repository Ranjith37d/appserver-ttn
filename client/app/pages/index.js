import "./home/Home";

import "./admin/Jobs";
import "./admin/OutdatedQueries";
import "./admin/SystemStatus";

import "./alerts/AlertsList";
import "./alert/Alert";
import "./devices/DeviceChannelList";
import "./devices/DeviceChannelOverview";
import "./devices/channeltab/DeviceList";
import "./devices/deviceTab/Data";
import "./devices/channeltab/Downlink";
// import "./devices/deviceTab/DeviceTab";
import "./devices/deviceTab/DeviceTabSetting";
import "./commands/command-list";
import "./commands/components/CommandView";
import "./logout/Logout";
// import "./scheduler/schedulerList";

import "./dashboards/DashboardList";
import "./dashboards/DashboardPage";
import "./dashboards/PublicDashboardPage";

import "./data-sources/DataSourcesList";
import "./data-sources/EditDataSource";

import "./destinations/DestinationsList";
import "./destinations/EditDestination";
import "./destination/DestinationList"
import "./destination/EditDestination.jsx"

import "./groups/GroupsList";
import "./groups/GroupDataSources";
import "./groups/GroupMembers";

import "./queries-list/QueriesList";
import "./queries/QuerySource";
import "./queries/QueryView";
import "./queries/VisualizationEmbed";
import "./sqlQuery/SqlQuery";
import "./queries/query-builder/QueryTree"
import "./query-snippets/QuerySnippetsList";

import "./settings/OrganizationSettings";

import "./users/UsersList";
import "./users/UserProfile";
