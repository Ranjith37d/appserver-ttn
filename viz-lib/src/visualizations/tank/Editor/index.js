import createTabbedEditor from "@/components/visualizations/editor/createTabbedEditor";
import RangeIndiactionSettings from "@/visualizations/components/RangeIndiactionSettings"
import GeneralSettings from "@/visualizations/components/GeneralSettings"
import FormatSettings from "@/visualizations/components/FormatSettings" ;


export default createTabbedEditor([
    { key: "General", title: "General", component: GeneralSettings },
    { key: "state", title: "State", component: RangeIndiactionSettings },
    {key:"Format" , title: "Format" ,component : FormatSettings}


])