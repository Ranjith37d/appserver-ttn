import React, { useEffect, useState } from "react";
import { map } from "lodash";
import { Section, Select, Input, Checkbox } from "@/components/visualizations/editor";
import { DownlinkCriteria } from "./DownlinkCriteria";

export const GenralSetting = ({ options, data, visualizationName, onOptionsChange }) => {
  const [channel, setChannel] = useState([]);
  const [selectedChannel, setSelectedChannel] = useState("");
  const [device, setDevice] = useState([]);

  useEffect(() => {
    fetch(`api/queries/applications`)
      .then(response => response.json())
      .then(channel => setChannel(channel));
  }, []);

  useEffect(() => {
    fetch(`api/queries/applications/${options.channelName}/devices`)
      .then(response => response.json())
      .then(device => setDevice(device));
  }, [selectedChannel]);

  return (
    <>
      <Section>
        <Section>
          <Select
            layout="horizontal"
            label="Select Channel "
            data-test="Counter.General.ValueColumn"
            defaultValue={options.channelName}
            onChange={channelName => {
              onOptionsChange({ channelName });
              setSelectedChannel({ channelName });
            }}>
            {map(channel, channel => (
              <Select.Option
                key={channel.channel_id}
                value={channel.channel_id}
                data-test={"Counter.General.ValueColumn." + channel.channel_name}>
                {channel.channel_name}
              </Select.Option>
            ))}
          </Select>
        </Section>
        <Section>
          <Select
            layout="horizontal"
            label="Select Device"
            data-test="Counter.General.ValueColumn"
            defaultValue={options.device_id}
            onChange={device_id => onOptionsChange({ device_id })}>
            {map(device, device => (
              <Select.Option
                value={device.device_id}
                key={device.device_id}
                data-test={"Counter.General.ValueColumn." + device.device_name}>
                {device.device_name}
              </Select.Option>
            ))}
          </Select>
        </Section>

        <Section>
          <Input
            onChange={e => onOptionsChange({ api_key: e.target.value })}
            placeholder="Bearer ..... "
            defaultValue={options.api_key}
            layout="horizontal"
            label="Api Key"
          />
        </Section>
        <Section>
          <Input
            onChange={e => onOptionsChange({ ONpayload: e.target.value })}
            placeholder="Payload"
            defaultValue={options.payload}
            layout="horizontal"
            label="ON Payload"
          />
        </Section>
        <Section>
          <Input
            onChange={e => onOptionsChange({ OFFpayload: e.target.value })}
            placeholder="Payload"
            defaultValue={options.payload}
            layout="horizontal"
            label="OFF Payload"
          />
        </Section>
        <Section>
          <Checkbox
            data-test="Chart.Downlink"
            defaultChecked={options.confirmed}
            onChange={event => onOptionsChange({ confirmed: event.target.checked })}>
            confirmed
          </Checkbox>
        </Section>
      </Section>
    </>
  );
};
