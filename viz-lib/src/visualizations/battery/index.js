import Renderer from "./Renderer";
import Editor from "./Editor";
;

//provide key start from zero
const DEFAULT_OPTIONS = {
  rowNumber: 1,
  targetRowNumber: 1,

  stringDecimal: 0,
  stringDecChar: ".",
  stringThouSep: ",",
  tooltipFormat: "0,0.000",
  range: [0, 100],
  ParameterData: "",
  labelName: "",
  showValue: true,
  showLabel: true,
  stringSuffix:' %',
  showIcon:true,
  states: [
    {
      label:'',
      from: 50,
      to: 100,
      key: 0, 
      color: "#43B05C",
    },
    { label:'',
      from: 20,
      to: 50,
      key: 1,
      color: "#f7bf6a",
    },
    {  label:'',from: 0, to: 20, key: 2, color: "#ff4245" },
  ],
};
export default {
  type: "BATTERY",
  name: "Battery",
  isDefault: true,
  getOptions: options => ({ ...DEFAULT_OPTIONS, ...options }),

  // getOptions,
  Renderer,
  Editor,
  defaultColumns: 3,
  defaultRows: 8,
  minColumns: 1,
  minRows: 5,
};
