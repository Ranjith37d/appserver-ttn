import React, { useState, useEffect } from "react";
import cx from "classnames";

import { RendererPropTypes } from "@/visualizations/prop-types";
import resizeObserver from "@/services/resizeObserver";
import { getCounterData } from "@/visualizations/components/utils";

var GaugeChart = require("gauge-chart");

export default function Renderer({ options, data, visualizationName }) {
  const [currentValue, setCurrentValue] = useState(null);
  const [formattedValue, setFormattedValue] = useState("");
  const [gaugeContainer, setGaugeContainer] = useState();
  const [arcDelimiters, setArcDelimiters] = useState();
  const [arcColors, setArcColors] = useState();
  const [rangeLabel, setRangeLabel] = useState([]);
  const [gaugeOptions, setGaugeOptions] = useState(GaugeChart.gaugeChart());
  const [gaugeWidth, setGaugeWidth] = useState(400);
  const [gaugeMin, setGaugeMin] = useState(0);
  const [gaugeMax, setGaugeMax] = useState(100);

  //update Gauge options
  const updateGaugeOptions = () => {
    var rangeColors = [];
    var rangeDelimiters = [];
    var minVal = options.range[0];
    var maxVal = options.range[1];

    setGaugeMin(minVal);
    setGaugeMax(maxVal);
    // get current value from the data rows.
    if (options.ParameterData !== undefined) {
      if (data.rows.length !== 0) {
        setCurrentValue(data.rows[0][options.ParameterData]);
        options.value = currentValue;
      } else {
        options.value = 0;
      }
    }
    options.states.forEach(state => {
      rangeColors.push(state.color);
      if (minVal < state.upto && state.upto < maxVal) {
        rangeDelimiters.push(calculatePercentage(minVal, maxVal, state.upto));
      }
    });
    setArcColors(rangeColors);
    setFormattedValue(options.showValue ? getCounterData(data.rows, options, visualizationName).counterValue : "");
    // setFormattedValue();
    setArcDelimiters(rangeDelimiters.sort((a, b) => a - b));
    setRangeLabel([minVal.toString(), maxVal.toString()]);
  };

  // calculate minmax in percentage
  function calculatePercentage(minVal, maxVal, currentVal) {
    return maxVal === minVal ? 0 : ((currentVal - minVal) / (maxVal - minVal)) * 100;
  }

  useEffect(() => {
    updateGaugeOptions();
  }, [options]);

  useEffect(() => {
    if (gaugeContainer) {
      if (gaugeOptions) {
        gaugeOptions.removeGauge();
      }
      setGaugeOptions(
        GaugeChart.gaugeChart(gaugeContainer, gaugeWidth, {
          hasNeedle: true,
          needleColor: "#CACACA",
          needleUpdateSpeed: 1000,
          arcColors: arcColors,
          arcDelimiters: arcDelimiters,
          rangeLabel: rangeLabel,
          centralLabel: formattedValue,
        })
      );
    }
  }, [gaugeContainer, arcColors, arcDelimiters, formattedValue, rangeLabel, gaugeWidth]);

  useEffect(() => {
    if (gaugeOptions) {
      gaugeOptions.updateNeedle(calculatePercentage(gaugeMin, gaugeMax, currentValue));
    }
  }, [gaugeOptions, currentValue, gaugeMin, gaugeMax]);

  useEffect(() => {
    if (gaugeContainer) {
      const unwatch = resizeObserver(gaugeContainer, () => {
        setGaugeWidth(gaugeContainer.offsetWidth);
      });
      return unwatch;
    }
  }, [gaugeContainer]);

  return (
    <div
      className={cx("counter-visualization-container", {
        "trend-positive": true,
        "trend-negative": true,
      })}>
      <div id="gauge" className="counter-visualization-content" ref={setGaugeContainer}></div>
      {options.showLabel ? <h3 style={{ paddingBottom: "10px" }}>{options.labelName}</h3> : ""}{" "}
    </div>
  );
}

Renderer.propsTypes = RendererPropTypes;
