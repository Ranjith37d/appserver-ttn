import React from "react";
import cx from "classnames";

export const DeviceDetails = props => {
  return (
    <>
      <div className={cx("bg-white tiled p-15 m-t-15 m-l-15 m-r-15")}>
        <h5>{props.name}</h5>
      </div>
    </>
  );
};
