import React, { useState, useEffect } from "react";
import { RendererPropTypes } from "@/visualizations/prop-types";
import "./render.less";
import Text from "../text/Renderer";

export default function Renderer({ data, options, visualizationName }) {
  const [scale, setScale] = useState("1.00");
  const [container, setContainer] = useState(null);
  const [currentValue, setCurrentValue] = useState(0);
  const [dustBinContainer, setDustBinContainer] = useState();
  const [textValueContainer, setTextvalueContainer] = useState();
  const [labelTextContainer, setLabelTextContainer] = useState();

  // Filter Value
  const filtervalue = () => {
    if (options.ParameterData !== undefined) {
      if (data.rows.length !== 0) {
        setCurrentValue(data.rows[0][options.ParameterData]);
        options.value = currentValue;
      } else {
        options.value = 0;
      }
    }
  };

  useEffect(() => {
    if (container) {
      if (options.ParameterData.length >= 1) {
        filtervalue();
        battteryPercentCalculation();

      }
    }
  }, [options, container, currentValue, dustBinContainer]);

  const battteryPercentCalculation = () => {
    const BATTERY_MIN = 1;
    const BATTERY_MAX = 0;
    const RANGE_MIN = options.range[0];
    const RANGE_MAX = options.range[1];
    const BATTERY_FILL_PER_UNIT = (BATTERY_MIN - BATTERY_MAX) / (RANGE_MAX - RANGE_MIN);
    var currentVal = currentValue > RANGE_MAX ? RANGE_MAX : currentValue < RANGE_MIN ? RANGE_MIN : currentValue-RANGE_MIN;

    const batteryFill = BATTERY_FILL_PER_UNIT * currentVal;
    var state = options.states.find(value => value.from <= currentValue && value.to >= currentValue);

    if (!(dustBinContainer == null || dustBinContainer == undefined)) {
      if (options.range[0] < options.range[1]&& options.range[0] <=currentValue) {
     
        if (batteryFill <= 1) {
          dustBinContainer.style.transform = `scaleY(${batteryFill})`;
        } else {
          dustBinContainer.style.transform = `scaleY(1)`;
        }

        if (!(state == null || state == undefined)) {
          dustBinContainer.style.fill = state.color;
          textValueContainer.style.color = state.color;
          labelTextContainer.style.color = state.color;
        } else {
          dustBinContainer.style.fill = "#43B05C";
          textValueContainer.style.color = "#43B05C";
          labelTextContainer.style.color = "#43B05C";
        }
      } else {
        dustBinContainer.style.transform = "scaleY(0)";
      }
    }
    if(state){
      labelTextContainer.style.color = state.color;
      textValueContainer.style.color = state.color;

    }
  };

  return (
    <div className="bulb-viz-container">
      <div className="counter-layout">
          {options.showIcon && (
        <div className="column col-main">
            <svg version="1.1" id="Layer_1" x="0px" y="0px" viewBox="0 0 640 480">
              <path
                ref={setDustBinContainer}
                className="dustbin0"
                d="M245,350.4l-76.3,0.1c-7.3,0-13.1-5.3-13.3-12.1L148.8,180h114.3L255,341.4C254.9,354.3,237.1,350.4,245,350.4z
	"
              />
              <path
                className="dustbin1"
                d="M169.6,351.1c-6.9,0-12.5-5.3-12.8-12.2l-8.1-187.3h11.6v17.5c0,2.6,2.1,4.8,4.8,4.8h11.9
	c2.6,0,4.8-2.1,4.8-4.8v-17.5H216v17.5c0,2.6,2.1,4.8,4.8,4.8h11.9c2.6,0,4.8-2.1,4.8-4.8v-17.5h14.5l-6.2,142.5c-0.4,0-0.9,0-1.3,0
	c-17.1,0-31.1,13.9-31.1,31c0,11,5.8,20.7,14.5,26.2L169.6,351.1L169.6,351.1z M250.5,294.6l6.2-143.1h5.1c3.2,0,5.8-2.6,5.8-5.8
	v-11.4c0-3.1-2.6-5.8-5.8-5.8h-3.3c-1.1-5.9-6.4-10.5-12.7-10.5h-22.9c-1.1-6.1-6.5-10.8-13.1-10.8h-22.1c-6.5,0-11.9,4.7-13.1,10.8
	h-22.9c-6.3,0-11.6,4.5-12.7,10.5h-3.3c-3.2,0-5.8,2.6-5.8,5.8v11.4c0,3.1,2.6,5.8,5.8,5.8h7.9l7.8,179.4l0.4,8.2
	c0.3,5.9,3.4,11,8,14c2.8,1.8,6.1,2.8,9.6,2.8c0,0,3,0,7.8,0c19.1,0,66.6,0,67,0c17.1,0,31.1-13.9,31.1-31
	C275.4,310,264.7,297.5,250.5,294.6z M187.8,112.2h22.1c3.9,0,7.1,2.6,8.1,6h-38.4C180.6,114.7,183.9,112.2,187.8,112.2z M151.8,123
	h94c3.6,0,6.7,2.4,7.7,5.7H144.1C145.1,125.4,148.2,123,151.8,123z M134.9,145.8v-11.4c0-0.5,0.4-1,1-1h125.9c0.5,0,1,0.4,1,1v11.4
	c0,0.5-0.4,1-1,1H135.9C135.3,146.8,134.9,146.3,134.9,145.8z M220.8,151.6h11.8v17.5h-11.8V151.6z M165.1,151.6h11.8v17.5h-11.8
	V151.6L165.1,151.6z M244.4,351.1c-14.5,0-26.3-11.7-26.3-26.2c0-14.4,11.8-26.2,26.3-26.2s26.3,11.7,26.3,26.2
	C270.7,339.4,258.9,351.1,244.4,351.1z"
              />
              <path
                className="dustbin2"
                d="M245.2,304.3c-11.4,0-20.7,9.3-20.7,20.7s9.3,20.7,20.7,20.7c11.4,0,20.7-9.3,20.7-20.7
	S256.6,304.3,245.2,304.3z M245.2,340.8c-8.8,0-15.9-7.1-15.9-15.9s7.1-15.9,15.9-15.9s15.9,7.1,15.9,15.9S253.9,340.8,245.2,340.8z
	"
              />
              <ellipse className="dustbin3" cx="245" cy="323.9" rx="27.8" ry="29.3" />
              <ellipse className="dustbin4" cx="244.9" cy="324.4" rx="17.7" ry="17.7" />
              <path className="dustbin5" d="M251,296.7" />
              <path className="dustbin5" d="M252.5,181" />
              <g>
                <path
                  className="dustbin6"
                  d="M205.5,231.2l-7,0c0.2,0.1,0.8,0.2,1.1,0.8l3.4,6l-1.9,1.1l6.2,0l3-5.3l-1.9,1.1l-1.8-3.1
		C206.5,231.4,206.3,231.2,205.5,231.2z M197.2,231.5c-0.9-0.1-2,0.3-2.8,1.4l-0.1,0.1l-2.6,4.4l5.4,3.1l3.4-5.8l-1.3-2.2
		C198.9,232,198.2,231.5,197.2,231.5z M210.9,239l-5.4,3.1l3.4,5.8h2.6c1,0,3.1-1.9,2-4.3l0-0.1L210.9,239z M187.5,240.2l1.9,1.1
		l-1.8,3.1c-0.2,0.4-0.3,0.7,0.1,1.3l3.6,6.1c-0.1-0.3-0.2-0.8,0.1-1.4l3.5-6l1.9,1.1l-3.1-5.3L187.5,240.2L187.5,240.2z
		 M204.4,246.3l-3.1,5.3l3,5.3l0-2.2h3.6c0.4,0,0.8-0.1,1.1-0.7l3.5-6.1c-0.2,0.2-0.6,0.6-1.3,0.6h-6.9L204.4,246.3L204.4,246.3z
		 M193.1,248.4l-1.3,2.2c-0.5,0.9,0.1,3.6,2.8,3.9l0.1,0h5.2v-6.2L193.1,248.4L193.1,248.4z"
                />
              </g>

              <path className="dustbin10" d="M256.7,151.6L251,290.7v0.9c0,0,8.2,1.5,11.7,5.1l9.2-130.7L256.7,151.6z" />
              <path className="dustbin0" d="M165.1,356" />
              <path
                className="dustbin10"
                d="M143.8,151.6l7.4,170c0,0-0.7,19.5,2.3,24.1s4.8,8.8,16.1,10.3s1.5,9.8,1.5,9.8l-23.6-4.1
	c-5.2-0.9-9-5.4-9.1-10.7l-2.6-199.3H143.8z"
              />
              <path className="dustbin10" d="M114.3,327.4" />
              <polygon className="dustbin10" points="166.9,356.4 163.9,364.4 259,371.3 249,356.5 " />
              <path className="dustbin10" d="M327.4,254.6" />
            </svg>
        </div>
          )}
        <div className="column col-complementary" role="complementary">
          {options && (
            <Text
              setlabelTextContainer={setLabelTextContainer}
              settextValueContainer={setTextvalueContainer}
              setcontainer={setContainer}
              setscale={setScale}
              options={options}
              visualizationName={visualizationName}
              data={data}
              container={container}
              scale={scale}
              currentLabel={options.labelName}
            />
          )}
        </div>
      </div>
    </div>
  );
}

Renderer.propTypes = RendererPropTypes;
