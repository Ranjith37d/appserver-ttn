import { trim } from "lodash";
import React, { useState } from "react";
import Modal from "antd/lib/modal";
import Input from "antd/lib/input";
import DynamicComponent from "@/components/DynamicComponent";
import { wrap as wrapDialog, DialogPropType } from "@/components/DialogWrapper";
import navigateTo from "@/components/ApplicationArea/navigateTo";
import { Form } from "antd";
import { Device } from "@/services/device";
import notification from "@/services/notification";
import { Alert } from "antd";

function CreateChannelModal({ dialog }) {
  const [name, setName] = useState("");
  const [id, setId] = useState("");
  const [isValid, setIsValid] = useState(false);
  const [saveInProgress, setSaveInProgress] = useState(false);
  const [validChannelId, setValidChannelId] = useState(false);
  const [form] =Form.useForm();
  function handleNameChange(event) {
    const value = trim(event.target.value);
    setName(value);
    setIsValid(value !== "" && id !== "");
  }
  function handleIdChange(event) {
    setId(trim(event.target.value));
    setIsValid(name !== "" && id !== "");
  }

  function save() {
    form.submit();
    form.validateFields().then(values=>{
  

      Device.createChannel({ channel_name: name, channel_id: id })
        .then(data => {
          dialog.close();
          navigateTo(`mqtt/${data.channel_id}`);
        })
        .catch(e => {
          setSaveInProgress(false);
          notification.error(e.response.data.message);
        });
    } ).catch(errorInfo=>console.log(errorInfo))
      
    
  }
  const validateChannelId = () => {
    const regex = /^[a-z0-9]+(-[a-z0-9]+)*$/;
    if (regex.test(id)) {
      setValidChannelId(false);
    } else {
      setValidChannelId(true);
    }
  };
  const layout = {
    labelCol: { span: 6 },
    wrapperCol: { span: 16 },
  };
  return (
    <Modal
      {...dialog.props}
      title="New Channel"
      okText="Save"
      cancelText="Close"
      // okButtonProps={{
      //   disabled: !isValid || saveInProgress,
      //   loading: saveInProgress,
      //   "data-test": "DashboardSaveButton",
      // }}
      cancelButtonProps={{
        disabled: saveInProgress,
      }}
      onOk={save}
      closable={!saveInProgress}
      maskClosable={!saveInProgress}
      wrapProps={{
        "data-test": "CreateDashboardDialog",
      }}>
      <DynamicComponent name="CreateDashboardDialogExtra">
        <Form form={form} {...layout}>
          <Form.Item name="Channel ID" label="Channel ID" rules={[{ required: true }]}>
            <Input
              defaultValue={id}
              onChange={handleIdChange}
              onPressEnter={save}
              placeholder="Channel ID"
              disabled={saveInProgress}
              onBlur={validateChannelId}
            />
           
          </Form.Item>
          <Form.Item name="Channel Name" label="Channel Name">
            <Input
              defaultValue={name}
              onChange={handleNameChange}
              onPressEnter={save}
              placeholder="Channel Name"
              disabled={saveInProgress}
            />
          </Form.Item>
        </Form>
      </DynamicComponent>
    </Modal>
  );
}

CreateChannelModal.propTypes = {
  dialog: DialogPropType.isRequired,
};

export default wrapDialog(CreateChannelModal);
