"""added new fields to downlink command

Revision ID: 6d77d304d861
Revises: 3a9ffeb17a91
Create Date: 2021-01-21 16:28:27.613168

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '6d77d304d861'
down_revision = '3a9ffeb17a91'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('alert_downlinks',
    sa.Column('updated_at', sa.DateTime(timezone=True), nullable=False),
    sa.Column('created_at', sa.DateTime(timezone=True), nullable=False),
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('user_id', sa.Integer(), nullable=False),
    sa.Column('downlink_id', sa.Integer(), nullable=False),
    sa.Column('alert_id', sa.Integer(), nullable=False),
    sa.Column('confirm', sa.Boolean(), nullable=False),
    sa.Column('fport', sa.Integer(), nullable=False),
    sa.Column('mode', sa.String(length=12), nullable=False),
    sa.Column('payload', sa.Text(), nullable=False),
    sa.ForeignKeyConstraint(['alert_id'], ['alerts.id'], ),
    sa.ForeignKeyConstraint(['downlink_id'], ['downlink_commands.id'], ),
    sa.ForeignKeyConstraint(['user_id'], ['users.id'], ),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_index('alert_downlinks_downlink_id_alert_id', 'alert_downlinks', ['downlink_id', 'alert_id'], unique=True)
    # op.drop_column('downlink_commands', 'query')
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    # op.add_column('downlink_commands', sa.Column('query', sa.TEXT(), autoincrement=False, nullable=False))
    # op.drop_column('downlink_commands', 'payload')
    # op.drop_column('downlink_commands', 'mode')
    # op.drop_column('downlink_commands', 'fport')
    # op.drop_column('downlink_commands', 'confirm')
    op.drop_index('alert_downlinks_downlink_id_alert_id', table_name='alert_downlinks')
    op.drop_table('alert_downlinks')
    # ### end Alembic commands ###
