import "./tankwidget.css";
import React, { useEffect, useState } from "react";
import resizeObserver from "@/services/resizeObserver";
import { isFinite } from "lodash";
import { getCounterData } from "@/visualizations/components/utils";
import Text from "../text/Renderer";


const Renderer = ({ data, options, visualizationName }) => {
  const [scale, setScale] = useState("1.00");
  const [container, setContainer] = useState(null);
  const [tankElement, setTankElement] = useState();
  const [contextValue, setContextValue] = useState(0);
  const [textValueContainer, setTextvalueContainer] = useState();
  const [labelTextContainer, setLabelTextContainer] = useState();

  useEffect(() => {
    if (container) {

      if (options.ParameterData.length >= 1) {
        filtervalue();
        Watermeter();
      }
    }
  }, [options, container, contextValue,tankElement]);



  // Filter Value
  const filtervalue = () => {
    if (options.ParameterData !== undefined) {
      if (data.rows.length !== 0) {
        setContextValue(data.rows[0][options.ParameterData]);
        options.value = contextValue;
      } else {
        options.value = 0;
      }
    }
  };


  const Watermeter = () => {
    const BATTERY_MIN = 1;
    const BATTERY_MAX = 0;
    const RANGE_MIN = options.range[0];
    const RANGE_MAX = options.range[1];
    const BATTERY_FILL_PER_UNIT = (BATTERY_MIN - BATTERY_MAX) / (RANGE_MAX - RANGE_MIN);
    var currentVal = contextValue > RANGE_MAX ? RANGE_MAX : contextValue < RANGE_MIN ? RANGE_MIN : contextValue;

    const batteryFill = BATTERY_FILL_PER_UNIT * currentVal;
    var state = options.states.find(value => value.from <= contextValue && value.to >= contextValue);

    if (options.range[0] < options.range[1]) {
      if (!(state == null || state == undefined)) {
        if (batteryFill <= 1) {
          if ((!tankElement == null || !tankElement == undefined)) {
            tankElement.style.transform = `scaleY(${batteryFill})`;
            tankElement.style.fill = state.color;
          }
          labelTextContainer.style.color = state.color;
          textValueContainer.style.color = state.color;
        } else {
          if (!(!tankElement == null || !tankElement == undefined)) {
 
            tankElement.style.transform = `scaleY(1)`;
            tankElement.style.fill = state.color;
          }         
          labelTextContainer.style.color = state.color;
          textValueContainer.style.color = state.color;
        }
      } else {
        //handle, when min value is greater than mx value
        if (!tankElement == null) {
          tankElement.style.transform = `scaleY(0)`;
        }
      }
    }
  };


  return (
    <>
      <div className="bulb-viz-container">
        <div className="counter-layout">
            {options.showIcon && (
          <div className="column col-main">
              <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" viewBox="0 0 640 480">
                <linearGradient
                  id="SVGID_1_"
                  gradientUnits="userSpaceOnUse"
                  x1="186.6756"
                  y1="402.5"
                  x2="186.6756"
                  y2="159.5">
                  <stop offset="0" style={{ stopColor: "#CCE0F4" }} />
                  <stop offset="0" style={{ stopColor: "#C3DAF0" }} />
                  <stop offset="0" style={{ stopColor: "#A9CBE6" }} />
                  <stop offset="0" style={{ stopColor: "#80B1D6" }} />
                  <stop offset="0" style={{ stopColor: "#468EBF" }} />
                  <stop offset="0" style={{ stopColor: "#0063A3" }} />
                  <stop offset="0.0826" style={{ stopColor: "#0E7DBA" }} />
                  <stop offset="0.1758" style={{ stopColor: "#1A94CF" }} />
                  <stop offset="0.2346" style={{ stopColor: "#1E9CD7" }} />
                  <stop offset="0.2383" style={{ stopColor: "#1D9AD6" }} />
                  <stop offset="0.2808" style={{ stopColor: "#1089CB" }} />
                  <stop offset="0.3336" style={{ stopColor: "#077EC4" }} />
                  <stop offset="0.4078" style={{ stopColor: "#0177BF" }} />
                  <stop offset="0.6138" style={{ stopColor: "#0075BE" }} />
                  <stop offset="0.7577" style={{ stopColor: "#0276BE" }} />
                  <stop offset="0.8095" style={{ stopColor: "#097AC0" }} />
                  <stop offset="0.8465" style={{ stopColor: "#1480C3" }} />
                  <stop offset="0.8764" style={{ stopColor: "#2589C8" }} />
                  <stop offset="0.902" style={{ stopColor: "#3B95CD" }} />
                  <stop offset="0.9247" style={{ stopColor: "#57A4D4" }} />
                  <stop offset="0.9452" style={{ stopColor: "#77B5DC" }} />
                  <stop offset="0.964" style={{ stopColor: "#9DCAE6" }} />
                  <stop offset="0.9816" style={{ stopColor: "#C8E1F1" }} />
                  <stop offset="0.9971" style={{ stopColor: "#F6FAFD" }} />
                  <stop offset="1" style={{ stopColor: "#FFFFFF" }} />
                </linearGradient>

                <path
                  className="tank0"
                  ref={setTankElement}
                  d="M56.5,159.5c0,0,95.5,28.5,260,5v212c0,0,4,14-15,18s-77,8-96,8s-123-4-123-4s-26,0-26-16  S56.5,159.5,56.5,159.5z"
                />
                <path className="tankst1" d="M37.29,214.58" />

                <g>
                  <path className="tank2" d="M197.89,136.29" />
                  <path
                    className="tank2"
                    d="M138.62,101.03c0,0-81.29,9.94-81.91,41c-0.63,31.06,0,34.17,0,34.17L56.5,376.5c0,0-8.17,22.25,47.5,24.77
		c36.02,1.63,42.41,2.23,121.93,1c0,0,65.57,0.24,81.6-9.88c8.86-5.59,8.04-7.45,9.29-15.53s0.68-24.35,0.63-90.08l-1.82-55.29
		c-0.12,0.01-0.12,20.01,0.57-15.53v-22.99c-0.69-0.47-0.34,1.85,0-14.91l0.63-36.65c0,0,12.51-21.74-83.16-37.89l-0.63-9.94
		c0,0,5.61,4.04-46.9,4.04c-30.49,0-22.82-0.32-45.64-4.04L138.62,101.03z"
                  />
                  <path
                    className="tank2"
                    d="M316.82,141.41c-10.66,6.86-36.21,11.13-62.38,13.78c-27.71,2.8-56.11,3.79-68.3,4.1
		c-3.92,0.1-6.16,0.13-6.16,0.13c-129.4-6.15-123.27-17.39-123.27-17.39"
                  />
                  <path className="tank2" d="M186.19,203.22" />
                  <path className="tank2" d="M56.43,192.35c105.29,15.62,132.07,16.15,259.77,0.62" />
                  <path
                    className="tank2"
                    d="M56.71,232.11c0,0,47.2,13.87,129.48,13.77c89.32-0.11,108.29-6.63,129.44-14.39"
                  />
                  <path
                    className="tank2"
                    d="M56.71,270.62c0,0,65.22,14.42,122.5,14.6c94.87,0.3,123.13-7.6,136.99-15.22"
                  />
                  <path
                    className="tank2"
                    d="M56.71,305.41c0,0,1.25,16.77,131.48,19.88c-0.41,0.16,132.32,0.21,127.32-19.79"
                  />
                  <path
                    className="tank2"
                    d="M57.33,344.55c0,0-12.28,13.36,124.52,17.08c0.64-0.1,104.65,7.87,134.97-17.08"
                  />
                  <path
                    className="tank2"
                    d="M233.66,103.52c0,0,0.84,7.05-45.48,7.77c-23.36,0.36-49.68-4.78-49.57-10.25"
                  />
                  <path className="tank2" d="M140.49,93.58c0,0-10.89-6.83,46.27-6.21c0,0,45.87-0.11,46.27,6.21" />
                  <path className="tank2" d="M205.76,94.04" />
                  <path className="tank2" d="M168.89,93.58" />
                </g>
                <path className="tank3" d="M364,395" />
                <path className="tank3" d="M295.26,398.76" />
                <polyline className="tank2" points="172.5,93.5 172.5,75.5 205.5,75.5 205.5,94.5 " />

                <path
                  className="tank9"
                  d="M56.2,385.8c0,0,2.48,4.86,8.03,7.81c5.55,2.95,8.11,4.72,14.37,5.75c6.26,1.03,9.96,2.01,9.96,2.01
	l-22.91,4.77H56.2V385.8z"
                />
                <path className="tank10" d="M316.85,385.28" />
                <path className="tank10" d="M271.43,401.48" />
                <path className="tank10" d="M225.71,420.29" />

                <path className="tank11" d="M303.86,395.86" />
                <path
                  className="tank9"
                  d="M276.86,401.41c0,0,20.78-3.05,26.99-5.55s12.64-9.36,12.64-9.36l1.12,16.42l-28.12,10.15L276.86,401.41z"
                />
                <polygon className="tank9" points="88.57,401.37 101.14,402.92 103.71,413.07 80,415.14 80,401.37 " />
                <polygon className="tank9" points="268.86,402.21 276.86,401.41 282.29,416.57 262.57,420.29 " />
              </svg>
          </div>
            )}
          <div className="column col-complementary" role="complementary">
            {options && (
              <Text
                setlabelTextContainer={setLabelTextContainer}
                settextValueContainer={setTextvalueContainer}
                setcontainer={setContainer}
                setscale={setScale}
                options={options}
                visualizationName={visualizationName}
                data={data}
                container={container}
                scale={scale}
                currentLabel={options.labelName}
              />
            )}
          </div>
        </div>
      </div>
    </>
  );
};
export default Renderer;
