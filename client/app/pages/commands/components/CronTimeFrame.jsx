import React, { useEffect, useState } from "react";
import Cron from "../cron-generator/lib/cron";
// import '../lib/react-cron-generator/dist/cron-builder.css'
import "./crontimeframe.css";
import TimeZoneComponent from "./TimezoneComponent";

const CronTimeFrame = props => {
  const [value, setValue] = useState("");

  useEffect(() => {
    props.onChange(value);
  }, [value]);

  return (
    <div>
      <TimeZoneComponent setTimezone={props.setTimezone} />

      <Cron
        onChange={e => {
          props.onChange(e);
        }}
        value={value}
        time={props.time}
        settime={props.settime}
        showText={false}
        showResultText={true}
        showResultCron={true}
        schedulesType={props.schedulesType}
      />
    </div>
  );
};

export default CronTimeFrame;
