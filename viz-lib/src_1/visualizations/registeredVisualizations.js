// this file has been  include all the widget types

import { find, flatten, each } from "lodash";
import PropTypes from "prop-types";

import boxPlotVisualization from "./box-plot";
import chartVisualization from "./chart";
import compass from "./compass";
import counterVisualization from "./counter";
import detailsVisualization from "./details";
import mapVisualization from "./map";
import indicator from "./indicator";
import dustbin from "./dustbin";
import parking from "./parking";
import tableVisualization from "./table";
import thermometer from "./thermometer";
import gague from "./gague";
import tank from "./tank";
import bulb from "./bulb"
// import downlik from "./switch";
import newGauge from "./gaugeNew";
import formdownlink from "./formdownlink";
import battery from "./battery";
const VisualizationConfig = PropTypes.shape({
  type: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  getOptions: PropTypes.func.isRequired, // (existingOptions: object, data: { columns[], rows[] }) => object
  isDefault: PropTypes.bool,
  isDeprecated: PropTypes.bool,
  Renderer: PropTypes.func.isRequired,
  Editor: PropTypes.func,

  // other config options
  autoHeight: PropTypes.bool,
  defaultRows: PropTypes.number,
  defaultColumns: PropTypes.number,
  minRows: PropTypes.number,
  maxRows: PropTypes.number,
  minColumns: PropTypes.number,
  maxColumns: PropTypes.number,
});

const registeredVisualizations = {};

function validateVisualizationConfig(config) {
  const typeSpecs = { config: VisualizationConfig };
  const values = { config };
  PropTypes.checkPropTypes(typeSpecs, values, "prop", "registerVisualization");
}
// ====|
//     |
//     this is function excute 1 st
function registerVisualization(config) {
  validateVisualizationConfig(config);
  config = {
    Editor: () => null,
    ...config,
    isDefault: config.isDefault && !config.isDeprecated,
  };

  if (registeredVisualizations[config.type]) {
    throw new Error(`Visualization ${config.type} already registered.`);
  }

  registeredVisualizations[config.type] = config;
}

each(
  flatten([
    boxPlotVisualization,
    chartVisualization,
    // choroplethVisualization,
    // downlik,
    counterVisualization,
    detailsVisualization,
    formdownlink,
    newGauge,
    mapVisualization,
    battery,
    thermometer,
    tank,
    tableVisualization,
    indicator,
    gague,
    dustbin,
    parking,
    compass,
    bulb
  ]),
  registerVisualization
);

export default registeredVisualizations;

export function getDefaultVisualization() {
  // return any visualization explicitly marked as default, or any non-deprecated otherwise
  return (
    find(registeredVisualizations, visualization => visualization.isDefault) ||
    find(registeredVisualizations, visualization => !visualization.isDeprecated)
  );
}

export function newVisualization(type = null, options = {}) {
  const visualization = type ? registeredVisualizations[type] : getDefaultVisualization();
  return {
    type: visualization.type,
    name: visualization.name,
    description: "",
    options,
  };
}
