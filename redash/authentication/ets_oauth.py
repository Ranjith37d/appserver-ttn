import os 
os.environ['OAUTHLIB_INSECURE_TRANSPORT'] = '1'

import logging
import requests
from flask import redirect, Response, url_for, Blueprint, flash, request, session, render_template

from flask_dance.consumer import OAuth2ConsumerBlueprint

from functools import partial
from flask.globals import LocalProxy, _lookup_app_object

try:
    from flask import _app_ctx_stack as stack
except ImportError:
    from flask import _request_ctx_stack as stack


from redash import models, settings
from redash.authentication import (
    create_and_login_user,
    logout_and_redirect_to_index,
    get_next_path,
)
from redash.authentication.org_resolving import current_org

logger = logging.getLogger("ets_oauth")

blueprint = OAuth2ConsumerBlueprint(
    "ets", __name__,
    client_id="console",
    client_secret="AxdRuzCovM@r3xv23ipyg%",
    base_url='https://beta.thethingsmate.com/',
    login_url='https://beta.thethingsmate.com/console/',
    token_url='https://beta.thethingsmate.com/oauth/token',
    auto_refresh_url='https://beta.thethingsmate.com/console/api/auth/token',
    authorization_url='https://beta.thethingsmate.com/oauth/authorize',    
    redirect_to= '.auth_callback'
)

@blueprint.before_app_request
def set_applocal_session():
    ctx = stack.top
    ctx.ets_oauth = blueprint.session

ets = LocalProxy(partial(_lookup_app_object, "ets_oauth"))

@blueprint.route("/auth_callback", endpoint="auth_callback")
def ets_authorized():
    if not ets.authorized:
        logger.warning("Access token missing in call back request.")
        #flash("Validation error. Please retry.")
        return redirect(url_for("ets.login"))

    resp = ets.get("/api/v3/auth_info")

    if resp.status_code == 401:
        logger.warning("Failed getting user auth info (response code 401).")
        return redirect(url_for("ets.login"))
    
    username = resp.json()["oauth_access_token"]["user_ids"]["user_id"]
    profile_url = "/api/v3/users/{user}?field_mask=state,name,primary_email_address_validated_at,primary_email_address".format(user=username)
    resp = ets.get(profile_url)

    if resp.status_code == 401:
        logger.warning("Failed getting user profile (response code 401).")
        return None
    
    profile = resp.json()
    if profile is None:
        # flash("Validation error. Please retry.")
        logger.warning("Failed getting user profile (response code 401).")
        return redirect(url_for('ets.unapproved'))
    if 'state' not in profile or profile["state"] != 'STATE_APPROVED':
        logger.warning("Your account {username} is not yet approved".format(username=username))
        return redirect(url_for('ets.unapproved'))

    if "org_slug" in session:
        org = models.Organization.get_by_slug(session.pop("org_slug"))
    else:
        org = current_org
    
    user = create_and_login_user(org, profile["name"], profile["primary_email_address"], "")
    if user is None:
        return logout_and_redirect_to_index()

    unsafe_next_path = request.args.get("state") or "/console/app/dashboards?org_slug=default" or url_for(
        "redash.index", org_slug=org.slug
    )
    next_path = get_next_path(unsafe_next_path)
    return redirect(next_path)

@blueprint.route("/<org_slug>/oauth/ets", endpoint="authorize_org")
def org_login(org_slug):
    session["org_slug"] = current_org.slug
    return redirect(url_for(".login", next=request.args.get("next", None)))

@blueprint.route("/logout", endpoint="logout")
def ets_logout():
    
    if ets.authorized :
        if blueprint.token['expires_in'] > 0 :
            try:
                resp = ets.get("/console")
                _csrf = resp.headers['X-Csrf-Token']
                resp = ets.get("/console/api/auth/token")
                if resp.status_code == 200:
                    resp = ets.post("/console/api/auth/logout", headers={'Referer': 'https://beta.thethingsmate.com/', 'X-Csrf-Token' : _csrf, 'Authorization': 'Bearer {}'.format(ets.access_token)})
                    if resp.status_code != 200:
                        logging.error(
                        "logout error. status_code => {status} status_message => {message}".format(
                            status=resp.status_code, message=resp.status
                        )
                    )
                    
            except Exception:
                logging.exception("EXCEPTION WHILE LOGGING OUT.")
    del blueprint.token
    
@blueprint.route("/unapproved", endpoint="unapproved")
def unapproved():
    return (render_template("unapproved.html"),401,)