import Renderer from "./Renderer";
import Editor from "./Editor";
const DEFAULT_OPTIONS = {
  rowNumber: 1,
  targetRowNumber: 1,
  counterColName: "counter",
  stringDecimal: 0,
  stringDecChar: ".",
  stringThouSep: ",",
  tooltipFormat: "0,0.000",
  occupied: "",
  available: "",
  ParameterData: "",
  labelName: "",
  showLabel:true,
  showValue:false,
  showIcon:true,
  states: [
    {
      key: 1,
      label: "Occupied",
      value: 1,
      color: "#ff0000",
    },
    {
      key: 2,
      label: "Available",
      value: 0,
      color: "#43B05C",
    },
  ],
};
export default {
  type: "PARKING",
  name: "Parking",
  isDefault: true,

  getOptions: options => ({ ...DEFAULT_OPTIONS, ...options }),
  Renderer,
  Editor,
  defaultColumns: 3,
  defaultRows: 8,
  minColumns: 1,
  minRows: 5,
};


