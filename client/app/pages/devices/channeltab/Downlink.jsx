import React, { useState, useEffect } from "react";
import { Input, Form, Button, Row, Col } from "antd";
import routeWithUserSession from "@/components/ApplicationArea/routeWithUserSession";
import routes from "@/services/routes";
import io from "socket.io-client";
import cx from "classnames";
import HelpTrigger from "@/components/HelpTrigger";
import notification from "@/services/notification";
import { Alert } from "antd";

export const MqttDownLink = props => {
  const layout = {
    labelCol: { span: 1 },
    wrapperCol: { span: 8 },
  };
  useEffect(() => {}, []);

  const [downlink, setDownlink] = useState("");
  const [validJson, setvalidJson] = useState(false);
  const handleDownlink = () => {
    if (downlink.length >= 1) {
      props.socketApi.on("connect", function() {
        props.socketApi.emit("downlink", props.device.channel_id, props.device.device_id, downlink);
      });
      props.socketApi.emit("downlink", props.device.channel_id, props.device.device_id, downlink);
    } else {
      setvalidJson(true);
    }
  };
  //validate Json

  return (
    <div>
      <Form {...layout} name="basic" initialValues={{ remember: true }}>
        <Form.Item label="payload" className="m-b-10">
          <Input
            id="payload"
            data-test="deviceId"
            placeholder="Enter the payload "
            onChange={e => setDownlink(e.target.value)}
          />
        </Form.Item>
        <Form.Item {...layout} className="m-b-10">
          {validJson ? (
            <Form.Item {...layout} className="m-b-10">
              {" "}
              <Alert message="Not a valid value" type="error" banner />
            </Form.Item>
          ) : null}
          <Button type="primary" onClick={handleDownlink}>
            Schedule downlink
          </Button>
        </Form.Item>
      </Form>
    </div>
  );
};
