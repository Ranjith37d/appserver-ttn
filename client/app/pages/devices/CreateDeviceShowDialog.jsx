import { trim } from "lodash";
import React, { useState } from "react";
import Modal from "antd/lib/modal";
import Input from "antd/lib/input";
import DynamicComponent from "@/components/DynamicComponent";
import { wrap as wrapDialog, DialogPropType } from "@/components/DialogWrapper";
import navigateTo from "@/components/ApplicationArea/navigateTo";
import recordEvent from "@/services/recordEvent";
import { policy } from "@/services/policy";
import { Device } from "@/services/device";
import { Form, Alert } from "antd";

import notification from "@/services/notification";
function CreateDeviceDialog({ dialog }) {
  const [channelid, setchannelId] = useState(dialog.props.parameterId);
  const [name, setName] = useState('');
  const [deviceId, setDeviceId] = useState('');
  const [url, setUrl] = useState("mqtt/" + channelid + "/devices/");
  const [isValid, setIsValid] = useState(true);
  const [isValidDevice, setIsValidDevice] = useState(false);

  const [saveInProgress, setSaveInProgress] = useState(false);

  function handleNameChange(event) {
    const value = trim(event.target.value);
    setName(value);
    setIsValid(!(value == ""));
  }
  function handleDeviceIdChange(event) {
    setDeviceId(event.target.value);
  }

  function save() {
    if (name.length >= 1 && deviceId.length >= 1 && !isValidDevice) {
      setSaveInProgress(true);
      Device.createDevice(name, dialog.props.parameterId, deviceId)
        .then(data => {
          notification.success("Device Added");
          dialog.close();
          navigateTo(url + data.device_id);
        })
        .catch(e => {
          setSaveInProgress(false);
          if (e.response) {
          }
          notification.error(e.response.data.message);
        });
      recordEvent("create", "dashboard");
    } else {
      notification.error("fill all the value");
    }
  }
  const layout = {
    labelCol: { span: 6 },
    wrapperCol: { span: 16 },
  };
  const taillayout = {
    labelCol: { span: 6 },
    wrapperCol: { span: 16 },
  };
  const validateDeviceId = () => {
    const regex = /^[a-z0-9]+(-[a-z0-9]+)*$/;
    if (regex.test(deviceId)) {
      setIsValidDevice(false);
    } else {
      setIsValidDevice(true);
    }
  };

  return (
    <Modal
      {...dialog.props}
      title="New Device"
      okText="Save"
      cancelText="Close"
      okButtonProps={{
        loading: saveInProgress,
        "data-test": "DashboardSaveButton",
      }}
      cancelButtonProps={{
        disabled: saveInProgress,
      }}
      onOk={save}
      closable={!saveInProgress}
      maskClosable={!saveInProgress}
      wrapProps={{
        "data-test": "CreateDashboardDialog",
      }}>
      <DynamicComponent name="CreateDashboardDialogExtra">
        <Form>
          <Form.Item {...layout} name="Device ID" label="Device ID">
            <Input
              defaultValue={deviceId}
              onChange={handleDeviceIdChange}
              onPressEnter={save}
              placeholder="Device Id"
              disabled={saveInProgress}
              onBlur={validateDeviceId}
            />
          </Form.Item>
          {isValidDevice ? (
            <Form.Item {...taillayout}>
              {" "}
              <Alert message="Device_id must match the following: /^[a-z0-9]+(-[a-z0-9]+)*$/" type="error" banner />
            </Form.Item>
          ) : null}
          <Form.Item {...layout} name="Device Name" label="Device Name" rules={[{ required: true }]}>
            <Input
              defaultValue={name}
              onChange={handleNameChange}
              onPressEnter={save}
              placeholder="Device Name"
              disabled={saveInProgress}
            />
          </Form.Item>
        </Form>
      </DynamicComponent>
    </Modal>
  );
}

CreateDeviceDialog.propTypes = {
  dialog: DialogPropType.isRequired,
};

export default wrapDialog(CreateDeviceDialog);
