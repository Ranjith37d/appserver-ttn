import createTabbedEditor from "@/components/visualizations/editor/createTabbedEditor";
import RangeIndicationSettings from "@/visualizations/battery/Editor/RangeIndiactionSettings";
import GeneralSettings from "@/visualizations/components/GeneralSettings";
import FormatSettings from "@/visualizations/components/FormatSettings";

export default createTabbedEditor([
  { key: "General", title: "General", component: GeneralSettings },
  { key: "state", title: "State", component: RangeIndicationSettings },
  { key: "Format", title: "Format", component: FormatSettings },
]);
