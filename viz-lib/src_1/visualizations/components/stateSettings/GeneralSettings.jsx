import React from "react";
import { map } from "lodash";
import Form from "antd/lib/form";
import { Section, Select, Input } from "@/components/visualizations/editor";

const GenralSettings = ({ form, options, data, visualizationName, onOptionsChange }) => {
  return (
    <>
      <Section>
        <Form form={form[0]} initialValues={{ Parameter: options.ParameterData, labelName: options.labelName }}>
          <Form.Item name="Parameter" rules={[{ required: true }]}>
            <Select
              layout="horizontal"
              label="Select parameter"
              data-test="Counter.General.ValueColumn"
              defaultValue={options.ParameterData}
              disabled={options.countRow}
              onChange={selectedValue => onOptionsChange({ ParameterData: selectedValue })}>
              {map(data.columns, col => (
                <Select.Option key={col.name} data-test={"Counter.General.ValueColumn." + col.name}>
                  {col.name}
                </Select.Option>
              ))}
            </Select>
          </Form.Item>
          <Section>
            <Form.Item name="labelName" >
              <Input
                label="LabelName"
                placeholder="LabelName"
                type="text"
                onChange={e => onOptionsChange({ labelName: e.target.value })}
                defaultValue={options.labelName}
              />
            </Form.Item>
          </Section>
        </Form>
      </Section>
    </>
  );
};

export default GenralSettings;
