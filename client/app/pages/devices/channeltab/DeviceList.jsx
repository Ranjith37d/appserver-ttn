import { toUpper } from "lodash";
import React from "react";
import routeWithUserSession from "@/components/ApplicationArea/routeWithUserSession";
import Link from "@/components/Link";
import PageHeader from "@/components/PageHeader";
import Paginator from "@/components/Paginator";
import EmptyState from "@/components/empty-state/EmptyState";
import { wrap as itemsList, ControllerType } from "@/components/items-list/ItemsList";
import { ResourceItemsSource } from "@/components/items-list/classes/ItemsSource";
import { StateStorage } from "@/components/items-list/classes/StateStorage";
import Button from "antd/lib/button";
import CreateDeviceDialog from "../CreateDeviceShowDialog";
import ItemsTable, { Columns } from "@/components/items-list/components/ItemsTable";
import { CurrentRoute } from "@/services/routes";
import { Device } from "@/services/device";
import { currentUser } from "@/services/auth";
import routes from "@/services/routes";
import { ChannelConsumer } from "../DeviceChannelOverview";

localStorage.debug = "*";
export const STATE_CLASS = {
  unknown: "label-warning",
  ok: "label-success",
  triggered: "label-danger",
};

export class DeviceList extends React.Component {
  static propTypes = {
    controller: ControllerType.isRequired,
  };

  state = { url: "" };

  componentDidMount() {
    this.setState({ url: "mqtt/" + this.props.channel_id + "/devices/" });
  }

  listColumns = [
    Columns.custom.sortable(
      (text, device) => (
        <div>
          <Link className="table-main-title" href={this.state.url + device.device_id}>
            {device.name}
          </Link>
        </div>
      ),
      {
        title: "Name",
        field: "name",
      }
    ),
    Columns.custom.sortable(
      (text, device) => (
        <div>
          <Link className="table-main-title" href={this.state.url + device.device_id}>
            {device.api_key}
          </Link>
        </div>
      ),
      { width: "1%", title: "Api Key", field: "api_key" }
    ),
    Columns.dateTime.sortable({ title: "Created At", field: "created_at", width: "1%" }),
  ];

  render() {
    const { controller } = this.props;
    return (
      <div className="page-alerts-list">
        <div className="container">
          <PageHeader
            title={controller.params.pageTitle}
            actions={
              <Button
                value="large"
                type="primary"
                onClick={() => CreateDeviceDialog.showModal(controller.params.channel_id)}>
                <i className="fa fa-plus m-r-5" />
                Add Device
              </Button>
            }
          />
          <div>
            {controller.isLoaded ? (
              <div className="table-responsive bg-white tiled">
                <ItemsTable
                  loading={!controller.isLoaded}
                  items={controller.pageItems}
                  columns={this.listColumns}
                  orderByField={controller.orderByField}
                  orderByReverse={controller.orderByReverse}
                  toggleSorting={controller.toggleSorting}
                />
                <Paginator
                  showPageSizeSelect
                  totalCount={controller.totalItemsCount}
                  pageSize={controller.itemsPerPage}
                  onPageSizeChange={itemsPerPage => controller.updatePagination({ itemsPerPage })}
                  page={controller.page}
                  onChange={page => controller.updatePagination({ page })}
                />
              </div>
            ) : null}
          </div>
        </div>
      </div>
    );
  }
}
export const DeviceListPage = itemsList(
  DeviceList,
  props =>
    new ResourceItemsSource({
      isPlainList: true,
      getRequest() {
        return {};
      },
      getResource(d, data) {
        return Device.device.bind(Device, d.params.channel_id);
      },
    }),
  () => new StateStorage({ orderByField: "created_at", orderByReverse: true, itemsPerPage: 20 })
);
