import React, { useState, useMemo, useEffect } from "react";
import { Section, InputNumber } from "@/components/visualizations/editor";
import Checkbox from "antd/lib/checkbox/Checkbox";
import ColorPicker from "@/components/ColorPicker";
import ColorPalette from "@/visualizations/ColorPalette";
import Table from "antd/lib/table";
import Form from "antd/lib/form";
import Button from "antd/lib/button";
import { UpdateOptionsStrategy } from "@/components/visualizations/editor/createTabbedEditor";
import { max } from "lodash";

const RangeStateSettings = ({ form, options, data, visualizationName, onOptionsChange }) => {

  const [states, setStates] = useState(options.states);
  const MIN = options.range[0];
  const MAX = options.range[1];

  const colors = useMemo(
    () => ({
      Automatic: null,
      ...ColorPalette,
    }),
    []
  );

  useEffect(() => {
    onOptionsChange({ states: states }, UpdateOptionsStrategy.shallowMerge);
  }, [states]);

  const addRow = () => {
    var newState = [...states];
    newState.push({ range: [0, 0], key: newState.length, color: "#FFFFFF" });
    setStates(newState);
  };

  const deleteRow = recordKey => {
    var newState = [...states];
    var index = newState.findIndex(record => record.key == recordKey);

    newState.splice(index, 1);
    setStates(newState);
  };

  const updateStates = (value, index, record) => {
    var newState = [...states];
    if ("color" == index) {
      newState[record.key].color = value;
      onOptionsChange({ states: newState });
    } else if ("from" == index) {
      newState[record.key].from = value;

      onOptionsChange({ states: newState });
    } else {
      newState[record.key].to = value;

      onOptionsChange({ states: newState });
    }
  };

  const columns = [
    {
      title: "From",
      key: "from",
      dataIndex: "from",
      render: (from, record) => (
        <Form initialValues={{ from: from }}>
          <Form.Item key={record.key} name="from" rules={[{ required: true }]}>
            <InputNumber min={MIN} MAX={max} onChange={value => updateStates(value, "from", record)} />
          </Form.Item>
        </Form>
      ),
    },
    {
      title: "TO",
      key: "to",
      dataIndex: "to",
      render: (to, record) => (
        <Form initialValues={{ to: to }}>
          <Form.Item key={record.key} name="to" rules={[{ required: true }]}>
            <InputNumber max={MAX} min={MIN} onChange={value => updateStates(value, "to", record)} />
          </Form.Item>
        </Form>
      ),
    },
    {
      title: "Color ",
      key: "color",
      dataIndex: "color",
      render: (color, record) => (
        <Form initialValues={{ color: color }}>
          <Form.Item key={record.key} name="color">
            <ColorPicker
              color={color}
              interactive
              presetColors={colors}
              placement="topRight"
              onChange={newColor => updateStates(newColor, "color", record)}
              addonAfter={<ColorPicker.Label color={color} presetColors={colors} />}
            />
          </Form.Item>
        </Form>
      ),
    },

    {
      title: "",
      dataIndex: "operation",
      render: (text, record) => {
        return <>{record.key > 2 && <i className="fa fa-trash-o m-r-5" onClick={() => deleteRow(record.key)} />}</>;
      },
    },
  ];

  return (
    <>
      <div className="add-btn" style={{ display: "flex", justifyContent: "flex-end", marginBottom: "5px" }}>
        <div style={{ paddingRight: "15px", marginRight: "auto" }}>
          <Section>
            <Checkbox checked={options.showLabel} onChange={e => onOptionsChange({ showLabel: e.target.checked })}>
              Show Label
            </Checkbox>

            <Checkbox checked={options.showValue} onChange={e => onOptionsChange({ showValue: e.target.checked })}>
              Show Value
            </Checkbox>
            <Checkbox checked={options.showIcon} onChange={e => onOptionsChange({ showIcon: e.target.checked })}>
              Show Icon
            </Checkbox>
          </Section>
        </div>
        <Button onClick={addRow} type="primary">
          <i className="fa fa-plus m-r-5" />
          Add
        </Button>
      </div>
      <Section>
        <Table dataSource={states} columns={columns} pagination />
      </Section>
    </>
  );
};

export default RangeStateSettings;
