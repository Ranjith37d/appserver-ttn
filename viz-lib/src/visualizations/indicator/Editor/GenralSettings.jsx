import React from "react";
import { map } from "lodash";
import Form from "antd/lib/form";
import { Section, Select } from "@/components/visualizations/editor";

const GenralSettings = ({ form, options, data, visualizationName, onOptionsChange }) => {
  return (
    <>
      <Section>
        <Form form={form[0]} initialValues={{ Parameters: options.ParameterData}}>
          <Form.Item name="Parameters" rules={[{ required: true }]}>
            <Select
              layout="horizontal"
              label="Select Parameter"
            
              onChange={selectedValue => onOptionsChange({ ParameterData: selectedValue })}>
              {map(data.columns, col => (
                <Select.Option key={col.name} data-test={"Counter.General.ValueColumn." + col.name}>
                  {col.name}
                </Select.Option>
              ))}
            </Select>
          </Form.Item>
        
        </Form>
      </Section>
    </>
  );
};

export default GenralSettings;
