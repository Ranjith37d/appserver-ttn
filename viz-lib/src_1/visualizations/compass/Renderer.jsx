import React, { useEffect, useState } from "react";
import compass from ".";
import Text from "../text/Renderer"
import "./wind.css";




const Renderer = ({ data, options, visualizationName }) => {


  const [scale, setScale] = useState("1.00");
  const [container, setContainer] = useState(null);
  const [windLineElement, setWindLineElement] = useState();
  const [windArrrowElement, setWindArrowElement] = useState();
  const [windCircleElement, setWindCircleElement] = useState();
  const [currentValue, setCurrentValue] = useState(0);
  const [textValueContainer, setTextvalueContainer] = useState();
  const [labelTextContainer, setLabelTextContainer] = useState();
  const [currentLabel, setCurrentLabel] = useState("");

  const filtervalue = () => {
    if (options.ParameterData !== undefined) {
      if (data.rows.length !== 0) {
        setCurrentValue(data.rows[0][options.ParameterData]);
        options.value = currentValue;
      } else {
        options.value = 0;
      }
    }
  };
  useEffect(() => {
    if (container) {
      filtervalue();
      if(options.showIcon){}
      windMillfunctioning();
      
    }
  }, [options, container, currentValue,windArrrowElement,windCircleElement,windLineElement]);


  const windMillfunctioning = () => {
    if(options.showIcon && windArrrowElement){
    windArrrowElement.style.transform = `rotate(${currentValue}deg)`;
    windCircleElement.style.transform = `rotate(${currentValue}deg)`;
    windLineElement.style.transform = `rotate(${currentValue}deg)`;
  }
 };
  return (
    <>
      <div className="bulb-viz-container">
        <div className="counter-layout">
          { options.showIcon &&
          <div className="column col-main">
            <svg viewBox="0 0 640 480">
              <g>
                <circle className="wind0" cx="195.5" cy="238" r="105.5" />
                <path
                  className="wind1"
                  d="M195.5,133c57.9,0,105,47.1,105,105s-47.1,105-105,105s-105-47.1-105-105S137.6,133,195.5,133 M195.5,132   c-58.54,0-106,47.46-106,106s47.46,106,106,106s106-47.46,106-106S254.04,132,195.5,132L195.5,132z"
                />
              </g>
              <g>
                <line ref={setWindLineElement} className="wind2" x1="193" y1="315" x2="193" y2="173" />
              </g>
              <g>
                <g>
                  <circle className="wind0" cx="193" cy="323.5" r="4.5" />
                  <path
                    className="wind1"
                    ref={setWindCircleElement}
                    d="M193,320c1.93,0,3.5,1.57,3.5,3.5s-1.57,3.5-3.5,3.5s-3.5-1.57-3.5-3.5S191.07,320,193,320 M193,318    c-3.04,0-5.5,2.46-5.5,5.5s2.46,5.5,5.5,5.5s5.5-2.46,5.5-5.5S196.04,318,193,318L193,318z"
                  />
                </g>
                <polygon ref={setWindArrowElement} className="wind3" points="194,160 180,174 206,174  " />
              </g>
              <g>
                <circle className="wind0" cx="263" cy="159.5" r="12.5" />
              </g>
              <g>
                <circle className="wind0" cx="128" cy="159.5" r="12.5" />
              </g>
              <g>
                <circle className="wind0" cx="90" cy="231.5" r="12.5" />
              </g>
              <g>
                <circle className="wind0" cx="302" cy="231.5" r="12.5" />
              </g>
              <g>
                <circle className="wind0" cx="118" cy="311.5" r="12.5" />
              </g>
              <g>
                <circle className="wind0" cx="272" cy="311.5" r="12.5" />
              </g>
              <g>
                <circle className="wind0" cx="195" cy="341.5" r="12.5" />
              </g>
              <g>
                <circle className="wind0" cx="195" cy="134.5" r="12.5" />
              </g>
              <text transform="matrix(1 0 0 1 187.8486 138.9614)" className="wind4 wind5">
                N
              </text>
              <text transform="matrix(1 0 0 1 81.8486 237.9614)" className="wind4 wind5">
                W
              </text>
              <text transform="matrix(1 0 0 1 187.8486 346.9614)" className="wind4 wind5">
                S
              </text>
              <text transform="matrix(1 0 0 1 107.8486 162.9614)" className="wind4 wind6">
                NW
              </text>
              <text transform="matrix(1 0 0 1 109.8486 314.9614)" className="wind4 wind6">
                SW
              </text>
              <text transform="matrix(1 0 0 1 267.8486 314.9614)" className="wind4 wind6">
                SE
              </text>
              <text transform="matrix(1 0 0 1 297.8486 238.9614)" className="wind4 wind5">
                E
              </text>
              <text transform="matrix(1 0 0 1 255.8486 159.9614)" className="wind4 wind6">
                NE
              </text>
              <text transform="matrix(1 0 0 1 388.8916 235.4189)" className="wind7 wind8 wind9">
                {currentValue}
              </text>
              <g></g>
            </svg>
          </div>
}
          <div className="column col-complementary" role="complementary">
            <Text
              setlabelTextContainer={setLabelTextContainer}
              settextValueContainer={setTextvalueContainer}
              setcontainer={setContainer}
              setscale={setScale}
              options={options}
              visualizationName={visualizationName}
              data={data}
              container={container}
              scale={scale}
              currentLabel={options.labelName}
            />
          </div>
        </div>
      </div>
    </>
  );
};
export default Renderer;
