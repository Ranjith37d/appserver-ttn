import React, { useState, useEffect } from "react";

import { RendererPropTypes } from "@/visualizations/prop-types";

import Text from "../text/Renderer";
import "./bulb.less";

export default function Renderer({ data, options, visualizationName }) {
  const [scale, setScale] = useState("1.00");
  const [currentValue, setCurrentValue] = useState(0);

  const [container, setContainer] = useState(null);
  const [bulbContainer, setBulbContainer] = useState();
  const [textValueContainer, setTextvalueContainer] = useState();
  const [labelTextContainer, setLabelTextContainer] = useState();
  const [currentLabel, setCurrentLabel] = useState("");

  useEffect(() => {
    if (container) {
      if (options.ParameterData.length >= 1) {
        filtervalue();
        applyConfig();
      }
    }
  }, [options, container, currentValue, bulbContainer]);

  const filtervalue = () => {
    if (options.ParameterData !== undefined) {
      if (data.rows.length !== 0) {
        setCurrentValue(data.rows[0][options.ParameterData]);
        options.value = currentValue;
      } else {
        options.value = 0;
      }
    }
  };
  const applyConfig = () => {
    var state = options.states.find(value => value.from <= currentValue && value.to >= currentValue);
    if (!(bulbContainer == null)) {
    
      bulbContainer.style.fill = state ? state.color : "#cacaca";
    }
    textValueContainer.style.color = state ? state.color : "#595959";
    labelTextContainer.style.color = state ? state.color : "rgb(251, 181, 64)";
    setCurrentLabel(state ? state.label : "");
  };

  return (
    <>
      <div className="bulb-viz-container">
        <div className="counter-layout">
          {options.showIcon && (
            <div className="column col-main">
              <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" viewBox="0 0 512 460">
                <path
                  ref={setBulbContainer}
                  className="bulb0"
                  d="M341.89,279.51c3.04-10.14,9.13-19.28,17.25-26.38c15.22-12.68,24.86-31.96,24.86-53.26
	c0-39.56-32.97-72.03-73.55-70c-36.01,1.52-65.43,31.96-66.45,67.97c-0.51,22.32,9.13,42.1,24.86,55.29
	c8.62,7.1,14.71,16.23,17.75,26.38H341.89L341.89,279.51z"
                />
                <path
                  className="bulb1"
                  d="M307.4,280.52h4.57l-20.29-75.58c0.51,0,1.01,0,1.52,0c2.54,0,5.07-1.01,7.1-3.04
	c1.01-1.01,2.03-1.52,3.55-1.52c1.52,0,2.54,0.51,3.55,1.52c3.55,4.06,9.64,4.06,13.19,0c1.01-1.01,2.03-1.52,3.55-1.52
	c1.01,0,2.54,0.51,3.55,1.52c2.03,2.03,4.06,3.04,7.1,3.04c0.51,0,1.01,0,1.52,0l-19.78,75.58h4.57l20.8-79.13
	c0-1.01,0-2.03-1.01-2.54c-1.01-0.51-2.03,0-2.54,0.51c-1.01,1.01-2.03,1.52-3.04,1.52c-1.52,0-2.54-0.51-4.06-1.52
	c-2.03-2.03-4.06-3.04-6.59-3.04s-4.57,1.01-6.59,3.04c-2.03,2.03-5.07,2.03-7.1,0c-1.52-2.03-4.06-3.04-6.59-3.04l0,0
	c-2.54,0-5.07,1.01-6.59,3.04c-1.01,1.01-2.54,1.52-4.06,1.52c-1.01,0-2.54-0.51-3.04-1.52c-0.51-0.51-1.52-1.01-2.54-0.51
	c-1.01,0.51-1.52,1.52-1.01,2.54L307.4,280.52z"
                />
                <g>
                  <path
                    className="bulb2"
                    d="M298.27,326.18c2.54,6.09,8.62,10.65,15.72,10.65c7.1,0,13.19-4.57,15.72-10.65H298.27z"
                  />
                  <path
                    className="bulb2"
                    d="M332.76,326.68h-37.03c-5.07,0-9.13-4.06-9.13-9.13V279h55.29v38.55
		C341.89,322.62,337.83,326.68,332.76,326.68z"
                  />
                </g>
                <g>
                  <path
                    className="bulb3"
                    d="M341.38,298.28H286.6c-3.04,0-5.58-2.54-5.58-5.58l0,0c0-3.04,2.54-5.58,5.58-5.58h54.78
		c3.04,0,5.58,2.54,5.58,5.58l0,0C346.96,295.74,344.42,298.28,341.38,298.28z"
                  />
                  <path
                    className="bulb3"
                    d="M341.38,316.03H286.6c-3.04,0-5.58-2.54-5.58-5.58l0,0c0-3.04,2.54-5.58,5.58-5.58h54.78
		c3.04,0,5.58,2.54,5.58,5.58l0,0C346.96,313.49,344.42,316.03,341.38,316.03z"
                  />
                </g>
              </svg>
            </div>
          )}
          {/* {!(options.showValue ==false && options.showLabel)  && ( */}
            <div className="column col-complementary" role="complementary">
              {options && (
                <Text
                  setlabelTextContainer={setLabelTextContainer}
                  settextValueContainer={setTextvalueContainer}
                  setcontainer={setContainer}
                  setscale={setScale}
                  options={options}
                  visualizationName={visualizationName}
                  data={data}
                  container={container}
                  scale={scale}
                  currentLabel={currentLabel}
                />
              )}
            </div>
          {/* )} */}
        </div>
      </div>
    </>
  );
}

Renderer.propTypes = RendererPropTypes;
